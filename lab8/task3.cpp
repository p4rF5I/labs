#include <iostream>
#include "queue.h"
using namespace std;
int main(){
	Queue<int> my;
	my.push(10);
	my.push(20);
	my.push(30);
	int n;
	cin >> n;
	int temp;
	for(int i = 0; i < n; ++i){
		cin >> temp;
		my.push(temp);
	}
	while(!my.isEmpty()){
		cout << my.pop() << '\n';
	}
	system("pause");
	return 0;
}