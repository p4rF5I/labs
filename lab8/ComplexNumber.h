#ifndef complex_number
#define complex_number
#include <iostream>
class Complex{
	private:
		double im;
		double real;
	public:
		const Complex operator+ (const Complex &a);
		const Complex operator+ (const double &a);
		const Complex operator*(const Complex &a);
		const Complex operator*(const double &a);
		const Complex operator-(const Complex &a);
		const Complex operator-(const double &a);
		const Complex operator/(const double &b);
		const Complex operator/(const Complex &b);
		const Complex& operator=(const Complex &a);
		const Complex& operator=(const double &a);
		Complex(const Complex &a);
		Complex(double a = 0,double b = 0): real(a),im(b){}
		void add(const Complex &b);
		void div(const Complex &b);
		void mul(const Complex &b);
		bool cmp(const Complex &b) const;
		void print() const;
		double getIm() const;
		double getReal() const;
};
class ComplexCommand{
public:
	static bool cmp(const Complex &a, const Complex &b);
	static void add(Complex &a, const Complex &b);
};
#endif
