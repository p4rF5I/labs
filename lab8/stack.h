#ifndef my_stack
#define my_stack
#include <iostream>
template <class T>class Node_stack{
	private:
		T key;
		Node_stack *next = nullptr;
	public:
		Node_stack(T var = 0, Node_stack* ptr = nullptr):next(ptr),key(var){}
		Node_stack<T> *getNext(){return next;}
		void setNext(Node_stack *ptr){
			next = ptr;
		}
		void setKey(T var){
			key = var;
		}
		T getKey(){
			return key;
		}
};
template <class T> class Stack{
	private:
		Node_stack<T> *top = nullptr;
	public:
	~Stack(){
		Node_stack<T> *ptr;
		while(!isEmpty()){
			ptr = top;
			top = top -> getNext();
			delete ptr;
		}
	}
	bool push(T var){
			if(!isEmpty()){
				top = new Node_stack<T>(var, top);
			} else {
				top = new Node_stack<T>(var);
			}
		}		
	T pop(){
			if(!isEmpty()){
				Node_stack<T> *temp = top -> getNext(); 
				T var = top -> getKey();
				delete top;
				top = temp;
				return var;
			} else {
				std::cout << "\n**********************************\n"
					 << "******[ERROR] Stack is free!******\n"
					 << "**********************************\n";
			}
		}
	bool isEmpty(){
		if(top == nullptr){
			return true;
		} else {
			return false;
		}
	}
};
#endif