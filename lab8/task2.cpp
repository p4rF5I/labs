/*
author: soroka
task: Разработать классы «стек» и «элемент» для работы с объектами
«комплексное число», используя материалы лекции. Реализовать
операции добавления и удаления элементов в/из структуры
данных.
full-tested
*/
#include <iostream>
#include "stack.h"
#include "ComplexNumber.h"
using namespace std;
int main(){
	Stack<Complex> myStack;
	double re,im;
	int n;
	cin >> n;
	for (int i = 0; i < n; ++i){
		cin >> re >> im;
		myStack.push(Complex(re,im));
	}
	Complex temp = 0;
	for(int i = 0; i < n; ++i){
		temp = temp + myStack.pop();
		// temp.print();
	} 
	temp.print();
	system("pause");
	return 0;
}
