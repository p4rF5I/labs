#ifndef my_queue
#define my_queue
#include <iostream>
template <class T>class Node_queue{
	private:
		T key;
		Node_queue *prev = nullptr;
	public:
		Node_queue(T var, Node_queue* prevPtr): prev(prevPtr), key(var){}
		Node_queue<T> *getPrev(){return prev;}
		void setPrev(Node_queue *ptr){
			prev = ptr;
		}
		void setKey(T var){
			key = var;
		}
		T getKey(){
			return key;
		}
};
template <class T> class Queue{
	Node_queue<T> *head = nullptr;
	Node_queue<T> *tail = nullptr;
public:
	~Queue(){
		while(!isEmpty()){
			pop();
		}
	}
	void push(T var){
		if(!isEmpty()){
			tail -> setPrev(new Node_queue<T>(var,nullptr));
			tail = tail -> getPrev();
		} else {
			tail = head = new Node_queue<T>(var,nullptr);
		}
	}		
	T pop(){
		if(!isEmpty()){
			T temp = head -> getKey();
			// delete head;
			if(head == tail){
				delete head;
				//delete tail;
				head = tail = nullptr;
				return temp;
			} else {
				Node_queue<T> *tempPtr = head -> getPrev();
				delete head;
				head = tempPtr;
				return temp;
			}
		} else {
			std::cout << "\n**********************************\n"
				 << "******[ERROR] Queue is free!******\n"
				 << "**********************************\n";
		}
	}
	bool isEmpty(){
		if(head == nullptr){
			return true;
		} else {
			return false;
		}
	}
};
#endif
