/*
author: soroka
task: Протестировать работу на массиве комплексных чисел, найдя
наибольшее/наименьшее по длине вектора комплексное число, а
также поместив элементы массива в стек и очередь и удалив
элементы из этих структур данных
tested: full tested;
*/
#include <iostream>
using namespace std;
#include "ComplexNumber.h"
#include "queue.h"
#include "stack.h"
int main(){
	Complex a[10];
	Stack<Complex> stack;
	Queue<Complex> queue;
	double re, im;
	for(int i = 0; i < 10; ++i){
		cout << "Enter complex number: ";
		cin >> re >> im;
		a[i] = Complex(re,im);
		stack.push(a[i]);
		queue.push(a[i]);
		system("clear || cls");
	}
	Complex max = a[0];
	for (int i = 1; i < 10; ++i){
		if(ComplexCommand::cmp(a[i],max)){
			max = a[i];
		}
	}
	cout << "max(array): ";
	max.print();
	max = stack.pop();
	Complex temp; 
	while(!stack.isEmpty()){
		temp = stack.pop();
		temp.print();
		if(ComplexCommand::cmp(temp,max)){
			max = temp;
		}
	}
	cout << "max(stack): ";
	max.print();
	max = queue.pop(); 
	while(!queue.isEmpty()){
		temp = queue.pop();
		temp.print();
		if(ComplexCommand::cmp(temp,max)){
			max = temp;
		}
	}
	cout << "max(queue): ";
	max.print();
	system("pause");
	return 0;
}