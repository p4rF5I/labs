#include "ComplexNumber.h"
void Complex::add(const Complex &b){
	im += b.im;
	real += b.real;
}
void Complex::mul(const Complex &b){
	im = b.im * real + b.real * im;
	real = real * b.real - im * b.im;
}
bool Complex::cmp(const Complex &b) const{
	if((im * im + real * real) >= (b.im * b.im + b.real * b.real)){
		return true;
	} else {
		return false;
	}
}
void Complex::print() const{
	if(real){
		std::cout << real;
		if(im > 0){
			if(im == 1){
				std::cout << " + " << 'i';	
			} else {
				std::cout << " + " << im << 'i';
			}
		}
		if(im < 0){
			if(im == -1){
				std::cout << " - " << 'i';
			}else {
				std::cout << " - " << -im << 'i';
			}
		}
	} else {
		if(im == 0){
			std::cout << '0'; 
		} else {
			if(im > 0){
				if(im == 1){
					std::cout << 'i';	
				} else {
					std::cout << im << 'i';
				}
			}
			if(im < 0){
				if(im == -1){
					std::cout << 'i';
				}else {
					std::cout << -im << 'i';
				}
			}
		}
	}
	std::cout << '\n';
}
Complex::Complex(const Complex &a){
	this -> real = a.real;
	this -> im = a.im;
}
const Complex Complex::operator+(const Complex &a){
	return Complex(this -> real + a.real, this -> im + a.im);
}
const Complex Complex::operator+(const double &a){
	return Complex(this -> real + a, this -> im);
}
const Complex Complex::operator-(const Complex &a){
	return Complex(this -> real - a.real, this -> im - a.im);
}
const Complex Complex::operator-(const double &a){
	return Complex(this -> real - a, this -> im);
}
const Complex Complex::operator*(const Complex &a){
	return Complex(real * a.real - im * a.im, a.im * real + a.real * im);
}
const Complex Complex::operator*(const double &a){
	return Complex(real * a, a * im);
}
const Complex& Complex::operator=(const Complex &a){
	real = a.real;
	im = a.im;
	return (*this);
}
const Complex& Complex::operator=(const double &a){
	real = a;
	im = 0;
	return (*this);
}
void Complex::div(const Complex &b){
	double temp = b.real * b.real + b.im * b.im;
	this -> real = (this -> real * b.real + this -> im * b.im)/(temp);
	this -> im = (b.real * this -> im - this -> real * b.im)/(temp);
}
const Complex Complex::operator/(const Complex &b){
	double temp = b.real * b.real + b.im * b.im;
	return Complex((this -> real * b.real + this -> im * b.im)/(temp), (b.real * this -> im - this -> real * b.im)/(temp));
}
const Complex Complex::operator/(const double &b){
	return Complex(this -> real / b, this -> im / b);
}
double Complex::getIm() const{
	return im;
}
double Complex::getReal() const{
	return real;
}
bool ComplexCommand::cmp(const Complex &a, const Complex &b){
	return a.cmp(b);
}
void ComplexCommand::add(Complex &a, const Complex &b){
	a = a + b;
}