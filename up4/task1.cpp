/*
author: soroka
task:(2 балла) Используя датчик случайных чисел, заполнись двумерный
массив (n x m) неповторяющимися числами.
task: module tested
*/
#include <iostream>
#include <fstream>
template <class T> bool enterMatrix(T **matrix,int n, int m);
template <class T> void printMatrix(T **start, int n, int m);
using namespace std;
int main(){
	srand(time(NULL));
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n, m = 0;
	if(!(cin >> n >> m)  || n < 0 || m < 0){ 
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	long long int maxElemnt = n * m * 2;
	bool *isUsed = new bool[maxElemnt];
	for (int i = 0; i < maxElemnt; ++i){
		isUsed[i] = false;
	}
	int **array = new int*[n];
	for (int i = 0;i < n; ++i){
		array[i] = new int [m];
	}
	long long int temp = rand() % maxElemnt;
	for(int i = 0; i < n; ++i){
		for(int j = 0;j < m; ++j){
			temp = rand() % maxElemnt;
			while(isUsed[temp]){
				if(temp < maxElemnt){
					++temp;
				} else {
					temp = 0;
				}
			}
			isUsed[temp] = true;
			array[i][j] = temp;
			cout << array[i][j] << '\t';
		}
		cout << '\n';

	}
	delete[] isUsed;
	for (int i = 0;i < n; ++i){
		delete[] array[i];
	}
	delete[] array;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i <= n; ++i){
		for(int j= 0;j <= m; ++j){
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void printMatrix(T **start, int n, int m){
	for (int i=0;i<=n;++i){
		for (int j=0;j<=m;++j){
			cout << start[i][j] << '\t';
		}
		cout << '\n';
	} 
}
