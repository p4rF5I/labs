/*
author: soroka
task:4. (2 балла) Дан двумерный массив (n x m). Найти: а) число пар
одинаковых соседних элементов в каждой строке; б) число пар
одинаковых соседних элементов в каждом столбце.
task: module tested
*/
#include <iostream>
#include <fstream>
template <class T> bool enterMatrix(T **matrix,int iMAX, int jMAX);
template <class T> void deleteMatrix(T **matrix,int n,int m);
void replaceWithZero(int **matrix,int n,int m,int number,int startI,int startJ);
bool isZeroInMatrix(int **matrix,int n,int m);
using namespace std;
int main(){
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n, m = 0;
	if(!(cin >> n >> m) || n < 0 || m < 0){
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	int **array = new int*[n];
	for (int i = 0;i < n; ++i){
		array[i] = new int [m];
	}
	if(enterMatrix(array,n,m)){
		cout << "[ERROR] Inicalization error!\n";
		deleteMatrix(array,n,m);
		return 1;
	}
	cout << "The number of pairs of identical neighboring elements in each row: " << '\n';
	int count = 0;
	--m;
	for(int i = 0; i < n; ++i){
		for(int j = 0;j < m; ++j){
			if(array[i][j] == array[i][j+1]){
				++count;
			}
		}
			cout << "matrix[" << i << "][]\t" << count << '\n';
			count = 0; 
	}
	cout << "******************************************************************************\n";
	cout << "The number of pairs of identical neighboring elements in each column: " << '\n';
	++m;
	--n;
	for(int j = 0;j < m; ++j){
		for(int i = 0; i < n; ++i){
			if(array[i][j] == array[i+1][j]){
				++count;
			}
		}
			cout << "matrix[][" << j << "]\t" << count << '\n';
			count = 0; 
	}
	deleteMatrix(array,n,m);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i < n; ++i){
		for(int j= 0;j < m; ++j){
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void deleteMatrix(T **matrix,int n,int m){
	for (int i = 0;i < n; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
}