/*
author: soroka
task:4. (2 балла) Дан двумерный массив (n x m). Определить количество
различных элементов в нем.
task: module tested
*/
#include <iostream>
#include <fstream>
template <class T> bool enterMatrix(T **matrix,int n, int m);
template <class T> void deleteMatrix(T **matrix,int n,int m);
void replaceWithZero(int **matrix,int n,int m,int number,int startI,int startJ);
bool isZeroInMatrix(int **matrix,int n,int m);
using namespace std;
int main(){
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n, m = 0;
	if(!(cin >> n >> m) || n < 0 || m < 0){
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	int **array = new int*[n];
	for (int i = 0;i < n; ++i){
		array[i] = new int [m];
	}
	if(enterMatrix(array,n,m)){
		cout << "[ERROR] Inicalization error!\n";
		deleteMatrix(array,n,m);
		return 1;
	}
	int amountOfDifferentNumbers = isZeroInMatrix(array,n,m);
	for(int i = 0; i < n; ++i){
		for(int j = 0;j < m; ++j){
			if(array[i][j]){
				++amountOfDifferentNumbers;
				replaceWithZero(array,n,m,array[i][j],i,j);
			}
		}
	}
	cout << amountOfDifferentNumbers << '\n';
	deleteMatrix(array,n,m);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i < n; ++i){
		for(int j= 0;j < m; ++j){
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void deleteMatrix(T **matrix,int n,int m){
	for (int i = 0;i < n; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
}
bool isZeroInMatrix(int **matrix,int n,int m){
	for(int i = 0; i < n; ++i){
		for(int j = 0;j < m; ++j){
			if(matrix[i][j] == 0){
				return true;
			}
		}
	}
	return false;
}
void replaceWithZero(int **matrix,int n,int m,int number,int startI,int startJ){
	for(int i = startI; i < n; ++i){
		for(int j = startJ;j < m; ++j){
			if(matrix[i][j] == number){
				matrix[i][j] = 0;
			}
		}
	}
}	