/*
author: soroka
task: (2 балла) В двумерном массиве размером n x n записано количество
очков, набранных той или иной командой во встречах с другими
командами (3 – если данная команда выиграла игру, 0 – если
проиграла, 1 – если игра закончилась вничью). Определить:
а) сколько очков набрала команда, ставшая чемпионом,
б) номер команды, занявшей последнее место.
task: module tested
*/
#include <iostream>
#include <fstream>
template <class T> bool enterMatrix(T **matrix,int iMAX, int jMAX);
template <class T> void deleteMatrix(T **matrix,int n,int m);
using namespace std;
int main(){
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	int n = 0;
	if(!(cin >> n) || n < 0){
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	int **array = new int*[n];
	for (int i = 0;i < n; ++i){
		array[i] = new int [n];
	}
	if(enterMatrix(array,n,n)){
		cout << "[ERROR] Inicalization error!\n";
		deleteMatrix(array,n,n);
		return 1;
	}
	int winnerIndex = 0; int winnerPoints = -1;
	int looserIndex = 0; int looserPoints = 3*n + 1;
	int correctPoints = 0;
	for(int i = 0; i < n; ++i){
		for(int j = 0;j < i; ++j){
			correctPoints += array[i][j];
 		}
		for(int j = i+1; j < n; ++j){
			correctPoints += array[i][j];
		}
		if(correctPoints <= looserPoints){
			looserIndex = i;
			looserPoints = correctPoints;
		}
		if(correctPoints >= winnerPoints){
			winnerIndex = i;
			winnerPoints = correctPoints;	
		}
		correctPoints = 0;
	}
	cout << "winner[" << winnerIndex << "]: " << winnerPoints << '\n';
	cout << "looser[" << looserIndex << "]: " << looserPoints << '\n';
	deleteMatrix(array,n,n);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i < n; ++i){
		for(int j= 0;j < m; ++j){
			if (!(cin >> matrix[i][j]) || matrix[i][j] < 0) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void deleteMatrix(T **matrix,int n,int m){
	for (int i = 0;i < n; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
}