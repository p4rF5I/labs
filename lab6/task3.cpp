/*
author: soroka;
task: deleteString,deleteColumn,insertString,insertColumn
tested: full-part-tested
*/
#define AUTOFILL_STRING 0
#define AUTOFILL_COLUMN 1
#include <iostream>
template <class T> void printMatrix(T **matrix, int stringsAmount, int columnsAmount);
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount);
void fillMatrixWithRandomNumbers (int** matrix, int maxI, int maxJ, int max = 100);
void deleteString (int ** &matrix, int &maxI, int &maxJ, int del);
void deleteColumn (int ** &matrix, int &maxI, int &maxJ, int del);
void insertString (int** &matrix, int &maxI, int &maxJ, int insertPos);
void insertColumn (int** &matrix, int &maxI, int &maxJ, int insertPos);
using namespace std;
int main(){
	bool isOK = true;
	int stringsAmount,columnsAmount;
    cout << "Enter size of matrix(strings columns): ";
    if (!(cin >> stringsAmount) || !(cin >> columnsAmount) 
        || stringsAmount <= 0 || columnsAmount <= 0){
        cout << "[ERROR]Input error!\n";
        isOK = false;
    }
    if (isOK){
    	int **matrix;
    	createMatrix(matrix, stringsAmount, columnsAmount);
    	fillMatrixWithRandomNumbers(matrix, stringsAmount, columnsAmount);	
    	int choice = isOK;
    	void (*operatin[])(int **&, int&, int&, int) = {
    		NULL, &deleteString, &deleteColumn, &insertString, &insertColumn
    	};
    	int pos;
		while (choice != 0){
			printMatrix(matrix, stringsAmount, columnsAmount); 
			cout << "0 - exit, 1 - delete string, 2 - delete column, 3 - insert string, 4 - insert column\n";
			if (!(cin >> choice) || choice > 4 || choice <= 0) {
				if(choice == 0){continue;}
				system("clear || cls");
				cout << "[ERROR] Incorrect input, try again!\n";
			} else {
				system ("clear || cls");
				printMatrix(matrix, stringsAmount, columnsAmount); 
				cout << "Enter necessary position: ";
				if(!(cin >> pos)){
					cout << "[ERROR]Input error!\n";
					continue;
				}
				operatin[choice](matrix,stringsAmount,columnsAmount,pos);
			}
			// printMatrix(matrix, stringsAmount, columnsAmount); 
		}
	}
	system("pause");
	return !isOK;
}
void fillMatrixWithRandomNumbers(int **matrix,int maxI, int maxJ, int max){
	srand(time(NULL));
	for (int i = 0; i < maxI; ++i){
		for(int j = 0; j < maxJ; ++j){
			matrix[i][j] = random() % max + 1;
		}
	}
}
void deleteString (int** &matrix, int &maxI, int &maxJ, int del){
	if (maxI == 0){return;}
	if (maxI <= del) {del = maxI - 1;}
	int **temp;
	--maxI;
	createMatrix(temp,maxI, maxJ);
	for (int i = 0; i < del; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i][j];
		}
	}
	for (int i = del; i < maxI; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i+1][j];
		}
	}
	++ maxI;
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	-- maxI;
	matrix = temp;
}
void deleteColumn (int** &matrix, int &maxI, int &maxJ, int del){
	if (maxJ == 0){return;}
	if (maxJ <= del) {del = maxJ - 1;}
	int **temp;
	--maxJ; 
	createMatrix(temp, maxI, maxJ);
	for (int i = 0; i < maxI; ++i){
		for (int j = 0; j < del; ++j){
			temp[i][j] = matrix[i][j]; 
		}
		for (int j = del; j < maxJ; ++j){
			temp[i][j] = matrix[i][j+1]; 
		}
	}
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	matrix = temp;
}
template <class T> void printMatrix(T **matrix, int stringsAmount, int columnsAmount){
    for (int i=0;i < stringsAmount; ++i){
        for (int j = 0;j < columnsAmount; ++j){
            cout << matrix[i][j] << '\t';
        }
        cout << '\n';
    } 
}
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount){
        matrix = new T *[stringsAmount];
        for(int i = 0; i < stringsAmount;++i){
            matrix[i]= new T [columnsAmount];
        }  
}
void insertString (int** &matrix, int &maxI, int &maxJ, int insertPos){
	if (maxI < insertPos) {insertPos = maxI;}
	int **temp;
	++maxI;
	createMatrix(temp, maxI, maxJ);
	for (int i = 0; i < insertPos; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i][j];
		}
	}
	for (int j = 0; j < maxJ; ++j){
		temp[insertPos][j] = AUTOFILL_STRING;
	}
	for (int i = insertPos + 1; i < maxI; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i-1][j];
		}
	}
	--maxI;
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	++maxI;
	matrix = temp;
}
void insertColumn (int** &matrix, int &maxI, int &maxJ, int insertPos){
	if (maxJ < insertPos) {insertPos = maxJ;}
	int **temp;
	++maxJ;
	createMatrix(temp, maxI, maxJ);
	for (int i = 0; i < maxI; ++i){
		for (int j = 0; j < insertPos; ++j){
			temp[i][j] = matrix[i][j]; 
		}
		temp[i][insertPos] = AUTOFILL_COLUMN;
		for (int j = insertPos + 1; j < maxJ; ++j){
			temp[i][j] = matrix[i][j-1]; 
		}
	}
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	matrix = temp;
}