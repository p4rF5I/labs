/*
author: soroka;
task: sum,sub,mul,div
tested: full part-testd
*/
#include <iostream>
double sum(double a, double b);
double mul(double a, double b);
double div(double a, double b);
double sub(double a, double b);
using namespace std;
int main(){
	double (*operation[5])(double, double) = {
		NULL, 
		&sum,
		&sub,
		&mul,
		&div};
	char operationMark[5] = {
		'e', '+', '-', '*','/'
	};
	short int choice;
	double a,b;
	do{
		cout << "Use: 0 - exit , 1 - sum, 2 - sub, 3 - mul, 4 - div; \n";
		cout << "Your choice: ";
		cin >> choice;
		if (choice > 4 || choice <= 0) {
			if(choice == 0){continue;}
			system("clear || cls");
			cout << "[ERROR] Incorrect input, try again!\n";
		} else {
			cout << "Enter a and b(a b): ";
			if (!(cin >> a >> b)){
					system("clear || cls");
					cout << "[ERROR] Incorrect input, try again!\n";
					continue;
			}
			system("clear || cls");
			cout << a << ' ' << operationMark[choice] << ' ' << b 
			<< " = " <<  operation[choice](a,b) << '\n';
		}
	} while (choice != 0);
	system("pause");
	return 0;
}
double sum(double a, double b){
	return a + b;
}
double mul(double a, double b){
	return a * b;
}
double div(double a, double b){
	return a / b;
}
double sub(double a, double b){
	return a - b;
}