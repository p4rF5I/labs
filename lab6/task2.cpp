/*
author: soroka;
task: findSimple,findDivBy3,findDivBy2
tested: full-part-tested
*/
#include <iostream>
#include <cmath>
bool isSimple(int a);
int *findSimple(int *array, int size);
int *findDivBy3(int *array, int size); 
int *findDivBy2(int *array, int size); 
void fillWithMinusOne(int *start, int *end);
template <class T> void printArray(T *start, int size);
template <class T> void printArray(T *start, T *end);
bool enterArray(int *array, int size);
using namespace std;
int main(){
	bool isOK = true;
	int size;
    cout << "Enter size of array: ";
    if (!(cin >> size) || size <= 0){
        cout << "[ERROR]Input error!\n";
        isOK = false;
    }
    if(isOK){
        int *array = new int [size];
		if(enterArray(array,size)){
			cout << "[ERROR] array initialization error!\n";
			delete[] array;
			isOK = false;
		}
    	if(isOK){
			short int choice;
			int *(*operation[])(int*, int) = {
				NULL, &findSimple, &findDivBy3, &findDivBy2
			};
			int *result;
			do{
				cout << "Use: 0 - exit , 1 - find simle, 2 - find 3 | , 3 - 2 |\n";
				cout << "Your choice: ";
				if (!(cin >> choice) || choice > 3 || choice <= 0) {
					if(choice == 0){continue;}
					system("clear || cls");
					cout << "[ERROR] Incorrect input, try again!\n";
				} else {
					system ("clear || cls");
					printArray(array,array + size - 1); 
					result = operation[choice](array,size);
					// cout << "Pointer: " <<  result << '\n';
					for (int i = 0; i < size; ++i){
						if (result[i] == -1){
							break;
						} else {
							cout << result[i] << ", ";
						}
						cout << "\b\b.\n";
					}
					delete[] result;
				}
			} while (choice != 0);
		}
		delete[] array;

	}
	system("pause");
	return !isOK;
}
bool isSimple(int a){
	// cout << "isSimple: " << a << '\n';
	if (a < 0){return 0;}
	int temp = sqrt(a);
	for (int i = 2; i <= temp; ++i){
		if (a % i == 0) {return false;} 
	}
	return true;
}
int *findSimple(int *array, int size){
	int *result = new int[size];
	int t = 0;
	fillWithMinusOne(result,result + size - 1);
	for (int i = 0; i < size; ++i){
		// cout << "findSimple: " << array[i] << '\n';
		if(isSimple(array[i])) {
			result[t] = array[i];
			++t;
			// cout << "T: " << t << "\tresult[t] = " << result[t-1] << '\n';
		}
	}
	return result;
}
int *findDivBy3(int *array, int size){
	int *result = new int[size];
	int t = 0;
	fillWithMinusOne(result,result + size - 1);
	for (int i = 0; i < size; ++i){
		// cout << "findDivBy3: " << array[i] << '\n';
		if(array[i] % 3 == 0) {
			result[t] = array[i];
			++t;
		}
	}
	return result;
}
int *findDivBy2(int *array, int size){
	int *result = new int[size];
	int t = 0;
	fillWithMinusOne(result,result + size - 1);
	for (int i = 0; i < size; ++i){
		// cout << "findDivBy2: " << array[i] << '\n';
		if(array[i] % 2 == 0) {
			result[t] = array[i];
			++t;
		}
	}
	return result;
}
void fillWithMinusOne(int *start, int *end){
	while(start <= end){
		*start = -1;
		++start;
	}
}
template <class T> void printArray(T *start, T *end){
    if(start > end) return;
    while (start <= end){
        std::cout << *start << ", ";
        ++ start;
    }
    std::cout << "\b\b.\n";
}
template <class T> void printArray(T *start, int size){
	printArray(start, start + size -1);
}
bool enterArray(int *array, int size){
        int t;
        cout << "Wish random nambers(use: 0 -NO || t to 1=<x<=t): ";
        if (!(cin >> t) || !t){
            cout << "Enter array elements: ";
            for (int i = 0;i < size; i++){
                 if (!(cin >> array[i])) {
                    return true;
                }
            }
        } else {
            srand(time(NULL));
            for (int i=0; i<size; ++i){
                array[i]= rand()%t+1;
            }
        }
	return false;
}