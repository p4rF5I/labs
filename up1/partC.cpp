/*
author: soroka
task: 2.	Даны целое число к (1≤ k ≤150) и последовательность цифр 101102103 ... 149150, 
в которой выписаны подряд все трехзначные числа от 101 до 150. Определить k-ю цифру
tested: all k=1..150(by using "for" and "char")
*/
#include <iostream>
using namespace std;
int main() {
	short int k;
	cout << "Enter k(1<=k<=150): ";
	cin >> k;
	if (k > 150 || k < 1) {
		cout << "Input error\n";
		system("pause");
		return -1;
	}
	unsigned short int digitID = (k % 3);
	unsigned short int 	numberID = (int) (k / 3);
	if (!digitID) { digitID = 3; --numberID; }	//1- first digit of numberID number,..,0=3 digit of(+ numberID correction) ..
	unsigned short int tenPower = pow(10, (3 - digitID));
	cout << "Your digit(" << k << ") is: " << (((int)((101 + numberID) / tenPower)) % 10) << '\n';
	system("pause");
	return 0;
}