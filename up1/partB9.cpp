/*
author: soroka
task: B.9	Д9.	В трехзначном числе х зачеркнули его последнюю цифру. 
Когда в оставшимся двузначном числе переставили цифры, а затем приписали к 
ним слева последнюю цифру числа х, то получилось число 654. Найти число х.
tested: yes
*/
#include <iostream>
using namespace std;
int main() {
	int number = 654;
	/*cout << "Enter 3-digit number: ";
	cin >> number;
	if (number > 999 || number < 100) {
		cout << "It isn't 3-digit number!\n";
		system("pause");
		return -1;
	}*/
	cout << "x="<< 100*(number%10)+10*((number/10)%10)+number/100<<'\n';
	system("pause");
	return 0;
}