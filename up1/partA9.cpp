/*
author: soroka
task: A.9	Дано четырехзначное число. Найти:
а)	число, полученное при прочтении его цифр справа налево;
б)	число, образуемое при перестановке первой и второй, третьей и четвертой цифр заданного числа. Например, из числа 5434 получить 4543, из числа 7048—784;
в)	число, образуемое при перестановке второй и третьей цифр заданного числа. Например, из числа 5084 получить 5804;
г)	число, образуемое при перестановке двух первых и двух последних цифр заданного числа. Например, из числа 4566 получить 6645, из числа 7304 — 473;
Последнюю задачу решить двумя способами:
•	с выделением отдельных цифр заданного числа;
•	без выделения отдельных цифр заданного числа.
tested:
		1234 	0001	2000
		2002	1301	0111
		29333	1111	9999
*/
#include <iostream>
using namespace std;
int main() {
	int number;
	cout << "Enter 4-digit number: ";
	cin >> number;		// 1 2 3 4
	if (number > 9999 || number < 1000) {
		cout << "It isn't 4-digit number!\n";
		system("pause");
		return -1;
	}
	system("cls");
	int a[5]; //digits
	a[1] = number / 1000; // thats true because a[i]-integer(9.9999999->9)
	a[4] = number % 10;
	a[3] = (number /10) % 10;
	a[2] = (number / 100)%10; 
	cout << "Input:\t(1234)" <<'\t'<< number<< '\n';
	cout<<"a)\t(4321)\t" << 1000 * a[4] + 100 * a[3] + 10 * a[2] + a[1] << '\n';//4 3 2 1
	cout << "b)\t(2143)\t" << 1000 * a[2] + 100 * a[1] + 10 * a[4] + a[3] << '\n';//2 1 4 3
	cout << "c)\t(1324)\t" << 1000 * a[1] + 100 * a[3] + 10 * a[2] + a[4] << '\n';//1 3 2 4
	cout << "d.1)\t(3412)\t" << 1000 * a[3] + 100 * a[4] + 10 * a[1] + a[2] << '\n';//3 4 1 2
	cout << "d.2)\t(3412)\t" << 100*(number%100)+number/100 << '\n';//3 4 1 2
	system ("pause");
	return 0;
}