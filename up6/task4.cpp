/*
author: soroka
task: akkermanFunk
tested: full tested(if result is unsigned long long integer)
*/
#include <iostream>
using namespace std;
unsigned long long int akkermanFunc(int n, int m);
int main(){
	for (int i = 0; i <= 10; ++i){
		cout << akkermanFunc(0,i) << '\t' << akkermanFunc(1,i) << '\t'
		<< akkermanFunc(2,i) << '\t' << akkermanFunc(3,i) << '\n';
	}
	system("pause");
	return 0;
}
unsigned long long int akkermanFunc(int n, int m){
	if(n == 0) {
		return m+1;
	} 
	if(m == 0){
		return akkermanFunc(n-1,1);
	}
	return akkermanFunc(n-1,akkermanFunc(n,m-1));
}