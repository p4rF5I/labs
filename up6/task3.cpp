/*
author: soroka
task: Цифровой корень данного числа
получается следующим образом: если сложить все цифры этого
числа, затем все цифры найденной суммы и повторять этот процесс,
то в результате будет получено однозначное число (цифра), которая
и называется цифровым корнем данного числа.
tested: 15634, 222 ,228 ,-2 , 0 ,1 ,10 ,20 ,333 ,3333 ,33331 ,1234567890

*/
#include <iostream>
using namespace std;
int digitalRoot(int a);
int main(int argc, char *argv[]){
	// cout << argc;
	if(argc == 2){

		cout << digitalRoot(atoi(argv[1])) << '\n';
	} else {
		int a;
		cout << "Enter a: "
		cin >> a;
		cout << digitalRoot(a) << '\n';
	}
	system("pause");
	return 0;
}
int digitalRoot(int a){
	if(a < 10){
		return a;
	}
	int sum = 0;
	while(a){
		sum += a % 10;
		a /= 10;
	}
	return digitalRoot(sum);
}