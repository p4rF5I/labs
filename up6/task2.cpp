/*
author: soroka 
task: Написать рекурсивную функцию, определяющую, является
ли симметричной часть строки s, начиная с i-го элемента и кончая j-м.
tested: 
*/
#include <iostream>
#include <cstring>
using namespace std;
bool isSymmetrical(char *str, int i, int j);
bool isSymmetrical_(char *str, int i, int j);
int main(int argc, char *argv[]){
	if(argc == 4){
		cout << argv[1] << '\t' << argv[2] << '\t' << argv[3] << '\t' << isSymmetrical(argv[1],atoi(argv[2]),atoi(argv[3]));
	} else {
		char str[1024];
		int i;
		int j;
		cout << "str i j : ";
		cin >> str >> i >> j;
		cout << str << '\t' << i << '\t' << j << '\t' << isSymmetrical(str,i,j);
	}
	system("pause");
	return 0;
}
bool isSymmetrical(char *str, int i, int j){	//use it for check and call recursive func isSymmetrical_
	unsigned int strSize = strlen(str) - 1;
	if (i > strSize || j > strSize){
		return false;
	}
	if(i < j){
		return isSymmetrical_(str, i, j);
	} else {
		return isSymmetrical_(str, j, i);
	}
}
bool isSymmetrical_(char *str, int i, int j){
	// cout << "str[" << i << "]: " << str[i] << '\n';
	// cout << "str[" << j << "]: " << str[j] << '\n';
	if(str[i] != str[j]){
		return false;
	} else {
		if(i == j || i == (j - 1)){
			return true;
		}
		return isSymmetrical_(str, i + 1, j - 1);
	}
}