/*
author: soroka
task: Написать рекурсивную функцию для вычисления индекса
максимального элемента массива из n элементов
tested: full tested
*/
#include <iostream>
using namespace std;
unsigned int getIndexOfMax(int *arr, unsigned int size);
int main(){
	int a[4];
	for(int i = 0; i < 4; ++i){
		cin >> a[i];
 	}
	cout << '\n' << getIndexOfMax(a,4);
	system("pause");
	return 0;
}
unsigned int getIndexOfMax(int *arr, unsigned int size){
	static unsigned int maxIndex = size - 1;
	if(size != 0){
		maxIndex = (arr[size - 1] > arr[maxIndex]) ? (size - 1): maxIndex;
		return getIndexOfMax(arr,size - 1);
	} else {
		return maxIndex;
	}
}