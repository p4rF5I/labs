/*
author:soroka
task: Print in ascending order all simple irreducible fractions between 0 and 1 whose denominators are less than a given number n.
tested: 5 1/4,1/3,2/5,1/2,3/5,2/3,3/4,4/5
        1 -
        2   1/2
        3 1/3,1/2,2/3
        20.

*/
#include <iostream>
#define E 0.00000000001
using namespace std;
int gcd(int a, int b);//greatest common divisor
int main(){
	int number;
	cout << "Enter number(>0) : ";
	if (!(cin >> number)){
		cout << "[ERROR]Incorrect input!\n"; 
		system("pause");
		return 1;
	}
	if (number <= 0) {
		cout << "It isn't natural number!\n";
	 	system("pause");
	 	return 1;
    }
	int numeratorMin = number - 1;
    int denominatorMin = number;
    double min = (double) (number - 1) / number;
    double prev = 0;
    while (E + prev < min){
        for (int numerator = 1; numerator < number; ++numerator){
            for (int denominator = number; denominator > numerator; --denominator){
                if (((double)  numerator / denominator)  >  prev + E
                    && ((double) numerator / denominator) < min + E){
                    if (gcd(numerator, denominator) == 1){
                        min = (double) numerator/denominator;
                        // cout << min << '\n';
                        numeratorMin = numerator;
                        denominatorMin = denominator;
                    }
                }
            }
        }
        cout << numeratorMin << "/" << denominatorMin;// << " = " << (double) numeratorMin/denominatorMin;
        cout << '\n';
        prev = min;
        min = (double) (number - 1)/number;
        // cout << min << '\n';
    }
	system("pause");
	return 0;
}
int gcd(int a, int b) {
  int t;
  while (b != 0) {
    t = b;
    b = a % b;
    a = t;
  }
    // cout << a << '\t' << b << '\n';
    // last nonzero remainder - our gcd
    //Euclidean algorithm will end when one of the numbers 0
  return a;
}