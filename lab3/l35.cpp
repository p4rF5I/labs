/*
author:soroka
task:Find the k-th digit in the series of numbers composed of consecutive natural numbers starting from 1: 12345678910111213 ...
tested: full-tested
*/
#include <iostream>
using namespace std;
int main(){
	int number;
	cout << "Enter number(>0) : ";
	if (!(cin >> number)){
		cout << "[ERROR]Incorrect input!\n"; 
		system("pause");
		return 1;
	}
	if (number <= 0) {
		cout << "It isn't natural number!\n";
	 	system("pause");
	 	return 1;
	}
	int i = 1;	//digits
    int t = 9;	//numbers in line	
    int p = 9;	//in correct interval
    for (;number > t; i++, p *= 10, t += i * p){
    	// cout << t << '\t'<< p << '\t'<< i << '\n';
    }
    // cout << t << '\t'<< p << '\t'<< i << '\n';
    p = (p / 9 * 10 -((t - number) / i) - 1);	//correct number
    // cout << p << '\n';	
    for (int r = 0; r < ((t - number) % i); ++r){ p /= 10;}
    cout << p % 10  << '\n';
	system("pause");
	return 0;
}