/*
author: sorokaEA
condition: numbers of different digits
test: a -ERROR  -2331 -error, 0 - .
12 - 12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1. 
30 - 30, 29, 28, 27, 26, 25, 24, 23, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1. 
31 - 31, 30, 29, 28, 27, 26, 25, 24, 23, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1. 
*/
#include <iostream>
using namespace std;
bool isDigitInNumber(int digit,int a);
int  digitsInNumber(int number);
int numberOfDifferentDigits(int number);
int main(){
	int number;
	cout << "Enter number : ";
	if (!(cin >> number)){
		cout << "[ERROR]Incorrect input!\n"; 
		system("pause");
		return 1;
	}
	int count = 0;
	for (int i = number; i > 0 ; --i){
		// cout << numberOfDifferentDigits(i) << '\t' << digitsInNumber(i) << '\t' << i << '\n';
		if (numberOfDifferentDigits(i) == digitsInNumber(i)) {
			cout << i << ", ";
			++count;
			}
	}
	if(count != 0){
		cout << "\b\b.\n";
	} else {
		cout << "no such numbers.\n";
	}
	system("pause");
	return 0;
}
int digitsInNumber(int number){
	int digits = 0;
	for (digits = 1;number >= 10; number /= 10, ++digits);
	return digits;
}
bool isDigitInNumber(int digit,int number) {
	int i = 0;
	int digits = digitsInNumber(number);
	for (int i = 1,t = 1;i <= digits; ++i, t *= 10){
		if (digit == (number / t) % 10) return true;
	}
	return false;
}
int numberOfDifferentDigits(int number){
	bool digit[10];
	int digits = digitsInNumber(number);
	int result = 0;
	int temp;	// for opt.
	for (int i = 0;i < 10; ++i){digit[i] = false;}
	for (int i = 1,t = 1; i <= digits; ++i, t *= 10){
		temp = (number / t) % 10;
		if (!digit[temp]) {digit[temp] = true; ++result;}
	}
	return result;
}