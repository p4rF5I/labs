/*
author:soroka
task:For a given n, find all the smaller its perfect numbers (perfect is a number equal to the sum of all its divisors, not equal to the number itself).
tested: 33550336
*/
#include <iostream>
using namespace std;
bool isPerfect(long long number);
int main(){
	long long number;
	cout << "Enter number(>0) : ";
	if (!(cin >> number)){
		cout << "[ERROR]Incorrect input!\n"; 
		system("pause");
		return 1;
	}
	if (number <= 0) {
		cout << "It isn't natural number!\n";
	 	system("pause");
	 	return 1;
	}
	for (long long i = 1;i <= number; ++i){
		if (isPerfect(i)) {cout << i << ", ";}
	}
	cout << "\b\b.\n";
	system("pause");
	return 0;
}
// can be optimized
bool isPerfect(long long number){
	long long divisorsSum = 1;
	long long i;
	for (i = 2; i * i < number; ++i){
		if (!(number % i)) {
			divisorsSum += i + number / i;
		}
		if (divisorsSum > number){ return false;}
	}
	if (i * i == number){
		divisorsSum += i;
	}
	if (number == divisorsSum){
		return true;
	} else {
		return false;
	}
}