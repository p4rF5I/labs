/*
author: soroka
task: Отсортировать слова текста по возрастанию доли гласных букв (отношение количества гласных букв к общему количеству букв в слове).
tested: 
	"asd a s aas aaaas aaaaaas aaaaaaaaaas saaaaaaaaa saaa ssaa  sssssss "\
	"s"
	"    s    s"
	"123 as aa1 2ss aaaaa2 a"
+ also random-printed test;
+ their random big/min letter variations
*/
#include <iostream>
#include <cstring>
#define DEBUG
struct Word{
	char word[1024] = "\0";
	double key;
};
void setKey(Word &item);
unsigned int charactersInLine(const char *str,const char *symblos);
void selection (Word *arr, int length);
void swap(Word &a,Word &b);
using namespace std;
int main(){
	char text[1024];
	cin.getline(text,1024);
	char *p = text;
	int count = 0;
	while(*p != 0x0){
		while(*p != 0x0 && *p == 0x20){
			++p;}
		if(*p){
			++ count;
		}
		while(*p != 0x0 && *p != 0x20){
			++p;
		}
	}
	Word tempText[count]; //asdoasdhfiuhdfiasdf
	p = text;
	int wordLength = 0;
	for(int i = 0;i < count; ++i){
		while(*p != 0x0 && *p == 0x20){++p;}
		while(*p != 0x0 && *p != 0x20){
			++p;
			++wordLength;
		}
		strncpy(tempText[i].word, p - wordLength, wordLength);
		setKey(tempText[i]);
		wordLength = 0;
	}
	#ifdef DEBUG
		for(int i = 0;i < count; ++i){
			cout << tempText[i].word << '\t' << (double) tempText[i].key  << '\n';	
		}
	#endif
	selection(tempText,count);
	#ifdef DEBUG
		cout << "[DEBUG]AFTER\n";
	#endif
	text[0] = '\0';
	for(int i = 0;i < count; ++i){
		strcat (text,tempText[i].word);
		strcat (text," ");
		#ifdef DEBUG 
			cout << tempText[i].word << '\t' << (double) tempText[i].key  << '\n';
		#endif
	}
	cout << "answer: " << text << '\n';
	return 0;
}
unsigned int charactersInLine(const char *str,const char *symblos){
	unsigned int count = 0;
	int i = 0;
	int j = 0;
	while (symblos[i]){
		while(str[j]){
			if (symblos[i] == str[j]){
				++count; 
			}
			++j;
		}
		++i;
		j = 0;
	}
	return count;
}
void setKey(Word &item){
	item.key = (double)((double) charactersInLine(item.word,"AEIOUYaeiouy") / (double) strlen(item.word));
}
void selection (Word *arr, int length){
	int minIndex;
	for(int i = 0; i < length; ++i){
		minIndex = i;
		for (int j = i + 1; j < length; ++j){
			if(arr[minIndex].key > arr[j].key){
				minIndex = j;
			}
		}
			swap(arr[minIndex],arr[i]);
	}
}  
void swap(Word &a,Word &b){
	Word c = a;
	a = b;
	b = c;
}