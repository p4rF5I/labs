/*
author: soroka
task: Из текста удалить все слова заданной длины, начинающиеся на согласную.
tested: ")))) 000 aaaa www pp aap pw qw         as iiwww         asd            "
		"           asdd ww asdkp aksdp weijfiw ejfoiwepq  q www "
		"               w fooe a wi n oqs das"
		"q w e r t y u i o p [ ] \ a s d f g h j k l ; ' z x c v b n m , . / 1 2 3 5 6 7 8 9 0 - = _ + ( )"
		" Q W E R T Y U I O P [ ] A S D F G H J K L ; ' Z X C V B N M , ."
*/
#include <iostream>
#include <cstring>
bool isConsonant(char a);
using namespace std;
int main(){
	char text[1024];
	cin.getline(text,1024);
	char *p = text;
	int wordInText = 0;
	while(*p != 0x0){	//set wordInText
		while(*p != 0x0 && *p == 0x20){
			++p;}
		if(*p){
			++ wordInText;
		}
		while(*p != 0x0 && *p != 0x20){
			++p;
		}
	}
	p = text;
	char word[wordInText][1024]; //asdoisajfoijsfjios
	int wordLength = 0;
	for(int i = 0;i < wordInText; ++i){
		word[i][0] = '\0';
		while(*p != 0x0 && *p == 0x20){++p;} // set first word symbol
		while(*p != 0x0 && *p != 0x20){	//set last word symbol
			++p;
			++wordLength;
		}
		if(!isConsonant(*(p - wordLength))){
			strncpy(word[i], p - wordLength, wordLength);
			word[i][wordLength] = '\0';	//fix last sybol problems
		}
		// cout << word[i] << '|' << "\t" << wordLength << '\n';
		wordLength = 0;
	}
	text[0] = '\0';
	for(int i = 0;i < wordInText; ++i){
		if(word[i][0]){
			strcat (text,word[i]);
			strcat (text," ");
		}
	}
	int textLength = strlen(text);
	text[textLength ? (textLength - 1) : textLength] = '\0'; // delete last ' '
	cout << "answer: " << text << '|' <<  '\n';
	return 0;
}
bool isConsonant(char a){
	char consonant[] = "BCDFGHJKLMNPQRSTVXZWbcdfghjklmnpqrstvxzw";
	int i = 0;
	while(consonant[i]){
		if(consonant[i] == a) {return true;}
		++i; 
	}
	return false;
}