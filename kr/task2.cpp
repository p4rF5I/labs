/*
author: soroka
task: Дана матрица размера m x n. "Уплотнить" ее, удаляя из нее строки и столбцы, заполненные нулями.
tested: 
	4 4
	1 2 3 1		1 2 3 0					
	0 0 0 0		0 0 0 0		and another changes..
	2 2 0 0		2 2 0 0
	1 1 0 0		1 1 0 0
	4 4
	0 0 0 0		0 0 0 0
	0 0 0 0		0 0 0 0
	0 0 1 0		0 0 0 0
	0 0 0 0		0 0 0 0
+ exist input/output
+random tests
+ tests with incoreect input
*/
#include <iostream>
using namespace std;
template <class T> bool enterMatrix(T **matrix,int n, int m);
template <class T> void printMatrix(T **start, int n, int m);
template <class T> void deleteMatrix(T **matrix,int n,int m);
void deleteString (int ** &matrix, int &maxI, int &maxJ, int del);
void deleteColumn (int ** &matrix, int &maxI, int &maxJ, int del);
void fillMatrixWithRandomNumbers(int **matrix,int maxI, int maxJ, int max);
bool isZeroString(int **array, int n, int checkedInex);
bool isZeroColumn(int **array, int m, int checkedInex);
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount);
int main(){
	freopen("output.txt", "w", stdout);
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	int n, m = 0;
	if(!(cin >> n >> m) || n < 0 || m < 0){
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	int **array = new int*[n];
	for (int i = 0;i < n; ++i){
		array[i] = new int [m];
	}
	if(enterMatrix(array,n,m)){
		cout << "[ERROR]Inicalization error!\n";
		deleteMatrix(array,n,m);
		return 1;
	}
	for (int i = 0; i < n; ++i){
		if(isZeroString(array, m, i)){
			deleteString(array, n, m, i);
			--i;
		}
	}
	for (int i = 0; i < m; ++i){
		if(isZeroColumn(array, n, i)){
			deleteColumn(array, n, m, i);
			--i;
		}
	}
	printMatrix(array,n,m);
	fclose(stdin);
	fclose(stdout);
	deleteMatrix(array,n,m);
	return 0;
}
bool isZeroColumn(int **array,int n, int checkedInex){
	for(int i = 0;i < n; ++i){
		if(array[i][checkedInex]){
			return false;
		}
	}
	return true;
}
bool isZeroString(int **array, int m, int checkedInex){
	for(int i = 0; i < m; ++i){
		if(array[checkedInex][i]){
			return false;
		}
	}
	return true;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i < n; ++i){
		for(int j= 0;j < m; ++j){
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void printMatrix(T **start, int n, int m){
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < m; ++j){
			cout << start[i][j] << '\t';
		}
		cout << '\n';
	} 
}
template <class T> void deleteMatrix(T **matrix,int n,int m){
	for (int i = 0;i < n; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
}
void deleteString (int** &matrix, int &maxI, int &maxJ, int del){
	if (maxI == 0){return;}
	if (maxI <= del) {del = maxI - 1;}
	int **temp;
	--maxI;
	createMatrix(temp,maxI, maxJ);
	for (int i = 0; i < del; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i][j];
		}
	}
	for (int i = del; i < maxI; ++i){
		for (int j = 0; j < maxJ; ++j){
			temp[i][j] = matrix [i+1][j];
		}
	}
	++ maxI;
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	-- maxI;
	matrix = temp;
}
void deleteColumn (int** &matrix, int &maxI, int &maxJ, int del){
	if (maxJ == 0){return;}
	if (maxJ <= del) {del = maxJ - 1;}
	int **temp;
	--maxJ; 
	createMatrix(temp, maxI, maxJ);
	for (int i = 0; i < maxI; ++i){
		for (int j = 0; j < del; ++j){
			temp[i][j] = matrix[i][j]; 
		}
		for (int j = del; j < maxJ; ++j){
			temp[i][j] = matrix[i][j+1]; 
		}
	}
	for (int i = 0; i < maxI; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
	matrix = temp;
}
void fillMatrixWithRandomNumbers(int **matrix,int maxI, int maxJ, int max){
	srand(time(NULL));
	for (int i = 0; i < maxI; ++i){
		for(int j = 0; j < maxJ; ++j){
			matrix[i][j] = random() % max + 1;
		}
	}
}
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount){
        matrix = new T *[stringsAmount];
        for(int i = 0; i < stringsAmount;++i){
            matrix[i]= new T [columnsAmount];
        }  
}