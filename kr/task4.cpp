/*
author: soroka
task: Дана матрица размера m x n. Перестроить матрицу, переставляя в ней строки так,
 чтобы сумма элементов в строках полученной матрицы возрастала.
tested: 
2	4	5	1	|	12
2	4	5	11	|	22
2	3	1	24	|	30
7	8	1	2222	|	2238
+ random tests
+ exist input/output
+ tests with incoreect input
*/
#include <iostream>
using namespace std;
template <class T> bool enterMatrix(T **matrix,int n, int m);
template <class T> void printMatrix(T **start, int n, int m);
template <class T> void deleteMatrix(T **matrix,int n,int m);
template <class T> void swapString(T **matrix,int n , int m, int a, int b);
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount);
void selectionSort (int array[], int **matrix, int n, int m);
void fillMatrixWithRandomNumbers(int **matrix,int maxI, int maxJ, int max);
template <class T> void printMatrix(T **start,int *sum, int n, int m);
int main(){
	freopen("output.txt", "w", stdout);
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	int n, m = 0;
	if(!(cin >> n >> m) || n < 0 || m < 0){
		cout << "[ERROR]Size Inicalization error!\n";
		return 1;
	}
	int **matrix = new int*[n];
	for (int i = 0;i < n; ++i){
		matrix[i] = new int [m];
	}
	if(enterMatrix(matrix,n,m)){
		cout << "[ERROR]Inicalization error!\n";
		deleteMatrix(matrix,n,m);
		return 1;
	}
	int *sumOfStrings = new int[n];
	for(int i = 0; i < n; ++i){
		sumOfStrings[i] = 0;
		for(int j = 0; j < m; ++j){
			sumOfStrings[i] += matrix[i][j]; 
		}
	}
	selectionSort (sumOfStrings, matrix, n, m);
	printMatrix(matrix, sumOfStrings, n, m);
	delete[] sumOfStrings;
	deleteMatrix(matrix,n,m);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
template <class T> bool enterMatrix(T **matrix,int n, int m){
	for (int i = 0;i < n; ++i){
		for(int j= 0;j < m; ++j){
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		}
	}
	return false;	
}
template <class T> void printMatrix(T **start, int n, int m){
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < m; ++j){
			cout << start[i][j] << '\t';
		}
		cout << '\n';
	} 
}
template <class T> void printMatrix(T **start,int *sum, int n, int m){
	for (int i = 0; i < n; ++i){
		for (int j = 0; j < m; ++j){
			cout << start[i][j] << '\t';
		}
		cout << "|\t" <<  sum[i];
		cout << '\n';
	} 
}
template <class T> void deleteMatrix(T **matrix,int n,int m){
	for (int i = 0;i < n; ++i){
		delete[] matrix[i];
	}
	delete[] matrix;
}
void fillMatrixWithRandomNumbers(int **matrix,int maxI, int maxJ, int max){
	for (int i = 0; i < maxI; ++i){
		for(int j = 0; j < maxJ; ++j){
			matrix[i][j] = random() % max + 1;
		}
	}
}
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount){
        matrix = new T *[stringsAmount];
        for(int i = 0; i < stringsAmount;++i){
            matrix[i]= new T [columnsAmount];
        }  
}
template <class T> void swapString(T **matrix, int n , int m, int a, int b){
	if(a >= n || b >= n) {
		cout << "[ERROR] incorrect swap\n";
		return;
	}
	T temp;
	for(int i = 0; i < m; ++i){
		temp = matrix[a][i];
		matrix[a][i] = matrix[b][i];
		matrix[b][i] = temp;
	}
}
void selectionSort (int array[], int **matrix, int n, int m){
	int minIndex;
	int temp;
	for(int i = 0; i < n; ++i){
		minIndex = i;
		for (int j = i + 1; j < n; ++j){
			if(array[minIndex] > array[j]){
				minIndex = j;
			}
		}	
			temp = array[i];
			array[i] = array[minIndex];
			array[minIndex] = temp;
			swapString(matrix, n, m, i, minIndex);
	}
}  