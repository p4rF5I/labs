#ifndef rational_number_operations
#define rational_number_operations
#include "rationalNumber.h"
#include <iostream>
class RationalNumberOperatins{
	public:
		friend Rational;
		short int cmp(Rational &a, Rational &b);
		Rational add(Rational &a, Rational &b);
		Rational mul(Rational &a, Rational &b);
		Rational div(Rational &a, Rational &b);
};
#endif
