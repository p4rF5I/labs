/*
author: soroka
task:  Пусть даны две очереди X и Y, содержащие целые числа. 
Из каждой очереди одновременно извлекается по одному числу x и y соответственно. 
Если x<y, то число (x + y) помещается в конец очереди X, иначе число (x – y) помещается в конец очереди Y.
 Необходимо определить число шагов, через которые одна из очередей станет пустой.
 tested:
 (1 2 3 4) (4 3 2 1) -> 6
 (1 2 3 4) (1 2 2 3) -> 4
 (2 7 8 1 2 3) (1 2) -> 5
 (1) (1) -> 1
 (1 5 2 4) (2) -> 1
 () (3 4) -> r
 (4 4) (1 2 3 4) -> 2
 (1) (1 0 0) -> 1
*/
#include <iostream>
#include "queue.h"
using namespace std;
int main(){
	Queue<int> X;
	Queue<int> Y;
	int temp = 0;
	int amount = 0;
	while (amount <= 0){
		cout << "X amount: ";
		cin >> amount;
	}
	cout << "X: ";
	for (int i = 0; i < amount; ++i){
		cin >> temp;
		X.push(temp);
	}
	do{
		cout << "Y amount: ";
		cin >> amount;
	} while (amount <= 0);
	cout << "Y: ";
	for (int i = 0; i < amount; ++i){
		cin >> temp;
		Y.push(temp);
	}
	int step = 0;
	int x, y;
	while (!(X.isEmpty() || Y.isEmpty())){
		x = X.pop();
		y = Y.pop();
		if(y > x){
			cout << "x.push " << x+y << '\n';
			X.push(x+y);
		} else {
			cout << "y.push " << x-y << '\n';
			Y.push(x-y);
		}
		// cout << "step: " << step << '\t' <<  x << '\t' << y << '\n';
		// cout << "X.isEmpty(): " << X.isEmpty() << '\t' <<  "Y.isEmpty(): " << Y.isEmpty() << '\n';
		++step;
	}
	cout << "steps: " << step << '\n';
	system("pause");
	return 0;
}
