#include "headers/plurality.h"
#include <string>
std::ostream& operator<<(std::ostream &o, const Plurality &a){
	o << a.pl;
	return o;
}
std::istream& operator>>(std::istream &i,Plurality &a){
	std::string temp;
	i >> temp;
	if(temp.size() > a.maxSize){
		throw(Error("out of range(in op>>)"));
	}
	for(int c = 0; c < temp.size(); ++c){
		a.pl[c] = temp[c];
	}
	a.size = temp.size();
	return i;
}
int Plurality::inPl(const char t) const{
	int amount = 0;
	for(int i = 0; i < size; ++i){
		if(pl[i] == t){
			++amount;
		}
	}
	return amount;
}
Plurality::Plurality(const char *a):maxSize(DEFAULT_MAX_SIZE + 1){
	pl = new char[maxSize];
	int i = 0;
	while(a[i]){
		pl[i] = a[i];
		++i;
	}
	size = i;
	pl[i] = '\0';
}
Plurality::Plurality():maxSize(DEFAULT_MAX_SIZE + 1), size(0){
	pl = new char[maxSize];
	pl[0] = '\0';
}
Plurality::~Plurality(){
	delete[] pl;
}
bool Plurality::operator==(const Plurality &obj){
	if(size != obj.size){
		return false;
	}
	for(int i = 0;i < size; ++i){
		if(obj.pl[i] != pl[i]){
			return false;
		}
	}
	return true;
}
const Plurality Plurality::operator-(const Plurality &a) const{
	Plurality temp;
	size_t p = 0;
	int am;
	for(int i = 0; i < size; ++i){
		am = (this -> inPl(pl[i]) - a.inPl(pl[i]));
		if(am > 0 && !(temp.inPl(pl[i]))){
			temp.size += am;
			for(;am > 0; --am, ++p){
				temp.pl[p] = pl[i];
			}
			temp.pl[p] = '\0';
		}
	}
	return temp;
}
const Plurality& Plurality::operator=(const Plurality &a){
	size = a.size;
	for(int i = 0; i < size; ++i){
		pl[i] = a.pl[i];
	}
	pl[size] = '\0';
	return *this;
}
const Plurality Plurality::operator*(const Plurality &a) const{
	Plurality temp;
	size_t p = 0;
	int am;
	for(int i = 0; i < size; ++i){
		am = ((this -> inPl(pl[i]) > a.inPl(pl[i])))?a.inPl(pl[i]):(this -> inPl(pl[i]));	//max of 2 vku4enii
		if(am > 0 && !(temp.inPl(pl[i]))){
			temp.size += am;
			for(;am > 0; --am, ++p){
				temp.pl[p] = pl[i];
			}
			temp.pl[p] = '\0';
		}
	}
	return temp;
}
const Plurality Plurality::operator<(const Plurality &a) const{
	char temp[DEFAULT_MAX_SIZE + 1];
	if(size + a.size > maxSize){
		throw(Error("out of range(in op<)"));
	}
	size_t p = 0;
	for(int i = 0; i < size; ++i, ++p){
		temp[p] = pl[i];
	}
	for (int i = 0; i < a.size; ++i, ++p){
		temp[p] = a.pl[i]; 
	}
	temp[p] = '\0';
	return Plurality(temp);
}
const Plurality Plurality::operator+(const Plurality &a) const{
	Plurality temp1(*this - a);
	Plurality temp2(a - (*this));
	Plurality temp(*this * a);
	return (temp1 < temp2) < temp;
}