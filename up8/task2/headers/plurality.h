#pragma once
#define DEFAULT_MAX_SIZE 256
#include "error.h"
#include <iostream>
class Plurality{
private:
	char *pl;
	size_t size;
	const size_t maxSize;
	int inPl(const char t) const;
public:
	Plurality();
	Plurality(const char *a);
	Plurality(const Plurality &obj);
	~Plurality();
	friend std::ostream& operator<<(std::ostream &o, const Plurality &a);
	friend std::istream& operator>>(std::istream &i, Plurality &a);
	bool operator==(const Plurality &obj);
	const Plurality operator-(const Plurality &a) const;
	const Plurality operator*(const Plurality &a) const;
	const Plurality& operator=(const Plurality &a);
	const Plurality operator+(const Plurality &a) const;
	const Plurality operator<(const Plurality &a) const;
};