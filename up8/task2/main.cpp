/*
author: soroka 
task: pluarity class + operations.
tesed: (input.txt)
*/
#include "headers/error.h"
#include <iostream>
#include <string>
#include "headers/plurality.h"
using namespace std;
int main(){
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	// freopen("output.txt", "w", stdout);
	Plurality a, b;
	try{
		while((cin >> a) && (cin >> b)){
			// cin >> a;
			// cin >> b;
				cout << "a: " << a << " b: " << b << '\n';
				cout << "a-b: " << a-b << '\n';
				cout << "a*b: " << a*b << '\n';
				cout << "a+b: " << a+b << '\n';
				cout << "a<b: " << (a<b) << '\n';
				cout << "a==b: " << ((a == b)?"TRUE":"FALSE") << '\n';
				cout << "********************************************\n";
		}
	}
	catch(Error &a){
		cout << a.what() << '\n';
	}
	fclose(stdin);
	// fclose(stdout);
	system("pause");
	return 0;
}