#include <iostream>
#include <fstream>
#include "headers/userInterface.h"
#include "headers/train.h"
using namespace std;
int main(int argc, char *argv[]){
	system("clear || cls");
	std::vector<Train> tr;
	UserInterface ui(tr);
	ui.openInputFile();
	// system("clear || cls");
	bool isExit = false;
	while(!isExit){
		ui.printMenu();
		ui.makeChoose();
		// system("clear || cls");
		// cout << "++++++++++++++++++++++++++++++++++++\n";
		isExit = ui.doAction();
		cout << "++++++++++++++++++++++++++++++++++++\n";
	}
	system("clear || cls");
	system("pause");
	return 0;
}