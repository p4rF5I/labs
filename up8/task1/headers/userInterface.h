#pragma once
#include <fstream>
#include <algorithm>
#include "train.h"
#include <iostream>
#include <string>
#include <vector>
#define UNDEFINED_CASE -1
bool destCmp(const Train&, const Train&);
bool timeCmp(const Train&, const Train&);
class UserInterface{
private:
	std::vector<Train> &trains;
	void getDataFromInputFile();
	void printForCertainDest() const;
	void printFromCertainTime() const;
	int lastChoose;
	int choose;
	std::ifstream inFile;
	std::string inputFileName;
public:
	void openInputFile();
	~UserInterface();
	UserInterface(std::vector<Train> &tr);
	int makeChoose();
	void printMenu() const;
	int doAction();
};