#pragma once 
#include <iostream>
#include <string>
#include <cctype>
#include "time.h"
class Train{
private:
	std::string destination;
	int id;
	Time time;
	// Train::Train();
public:
	friend std::ostream& operator<<(std::ostream&, const Train &);
	friend std::istream& operator>>(std::istream&, Train &);
	friend bool destCmp(const Train&, const Train&);
	friend bool timeCmp(const Train&, const Train&);
	Train(int i, std::string, std::string);
	Train();
	std::string getDestination() const;
	int getId() const;
	const Time getDepTime() const;
};