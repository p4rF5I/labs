#pragma once
#include <iostream>
#include <string>

class Time {
private:
	unsigned int min;
	unsigned int hour;
public:
	Time(std::string);
	Time();
	Time(const Time &);
	friend std::ostream& operator<<(std::ostream&, const Time &);
	friend std::istream& operator>>(std::istream&, Time &);
	bool operator>(const Time &) const;
	bool operator<(const Time &t) const;
	bool operator==(const Time &t) const;
	bool operator>=(const Time &t) const;
	bool operator<=(const Time &t) const;
	operator std::string() const;
	const Time& operator=(const Time &);
	unsigned int gm() const;
	unsigned int gh() const;
};