#include "headers/userInterface.h"
using namespace std;
UserInterface::~UserInterface(){
	if(inFile.is_open()){
		inFile.close();
	}
}
UserInterface::UserInterface(std::vector<Train> &tr):trains(tr){}
void UserInterface::openInputFile(){
	do{
		cout << "Enter file name or full dirrectory of input file: ";
		cin >> inputFileName;
		inFile.open("data/" + inputFileName, fstream::in);
		if(!inFile.is_open()){
			inFile.open(inputFileName, fstream::in);
		}
	} while (!inFile.is_open());
	getDataFromInputFile();
}
void UserInterface::getDataFromInputFile(){
	if(!inFile.is_open()){
		throw("InputFileNotOppend");
	}
	Train temp;
	while(!inFile.eof()){
		// cout << "here\n";
		inFile >> temp;
		trains.emplace_back(temp);
	}
}
void UserInterface::printMenu() const{
	cout << "1 - change file\n";
	cout << "2 - sort by time\n";
	cout << "3 - sort by destination\n";
	cout << "4 - print from certain time\n";
	cout << "5 - print to certain dest\n";
	cout << "6 - print all\n";
	cout << "0 - выйти из приложения\n";
}
int UserInterface::makeChoose(){
	cout << "Enter your choose: ";
	lastChoose = choose;
	cin >> choose;
	return choose;
}
int UserInterface::doAction(){
	switch(choose){
		case 0: 
			return 1;
			break;
		case 1:
			trains.clear(); 
			do{
				openInputFile();
			} while(!inFile.is_open());
			// getDataFromInputFile();
			break;
		case 2:
			sort(trains.begin(),trains.end(),timeCmp);
			break;
		case 3:
			sort(trains.begin(),trains.end(),destCmp);
			break;
		case 4:
			printFromCertainTime();
			break;
		case 5:
			printForCertainDest();
			break;
		case 6:
			for(int i = 0; i < trains.size(); ++i){
				cout << trains[i] << '\n';
			}
			break;
		default:
			cout << "[ERROR] incorrect operation choosed.\n";
			break;
	} 
	return 0;
}
void UserInterface::printForCertainDest() const {
	string dest;
	unsigned int am = 0;
	cout << "Enter destination: ";
	cin >> dest;
	for(int i = 0; i < trains.size(); ++i){
		if(trains[i].getDestination() == dest){
			cout << trains[i] << '\n';
			++am;
		}
	}
	if(am == 0){
		cout << "no trains;\n";
	}
}
void UserInterface::printFromCertainTime() const{
	unsigned int am = 0;
	Time time;
	cout << "Enter time(\"h:min\"): ";
	cin >> time;
	for(int i = 0; i < trains.size(); ++i){
		if(trains[i].getDepTime() >= time){
			cout << trains[i] << '\n';
			++am;
		}
	}
	if(am == 0){
		cout << "no trains;\n";
	}
}