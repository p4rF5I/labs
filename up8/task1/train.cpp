#include "headers/train.h"
Train::Train(int i, std::string dest, std::string dTime):id(i), destination(dest), time(dTime){}
Train::Train():id(0),destination("null"),time("24:60"){}
std::string Train::getDestination() const{
	return destination;
}
int Train::getId() const{
	return id;
}
const Time Train::getDepTime() const{
	return time;
}
std::ostream& operator<<(std::ostream& o, const Train &obj){
	o << obj.id << " in " << obj.destination << " at " << obj.time;
	return o;
}
std::istream& operator>>(std::istream& i, Train &obj){
		try{
			i >> obj.id >> obj.destination >> obj.time;
		}
		catch(...){
			std::cout << "[ERR0R] in reading file.\n";
		}
	return i;
}
bool destCmp(const Train& a, const Train& b){
	for(int i = 0; i < a.destination.length() && i < b.destination.length(); ++i){
		if(tolower(a.destination[i]) == tolower(b.destination[i])){continue;}
		if (tolower(a.destination[i]) < tolower(b.destination[i])){
			return true;
		} else {
			return false;
		}
	}
	if(a.destination.length() > b.destination.length()){
		return false;
	} else {
		return true;
	}
}
bool timeCmp(const Train& a, const Train& b){
		return a.time > b.time;
}