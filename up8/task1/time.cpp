#include "headers/time.h"
unsigned int Time::gm() const{
	return min;
}
unsigned int Time::gh() const{
	return hour;
}
bool Time::operator>(const Time &t) const{
	if(hour == 24){
		return false;
	}
	if(t.hour == 24){
		return true;;
	}
	if(hour > t.hour){
		return true;
	} else {
		if(hour == t.hour){
			if(min > t.min){
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
bool Time::operator<(const Time &t) const{
	if(t.hour == 24){
		return false;;
	}
	if(hour == 24){
		return true;
	}
	if(hour < t.hour){
		return true;
	} else {
		if(hour == t.hour){
			if(min < t.min){
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
bool Time::operator==(const Time &t) const{
	return (hour == t.hour) && (min == t.min);
}
bool Time::operator>=(const Time &t) const{
	return ((*this) > t)?true:(t == *this);
}
bool Time::operator<=(const Time &t) const{
	return ((*this) < t)?true:(t == *this);
}
Time::operator std::string() const{
	if(hour < 24 && min < 60){
		return std::to_string(hour) + (std::string)":" + std::to_string(min);
	} else {
		return "undef";
	} 
}
Time::Time(const Time &t){
	hour = t.hour;
	min = t.min;
}
Time::Time(){
	min = 60;
	hour = 24;
}
// Time operator
Time::Time(const std::string time){
	size_t div = time.find(':');
	if(div == std::string::npos){
		hour = 24;
		min = 60;
		return;
		// throw ("cant find division sign\n");
	}
	try{
		hour = stoi(time.substr(0,div));
		min = stoi(time.substr(div + 1));
	}
	catch(...){
		hour = 24;
		min = 60;
		return;
	}
	if(hour > 23 || min > 59){
		hour = 24;
		min = 60;
	}
}
const Time& Time::operator=(const Time &t){
	hour = t.hour;
	min = t.min; 
	return *this;
}
std::ostream& operator<<(std::ostream& o, const Time &t){
	if(t.hour < 24 && t.min < 60){
		o << t.hour << ':';
		if(t.min >= 10){
			o << t.min;
		} else {
			o << '0' << t.min;
		}
	} else{
		o << "undef";
	}
}
std::istream& operator>>(std::istream& i, Time &t){
	std::string temp;
	i >> temp;
	t = Time(temp);
	return i;
}