/*
author: soroka
task: Дан текст. В выходной файл записать только те буквы слов,
которые встречаются в словах текста только один раз.
tested:
(AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz && adding/deleting random symbol)
*/
#include <iostream>
#include <cstring>
using namespace std;
int main(){
	freopen("output.txt", "w", stdout);
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	char letters[26];
	for(int i = 0; i < 26; ++i) {letters[i] = 0;}
	char temp;
	while((cin >> temp)){
		if(temp >= 0x41 && temp <= 0x5A){
			temp = temp ^ 0x20; // just lowwer symbol in temp,if need
		}
		if(temp >= 0x61 && temp <= 0x7A){
			if(letters[temp - 0x61] < 2){	//protect from overflow
				++letters[temp - 0x61];
			}
		}
	}
	for(int i = 0; i < 26; ++i){
		if(letters[i] == 1){
			cout << (char)(i + 0x61) << ' ';
		}
	}
	cout << '\n';
	fclose(stdin);
	fclose(stdout);
	return 0;
}