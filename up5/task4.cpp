/*
author: soroka
task:Дан текст, состоящий из предложений. Найти слово,
которое встречается во всех предложениях текста, или сообщить,
что такого слова нет.
tested: 3 wiki-articles && tests like:
"asd. asd."
"asd 1. asd 1."
"asd. qwe. asd qwe."
"asd qwe. qwe. asd qwe."
"asd qwe zxc."
"q w e r t y u i o p. q a s d f g h j k l l."
- all test passed!
*/
#include <iostream>
#include <cstring>
struct Node {
	char *str = NULL;
	Node *next = NULL;
	int used = 0;
	int usedInCorrectSentense = 0;
};
struct Stack{
	Node *top = NULL;
}myStack;
void push(const char *str);
void pop();
void setWord(char *word);	//delete symbol from begin and end of word
void setUsed();	//usedInCorrectSentense = 0 and increase used,if necessary
bool strcmpHL(const char *a,const char *b);	//strcmp, high-low register dosen't metter
void removeUnused(int sentencesAnalized);	//if word unused, we delete it from out stack
bool isNewSentence(const char *word);
bool isWordInStack(const char *word);
void markWord(const char* word);	//increase usedInCorrectSentense
using namespace std;
int main(){
	freopen("output.txt", "w", stdout);
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	char word[1024] = "\0";
	cin >> word;
	int sentences = 1;
	// first sentence analysis
	while(!isNewSentence(word)){
		setWord(word);
		if(!isWordInStack(word)){
			push(word);
		}
		if(!(cin >> word)){
			sentences = 0;
			break;
		}
	}
	setUsed();
	isNewSentence("aa");	//reset sentence counter
	if(sentences != 0){
		do{	//use do-whie because the word is already readed
			if(isNewSentence(word)){
				++sentences;
				setUsed();
				removeUnused(sentences);
			}
			setWord(word);
			markWord(word);
	
		}while(cin >> word);
		// last word in text
		++sentences;
		setUsed();
		removeUnused(sentences);}
	if(myStack.top == NULL){
		cout << "No such word;\n";
	} else {
		while(myStack.top != NULL){
			pop();
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
void push(const char *str){
	if(str[0] == '\0'){return;}
	if(myStack.top == NULL){
		myStack.top = new Node;
		myStack.top -> str = new char[strlen(str) + 1];
		myStack.top -> usedInCorrectSentense = 1;
		strcpy(myStack.top -> str,str);
	} else{
		Node *temp = new Node;
		temp -> str = new char[strlen(str) + 1];
		temp -> usedInCorrectSentense = 1;
		strcpy(temp -> str,str);
		temp -> next = myStack.top;
		myStack.top = temp;
	}
}
void pop(){
	if(myStack.top != NULL){
		cout << myStack.top -> str << '\n';
		delete[] myStack.top -> str; 
		Node *tempPtr = myStack.top -> next;
		delete myStack.top;
		myStack.top = tempPtr;
	} else {
		cout << "\n**********************************\n"
			 << "******[ERROR] Stack is free!******\n"
			 << "**********************************\n";
	}
}
bool strcmpHL(const char *a,const char *b){	//strcmp, high-low register dosen't metter
	if(strlen(a) != strlen(b)){
		return false;
	}
	char tempA, tempB;
	for(int i = 0; a[i]; ++i){
		tempA = a[i];
		tempB = b[i];
		if(tempA >= 0x41 && tempA <= 0x5A){
			tempA = tempA ^ 0x20;
		}
		if(tempB >= 0x41 && tempB <= 0x5A){
			tempB = tempB ^ 0x20;
		}
		if(tempA != tempB){
			return false;
		}
	} 
	return true;
}
void setWord(char *word){	//delete symbol from begin and end of word
	int i =0;
	while(word[i]){
		if(!((word[i] >= 33 && word[i] <= 47) || (word[i] >= 58 && word[i] <= 64) 
			|| (word[i] >= 91 && word[i] <= 96) || (word[i] >= 123 && word[i] <= 126))){
			break;
		}
	++i;
	}
	int temp = strlen(word) - i;
	strncpy(word,word + i, temp);
	for(i = temp - 1; 0 <= i; --i){
		if(!((word[i] >= 33 && word[i] <= 47) || (word[i] >= 58 && word[i] <= 64) 
			|| (word[i] >= 91 && word[i] <= 96) || (word[i] >= 123 && word[i] <= 126))){
			break;
		}
	}
	word[i+1] = '\0';
}
void removeUnused(int sentencesAnalized){	//if word unused, we delete it from out stack
	Node *prev = myStack.top;
	while((myStack.top != NULL) && (myStack.top -> used != sentencesAnalized)){
			delete[] myStack.top -> str;
			prev = myStack.top -> next;	//we use prev as temp var
			delete myStack.top;
			myStack.top = prev;
	}
	Node *correct;
	if(myStack.top != NULL){
		correct = myStack.top -> next;
	} else {
		correct = NULL;
	}
	while(correct != NULL){
		if(correct -> used != sentencesAnalized){
			delete[] correct -> str;
			prev -> next = correct -> next;
			delete correct;
			correct = prev -> next;
		} else {
			prev = correct;
			correct = correct -> next;
		}
	}
}
bool isNewSentence(const char *word){
	static char isNewSentence = 0; // 0 - no, 1 - with next word
	int length = strlen(word) - 1;
	if(isNewSentence){
		if(word[length] == '!' || word[length] == '?' || word[length] == '.'){
			isNewSentence = 1;
		} else {
			isNewSentence = 0;
		}
		return true;
	} else {
		if(word[length] == '!' || word[length] == '?' || word[length] == '.'){
			isNewSentence = 1;
		}
		return false;
	}
}
bool isWordInStack(const char *word){
	Node *tempPtr = myStack.top;
	while(tempPtr != NULL){	
		if(strcmpHL(tempPtr -> str,word)){
			return true;
		} else {
			tempPtr = tempPtr -> next;
		}
	}
	return false;
}
void markWord(const char* word){
	Node *tempPtr = myStack.top;
	while(tempPtr != NULL){	
		if(strcmpHL(tempPtr -> str,word)){
			++(tempPtr -> usedInCorrectSentense);
			return;
		} else {
			tempPtr = tempPtr -> next;
		}
	}
}
void setUsed(){
	Node *tempPtr = myStack.top;
	while(tempPtr != NULL){	
		if(tempPtr -> usedInCorrectSentense != 0){
			++(tempPtr -> used);
			tempPtr -> usedInCorrectSentense = 0;
		}
		tempPtr = tempPtr -> next;
	}
}