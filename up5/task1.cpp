/*
author: soroka
task: Даны два слова. Для каждой буквы первого слова (а) в том
числе для повторяющихся в этом слове букв; б) повторяющиеся
буквы первого слова не рассматривать) определить, входит ли она во
второе слово.
tested: 
2aw qwe
[ERROR]Is it a word?
qwerty qwerty    qqwer qwerty     qwerty asdfg    qwerty qwerty 
A: Y Y Y Y Y Y   A: Y N Y Y Y     A: N N N N N N  A: Y Y Y Y Y Y 
B: Y Y Y Y Y Y   B: Y Y Y	 B:   N N N N N N 	  B: N N N N N N 
qwerty asedf	qwertyw asdfgw		qqqqqqwwwweeeerrrttttyyy qwerty    
A: N N Y N N N 	A: N Y N N N N N 	A: Y N N N N N Y N N N Y N N N Y N N Y N N N Y N N 
B: N N Y N N N 	B: N N N N N 		B: 
*/
#include <iostream>
#include <cstring>
using namespace std;
bool isWord(char *str);
int main(){
	#ifdef work_with_files
		freopen("output.txt", "w", stdout);
		if(!freopen("input.txt", "r", stdin)){
			cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
			fclose(stdout);
			return 1;
		}
	#endif
	char a[1024] = "\0";
	char b[1024] = "\0";
	if(!(cin >> a >> b)){
		cout << "[ERROR]String inicalization error!\n";
		return 1;
	}
	if(!isWord(a) || !isWord(b)){
		cout << "[ERROR]Is it a word?\n";
		return 1;
	}
	unsigned short int lettersA[26];
	unsigned short int lettersB1[26];	//we use 2 lettersB because we will need to change them,
	unsigned short int lettersB2[26];	//allocate 26*2 more profitable than refilling
	for (int i = 0; i < 26; ++i){
		lettersA[i] = lettersB1[i] = lettersB2[i] = 0;
	}
	for(int i = 0; b[i]; ++i){
		if(b[i] >= 0x41 && b[i] <= 0x5A){
			b[i] = b[i] ^ 0x20;	//lowwer
		}
		++ lettersB1[b[i] - 0x61];
		++ lettersB2[b[i] - 0x61];
	}
	int temp;
	cout << "A: ";	// a case
	for(int i = 0; a[i]; ++i){
		if(a[i] >= 0x41 && a[i] <= 0x5A){
			a[i] = a[i] ^ 0x20;	//lowwer
		}
		temp = a[i] - 0x61;
		if(lettersB1[temp]){
			cout << "Y ";
			--lettersB1[temp];
		} else {
			cout << "N ";
		}
		++ lettersA[temp];
	}
	cout << "\nB: ";
	for(int i = 0; a[i]; ++i){
		temp = a[i] - 0x61;
		if(lettersA[temp] == 1){
			if(lettersB2[temp]){
				cout << "Y ";
				--lettersB2[temp];
			} else {
				cout << "N ";
			}
		}
	}
	cout << '\n';
	#ifdef work_with_files
		fclose(stdin);
		fclose(stdout);
	#endif
	return 0;
}
bool isWord(char *str){
	// char letters[] = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
	for(int i = 0; str[i]; ++i){
		if((str[i] < 0x41 || str[i] > 0x5A) && (str[i] < 0x61 || str[i] > 0x7A)){
			return false;
		}
	}
	return true;
}
