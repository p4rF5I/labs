/*
author: soroka
task: Дан текст. Посчитать частоту слов в тексте.
tested: every function tested on special set;
program: on random eng-wiki
*/
#include <iostream>
#include <cstring>
struct Node {
	char *str = NULL;
	Node *next = NULL;
	int count = 0;
};
struct Stack{
	Node *top = NULL;
}myStack;
void push(const char *str);
void pop();
void setWord(char *word);	//delete symbol from begin and end of word
bool strcmpHL(const char *a,const char *b);	//strcmp, high-low register dosen't metter
using namespace std;
int main(){
	freopen("output.txt", "w", stdout);
	if(!freopen("input.txt", "r", stdin)){
		cout << "[ERROR] file \"input.txt\" doesn't exist!\n";
		fclose(stdout);
		return 1;
	}
	char word[1024] = "\0";
	char* temp;
	Node *tempPtr = NULL;
	while((cin >> word)){
		setWord(word);
		tempPtr = myStack.top;
		while(tempPtr != NULL){	
			if(strcmpHL(tempPtr -> str,word)){
				++(tempPtr -> count);
				break; 
			} else {
				tempPtr = tempPtr -> next;
			}
		}
		if(tempPtr == NULL){
			push(word);
		}
	}
	while (myStack.top != NULL){
		pop();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
void push(const char *str){
	if(str[0] == '\0'){return;}
	if(myStack.top == NULL){
		myStack.top = new Node;
		myStack.top -> str = new char[strlen(str) + 1];
		++(myStack.top -> count);
		strcpy(myStack.top -> str,str);
	} else{
		Node *temp = new Node;
		temp -> str = new char[strlen(str) + 1];
		++(temp -> count);
		strcpy(temp -> str,str);
		temp -> next = myStack.top;
		myStack.top = temp;
	}
}
void pop(){
	if(myStack.top != NULL){
		cout << myStack.top -> str << ": " << myStack.top -> count << '\n';
		delete[] myStack.top -> str; 
		Node *tempPtr = myStack.top -> next;
		delete myStack.top;
		myStack.top = tempPtr;
	} else {
		cout << "\n**********************************\n"
			 << "******[ERROR] Stack is free!******\n"
			 << "**********************************\n";
	}
}
bool strcmpHL(const char *a,const char *b){	//strcmp, high-low register dosen't metter
	if(strlen(a) != strlen(b)){
		return false;
	}
	char tempA, tempB;
	for(int i = 0; a[i]; ++i){
		tempA = a[i];
		tempB = b[i];
		if(tempA >= 0x41 && tempA <= 0x5A){
			tempA = tempA ^ 0x20;
		}
		if(tempB >= 0x41 && tempB <= 0x5A){
			tempB = tempB ^ 0x20;
		}
		if(tempA != tempB){
			return false;
		}
	} 
	return true;
}
void setWord(char *word){	//delete symbol from begin and end of word
	int i =0;
	while(word[i]){
		if(!((word[i] >= 33 && word[i] <= 47) || (word[i] >= 58 && word[i] <= 64) 
			|| (word[i] >= 91 && word[i] <= 96) || (word[i] >= 123 && word[i] <= 126))){
			break;
		}
	++i;
	}
	int temp = strlen(word) - i;
	strncpy(word,word + i, temp);
	for(i = temp - 1; 0 <= i; --i){
		if(!((word[i] >= 33 && word[i] <= 47) || (word[i] >= 58 && word[i] <= 64) 
			|| (word[i] >= 91 && word[i] <= 96) || (word[i] >= 123 && word[i] <= 126))){
			break;
		}
	}
	word[i+1] = '\0';
}