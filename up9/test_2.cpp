// g++ test.cpp triangleFunc.h -lboost_unit_test_framework
#include <iostream>
#include "lab9t2.h"
#define BOOST_TEST_MODULE MyModule
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
BOOST_AUTO_TEST_SUITE(negative_cases)
BOOST_AUTO_TEST_CASE(all){
	BOOST_CHECK(calc("5") == '5');
	BOOST_CHECK(calc("0") == '0');
	BOOST_CHECK(calc("9") == '9');
	BOOST_CHECK(calc("n34") == '3');
	BOOST_CHECK(calc("xn637") == '7');
	BOOST_CHECK(calc("xn980") == '8');
	BOOST_CHECK(calc("x35") == '5');
	BOOST_CHECK(calc("x0n98") == '8');
	BOOST_CHECK(calc("nx563") == '3');
	BOOST_CHECK(calc("nx123") == '2');
	BOOST_CHECK(calc("n3x56") == '3');
	BOOST_CHECK(calc("n3x12") == '2');
	BOOST_CHECK(calc("nx12x34") == '2');
	BOOST_CHECK(calc("nx34x12") == '2');
	BOOST_CHECK(calc("xn12n34") == '3');
	BOOST_CHECK(calc("xn34n12") == '3');
	BOOST_CHECK(calc("xx12x34") == '4');
	BOOST_CHECK(calc("nn12n34") == '1');
	BOOST_CHECK(calc("nn56x34") == '4');
	BOOST_CHECK(calc("nx56n34") == '3');
	BOOST_CHECK(calc("nn122") == '1');
	BOOST_CHECK(calc("xn325") == '5');


}
BOOST_AUTO_TEST_SUITE_END()