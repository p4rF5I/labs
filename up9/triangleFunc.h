bool isTriangle(int a, int b, int c){
	return (a > 0 && b > 0 && c > 0) && (a > c - b && a > b - c && c > a - b);
}
bool isIsosceles(int a, int b, int c){
	return isTriangle(a,b,c) && (a == b || a == c || b == c); 
}
bool isEquilateral(int a, int b, int c){
	return isTriangle(a,b,c) && a == b && b == c;
}