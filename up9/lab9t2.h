#include "stack.h"
#include <cstring>
char max(char a, char b){
	return (a>b)?a:b;
}
char min(char a, char b){
	return (a<b)?a:b;
}
bool isDigit(char a){
	if(a >= 0x30 && a <= 0x39){
		return true;
	}
	return false;
}
int calc(const char * str){
	Stack<char> operand;
	int i = strlen(str);
	while(i >= 0){
		if(isDigit(str[i])){
			operand.push(str[i]);
			--i;
			continue;
		}
		if(str[i] == 'x'){
			operand.push(max(operand.pop(),operand.pop()));
			--i;
			continue;
		}
		if(str[i] == 'n'){
			operand.push(min(operand.pop(),operand.pop()));
			--i;
			continue;
		}
		--i;
	}
	return operand.pop();
}