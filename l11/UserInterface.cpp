#include "headers/UserInterface.h"
using namespace std;
int UserInterface::doAction(){
	try{
		switch(choose){
			case 0: 
				return 1;
				break;
			case 1:
				openInputFile();
				getDataFromInputFile();
				break;
			case 2:
				data.calculate();
				break;
			case 3:
				openOutputFile();
				printResultInFile();
				break;
			default:
				cout << "[ERROR] incorrect operation choosed.\n";
				break;
		}
	}
	catch(Error a){
		cout << "[ERROR] " << a.getErrorMessage() << '\n';
	} 
	return 0;

}
void UserInterface::printMenu(){
	cout << "1 - загрузить данные из файла \n";
	cout << "2 - вычислить результат\n";
	cout << "3 - выгрузить результат в файл\n";
	cout << "0 - выйти из приложения\n";
}
int UserInterface::makeChoose(){
	cout << "Enter your choose: ";
	lastChoose = choose;
	cin >> choose;
	return choose;
}
void UserInterface::openInputFile(){
	do{
		cout << "Enter file name or full dirrectory of input file('n' to cannel): ";
		cin >> inputFileName;
		if(inputFileName == "n"){
			return;
		}
		inFile.open("data/" + inputFileName, fstream::in);
		if(!inFile.is_open()){
			inFile.open(inputFileName, fstream::in);
		}
	} while (!inFile.is_open());
}
void UserInterface::getDataFromInputFile(){
	if(!inFile.is_open()){
		throw(InputFileNotOppend());
	}
	unsigned long long int lineID = 0;
	string buff;
	while(!inFile.eof()){
		getline(inFile,buff);
		++lineID;
		try{
			data.add(Expression(buff));
		}
		catch(Error a){
			// cout << "catched Error" << a.getErrorMessage() << '\n';
			incorrectInputLog.note(to_string(lineID) + (string)" | " + a.getErrorMessage() + buff);
		}
	}
}
UserInterface::~UserInterface(){
	if(inFile.is_open()){
		inFile.close();
	}
	if(outFile.is_open()){
		outFile.close();
	}
}
void UserInterface::printResultInFile(){
	if(!outFile.is_open()){
		throw(OutputFileNotOppend());
	}
	data.print(outFile);
	outFile.close();
	outFile.open("data/" + outputFileName, fstream::out | fstream::app);
}
void UserInterface::openOutputFile(){
	if(data.getResultSize() == 0){
		throw(EmptyResultData());
	}
	do{
		cout << "Enter output file name('n' to cannel): ";
		cin >> outputFileName;
		if(outputFileName == "n"){
			return;
		}
		outFile.open("data/" + outputFileName, fstream::out | fstream::app);
		if(!outFile.is_open()){
			outFile.open(outputFileName, fstream::out | fstream::app);
		}
	} while (!outFile.is_open());
}
UserInterface::UserInterface(std::vector<Expression> &d,
		std::vector<Rational> &res, std::string l):
		data(d,res,incorrectInputLog), incorrectInputLog(DEFAULT_INCORRECT_LOG){}