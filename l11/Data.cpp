#include "headers/Data.h"
Data::Data(std::vector<Expression> &i, std::vector<Rational> &r, Log &l):initialData(i), result(r), incorrectInputLog(l){}
void Data::calculate(){
	// std::cout << "idSIZE" << initialData.size() << '\n';
	if(initialData.empty()){
		// std::cout << "here" << '\n';
		throw(EmptyConditionData());
	}
	for(int i = 0 ; i < initialData.size(); ++i){
		try{
			result.emplace_back(initialData[i].exec());
		}
		catch(Error a){
			incorrectInputLog.note(initialData[i].expr_to_str() + (std::string)" | " + a.getErrorMessage());
		}
	}
}
void Data::add(const Expression &a){
	// std::cout << "ger\n";
	initialData.emplace_back(a);
	// std::cout << "idSIZE2" << initialData.size() << '\n';
}
void Data::print(std::ofstream& o){
	for(int i = 0; i < result.size(); ++i){
		result[i].print(o);
	}
}
unsigned int Data::getResultSize(){
	return result.size();
}