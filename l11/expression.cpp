#include "headers/expression.h"
using namespace std;
const Rational Expression::exec(){
	switch (operation){
		case '+':
			return a + b;
			break;
		case '-':
			return a - b;
			break;
		case '*':
			return a * b;
			break;
		case '/':
			return a / b;
			break;
		case 'n':
			return a;
			break;
		default:
			throw(UnexpectedOperatin());
	}
}
void Expression::print() const{
	a.print();
	cout << ' ' << operation << ' ';
	b.print();
}
Expression& Expression::operator=(Expression& r){
	a = r.a;
	b = r.b;
	operation = r.operation;
}
size_t Expression::findOperatin(const string str){
	size_t op = str.find(" / ");
	if(op == string::npos){
		op = str.find(" + ");
		if(op == string::npos){
			op = str.find(" - ");
			if(op == string::npos){
				op = str.find(" * ");
			}
		}
	}
	if(op != string::npos){
		return op + 1;
	} else {
		return op;
	}
}
Expression::Expression(const string str){
	if(str.size() == 0){
		throw(NullStr());
	}
	size_t op = findOperatin(str);
	// Expression t;
	if(op != string::npos){
		a = Rational(str.substr(0, op - 1));
		b = Rational(str.substr(op + 1));
		operation = str[op];
	} else {
		a = Rational(str);
		operation = 'n';
	}
}
/*ostream& operator<<(ostream& os, const Expression& e){
	if(e.operation != 'n'){
		os << e.a << ' ' << e.operation << ' ' << e.b;
	} else {
		os << e.a;
	}
	return os;
}*/
string Expression::expr_to_str(){
	string temp = to_string(a.getNumerator());
	if(a.getDenominator() != 1){
		temp += to_string(a.getDenominator());
	}
	if(operation != 'n'){
		temp += ' ';
		temp += operation;
		temp += ' ';
		temp += to_string(b.getNumerator());
		if(b.getDenominator() != 1){
			temp += to_string(a.getDenominator());
		}
	}
	return temp;
}
Expression::Expression(const Rational a_,const Rational b_ ,const char operation_):
		a(a_), b(b_), operation(operation_){}
Expression::Expression(const Rational a_):a(a_), operation('n'){}