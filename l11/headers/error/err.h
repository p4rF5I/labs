#pragma once
#include "/home/noname/labs/l11/headers/log.h"
#include <string>
#define DEFAULT_LOG_FILE "log/err.log"
// class Error;
class Error{
private:
	std::string message;
	Log errorsLogFile;
public:
	Error(const std::string mes, const std::string logName = DEFAULT_LOG_FILE);
	virtual void addNote(const std::string prefix);
	virtual std::string getErrorMessage();
};
class ExpressionError : public Error{
public:
	ExpressionError(const std::string mes = "uncnown error",const std::string logName = DEFAULT_LOG_FILE);
};
class UnexpectedOperatin : public ExpressionError {
public:
	UnexpectedOperatin(const std::string mes = "");
};
class NullStr : public ExpressionError {
public:
	NullStr(const std::string mes = "");
};
class LogError : public Error{
public:
	LogError(const std::string mes = "uncnown error",const std::string logName = DEFAULT_LOG_FILE);
};
class CantOpenLog : public LogError{
public:
	CantOpenLog(const std::string mes = "");
};
class LogFileNotOppend : public LogError{
public:
	LogFileNotOppend(const std::string mes = "");
};
class RationalError : public Error{
public:
	RationalError(const std::string mes = "uncnown error",const std::string logName = DEFAULT_LOG_FILE);
};
class DivByZero : public RationalError {
public:
	DivByZero(const std::string mes = "");
};
class BadMinus : public RationalError {
public:
	BadMinus(const std::string mes = "");
};
class ExcessSymbols : public RationalError {
public:
	ExcessSymbols(const std::string mes = "");
};
class TooMoreDividingOperations : public RationalError {
public:
	TooMoreDividingOperations(const std::string mes = "");
};
class UserInterfaceError : public Error{
public:
	UserInterfaceError(const std::string mes = "uncnown error",const std::string logName = DEFAULT_LOG_FILE);
};
class InputFileNotOppend : public UserInterfaceError {
public:
	InputFileNotOppend(const std::string mes = "");
};
class OutputFileNotOppend : public UserInterfaceError {
public:
	OutputFileNotOppend(const std::string mes = "");
};
class IncorrectOperation : public UserInterfaceError {
public:
	IncorrectOperation(const std::string mes = "");
};
class EmptyConditionData : public UserInterfaceError {
public:
	EmptyConditionData(const std::string mes = "");
};
class EmptyResultData : public UserInterfaceError {
public:
	EmptyResultData(const std::string mes = "");
};