#pragma once
#include <fstream>
#include "Data.h"
#include <iostream>
#include <string>
#include "rationalNumber.h"
#include <vector>
#include "expression.h"
#include "log.h"
#include "error/err.h"
#define UNDEFINED_CASE -1
#define DEFAULT_INCORRECT_LOG "log/log.txt"
class UserInterface{
private:
	Log incorrectInputLog;
	void openInputFile();
	void getDataFromInputFile();
	void printResultInFile();
	void openOutputFile();
	// void calculate();	//1
	bool isLogs;
	int lastChoose;
	int choose;
	std::ifstream inFile;
	std::string inputFileName;
	std::ofstream outFile;
	std::string outputFileName;
	Data data;

public:
	~UserInterface();
	UserInterface(std::vector<Expression> &d,
		std::vector<Rational> &res, std::string l = DEFAULT_INCORRECT_LOG);
	int makeChoose();
	void printMenu();
	int doAction();
};