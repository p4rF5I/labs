#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "error/err.h"
int gcd(int a,int b);
class Rational{
	private:
		int numerator;
		int denominator;
		void reduce();
		const Rational reduce(int a, int b);
	public:
		const Rational operator+(const Rational&);
		const Rational operator*(const Rational&);
		const Rational operator-(const Rational&);
		const Rational operator/(const Rational&);
		const Rational operator=(const Rational&);
		friend std::ostream& operator<<(std::ostream& os, const Rational& r);
		Rational(const Rational&);
		Rational(const int a = 1, const int b = 1);
		// Rational(const std::string a, const std::string b = "1");
		Rational(const std::string);
		void print() const;
		void print(std::ofstream&);
		int getNumerator();
		int getDenominator();
};
