#pragma once 
#include <string>
#include <iostream>
#include "rationalNumber.h"
#include "error/err.h"
class Expression{
private:
	size_t findOperatin(const std::string);
	Rational a;
	Rational b;
	char operation;
public:
	Expression(const std::string);
	std::string expr_to_str();
	Expression(const Rational a_,const Rational b_ ,const char operation_);
	Expression(const Rational a_);
	Expression& operator=(Expression&);
	const Rational exec();
	void print() const;
	// friend std::ostream& operator<<(std::ostream& os, const Rational& r);
};