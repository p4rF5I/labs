#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#define UNCHOOSED_DIR "null"
class Log{
private:
	time_t lastChange = 0;
	std::string logDir;
	std::ofstream logFile;
public:
	// void clearLog();
	// void deleteLogFile();
	void changeLogFile(const std::string);
	// void printLog();
	std::string getLogDir() const;
	void backup();
	void note(const std::string);
	Log(std::string);
	Log();
	~Log();
	Log(const Log&);
};
#include "error/err_basic.h"