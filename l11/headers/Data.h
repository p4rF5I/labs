#pragma once
#include "log.h"
#include "expression.h"
#include "rationalNumber.h"
#include "error/err.h"
#include <string>
#include <vector>
class Data{
private:
	Log incorrectInputLog;
	std::vector<Expression> &initialData;
	std::vector<Rational> &result;
public:
	void calculate();
	Data(std::vector<Expression> &i, std::vector<Rational> &r, Log &l);
	void add(const Expression &);
	void print(std::ofstream&);
	unsigned int getResultSize();

};