#include <iostream>
#include "headers/UserInterface.h"
#include "headers/expression.h"
#include "headers/rationalNumber.h"
#include <vector>
using namespace std;
int main(int argc, char *argv[]){
	vector<Expression> data;
	std::vector<Rational> ans;
	UserInterface ui(data, ans);
	system("clear || cls");
	bool isExit = false;
	while(!isExit){
		ui.printMenu();
		ui.makeChoose();
		system("clear || cls");
		isExit = ui.doAction();
/*		catch(Error a){
			cout << "catched: " << a.getErrorMessage() << '\n';
		}*/
	} 

	return 0;
} 