#include "headers/log.h"
using namespace std;
Log::Log(std::string file){
	changeLogFile(file);
}
Log::Log(){
	changeLogFile(UNCHOOSED_DIR);
}
Log::~Log(){
	// cout << "indestr\n";
	if(logFile.is_open()){
		logFile.close();
	}
}
void Log::note(const string message){
	if(!logFile.is_open()){
		throw(LogFileNotOppend());
	}
	lastChange = time(NULL);
	char buff[80];
	strftime(buff, 80, "%d/%m/%Y|%T", localtime(&lastChange));
	logFile << buff + (string)" | " + message + '\n';
	logFile.close();
	changeLogFile(logDir);
}
std::string Log::getLogDir() const{
	return logDir;
}
void Log::changeLogFile(const std::string file){
	if(logFile.is_open()){
		logFile.close();
	}
	logDir = file;
	if(logDir != UNCHOOSED_DIR){
		string temp = ("mkdir -p " + file.substr(0,file.find_last_of('/')));
		system(temp.c_str());
		logFile.open(file, fstream::out | fstream::app);
		if(!logFile.is_open()){
			throw(CantOpenLog());
		}
	}
}
void Log::backup(){
	if(!logFile.is_open()){
		throw(LogFileNotOppend());
	}
	logFile.close();
	string temp = "zip -q backup.zip " + logDir;
	system(temp.c_str());
	logFile.open(logDir, fstream::out | fstream::app);
}
Log::Log(const Log& a){
	changeLogFile(a.getLogDir());
}