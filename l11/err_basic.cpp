#include "headers/error/err.h"
Error::Error(const std::string mes, const std::string logName):message(mes){
	errorsLogFile.changeLogFile(logName);
}
void Error::addNote(const std::string prefix){
	try{
		errorsLogFile.note(prefix + (std::string)" " + message);
	}
	catch(...){
		errorsLogFile.changeLogFile(DEFAULT_LOG_FILE);
		addNote(prefix);
	}
}
std::string Error::getErrorMessage(){
	return message;
}
LogError::LogError(const std::string mes,const std::string logName) : Error(mes,logName){
	addNote("[LogError]");
}
CantOpenLog::CantOpenLog(const std::string mes) : LogError("cant open file. " + mes){}
LogFileNotOppend::LogFileNotOppend(const std::string mes) : LogError("log file isn't oppend. | " + mes){}
// ********************************RationalError***********************************
RationalError::RationalError(const std::string mes,const std::string logName) : Error(mes,logName){
	addNote("[RationalNumberError]");
}
DivByZero::DivByZero(const std::string mes):RationalError( "Dividing by 0. | "+ mes){}
BadMinus::BadMinus(const std::string mes):RationalError("bad minus. | " + mes){}
ExcessSymbols::ExcessSymbols(const std::string mes):RationalError("excess symbols. | " + mes){}
TooMoreDividingOperations::TooMoreDividingOperations(const std::string mes):RationalError("too more dividing operations! | " + mes){}
// *****************************ExpressionError*****************************
ExpressionError::ExpressionError(const std::string mes,const std::string logName) : Error(mes,logName){
	addNote("[ExpressionError]");
}
UnexpectedOperatin::UnexpectedOperatin(const std::string mes):ExpressionError("unexpected operation. | " + mes){}
NullStr::NullStr(const std::string mes):ExpressionError("null str. | " + mes){}
// **************************UserInterfaceError***********************
UserInterfaceError::UserInterfaceError(const std::string mes,const std::string logName) : Error(mes,logName){
	addNote("[UserInterfaceError]");
}
InputFileNotOppend::InputFileNotOppend(const std::string mes):UserInterfaceError("input file doesn't oppend. | " + mes){}
OutputFileNotOppend::OutputFileNotOppend(const std::string mes):UserInterfaceError("output file doesn't oppend. | " + mes){}
IncorrectOperation::IncorrectOperation(const std::string mes):UserInterfaceError("incorrect operation choosed. | " + mes){}
EmptyConditionData::EmptyConditionData(const std::string mes):UserInterfaceError("Cant Find any condition data. | " + mes){}
EmptyResultData::EmptyResultData(const std::string mes):UserInterfaceError("Cant Find any result data. | " + mes){}