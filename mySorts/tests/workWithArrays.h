#ifndef MAX_RAND
	#define MAX_RAND 10000
#endif
#ifndef ERROR_CODE
	#define ERROR_CODE  1
#endif
#ifndef SUCCESS_CODE
	#define SUCCESS_CODE  0
#endif
#include <iostream>
template <class T> T *getPointerOfMax(T *start, T *end);
template <class T> T *getPointerOfMin(T *start, T *end);
template <class T> void printArray(T *start, T *end);
template <class T> bool inputArray(T *start,T *end);
template <class T> void fillWithZero(T *start, T *end);
// ******************************************************
template <class T> T *getPointerOfMax(T *start, T *end){
	if(start > end) return 0; 	
	T *max=start;
	while (start < end){
		start ++;
		if(*(start) > *max) {max=start;}
	}
	return max; 
}
template <class T> void swap(T &a, T &b){
	T temp = a;
	a=b;
	b=temp;
}
template <class T> void swap(T *a, T *b){
	T temp = *a;
	*a = *b;
	*b = temp;
}
template <class T> T *getPointerOfMin(T *start, T *end){
	if(start > end) return 0; 	
	T *min = start;
	while (start < end){
		start ++;
		if(*(start) < *min) {min = start;}
	}
	return min; 
}
template <class T> void printArray(T *start, T *end){
	if(start > end) return;
	while (start <= end){
		std::cout << *start << ", ";
		++ start;
	}
	std::cout << "\b\b.\n";
}
template <class T> bool inputArray(T *start,T *end){
	int t=100;
	srand(time(NULL));
	std::cout << "Wish tandom nambers(use: 0 -NO || t to x<=t): ";
	if (!(std::cin >> t) || !t){
		std::cout << "Enter elements: ";
		while(start <= end){
			if (!(std::cin >> *start)) {
				return true;
			}
			++start;
		}
		return false;
	} else {
		while(start <= end){
			*start = rand()%t;
			++start;
		}
	}
	return false;
}
template <class T> void fillWithZero(T *start, T *end){
	while(start <= end){
		*start = 0;
		++start;
	}
}
template <class T> bool fillRand(T *start, T *end){
	if (start > end) return ERROR_CODE;
	++ end;
	// srand(time(NULL));
	while(start != end){
		*start = rand()%MAX_RAND;
		++start;
	} 
}