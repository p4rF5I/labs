#define ERROR_CODE 1 
#define SUCCESS_CODE  0
#define TEST
#define MAX 10000
#include <iostream>
#include "sorts.h"
#include "workWithArrays.h"
#include "getCPUTime.c"
long long int comparisons = 0;
double startTime,endTime;
extern long long int sw;
using namespace std;
int array[5][MAX];
template <class T> bool higher (T a, T b);
template <class T> bool lower (T a, T b);
template <class T> void prepareArray (T *start, int length, int amount);
template <class T> void testSort (bool (*testedSort)(T* , T*, bool (*)(T, T)),T *start,T *end);
template <class T> void test (char const *sort ,bool (*testedSort)(T* , T*, bool (*)(T, T)), T start[5][MAX],int length, int amount);
template <class T> void reset (T *start, int length, int amount);
int main(){
	srand(time(NULL));
	
	prepareArray(array,MAX,5);
	for(int i =0; i<5; ++i){
		printArray(array[i],array[i]+MAX-1);
	}
	int testArray[5][MAX];
	reset(testArray,MAX,5);
	test("insertion",sorts::insertion,testArray,MAX,5);	
	// for(int i =0; i<5; ++i){
	// 	printArray(array[i],array[i]+MAX-1);
	// }
	reset(testArray,MAX,5);
	test("selection",sorts::selection,testArray,MAX,5);
	// for(int i =0; i<5; ++i){
	// 	printArray(array[i],array[i]+MAX-1);
	// }
	reset(testArray,MAX,5);
	test("bubble",sorts::bubble,testArray,MAX,5);
	// for(int i =0; i<5; ++i){
	// 	printArray(array[i],array[i]+MAX-1);
	// }
	reset(testArray,MAX,5);
	test("shaker",sorts::shaker,testArray,MAX,5);
	// for(int i =0; i<5; ++i){
		// printArray(testArray[i],testArray[i]+MAX-1);
	// }
	reset(testArray,MAX,5);
	test("oddEven",sorts::oddEven,testArray,MAX,5);
	system("pause");
	return 0;
}
template <class T> bool higher (T a, T b){
	++ comparisons;
	return (a>b)?true:false;
}
template <class T> bool lower (T a, T b){
	++ comparisons;
	return (a<b)?true:false;
}
template <class T> void testSort (bool (*testedSort)(T* , T*, bool (*)(T, T)),T *start,T *end){
	comparisons = 0; sw = 0;
	volatile int *c=start;
	for (T f;c!=end+1;++c){ f=*start; *start=f;}
	startTime = getCPUTime( );
	testedSort(start, end,higher);
	endTime = getCPUTime( );
	cout << "cmp: " << comparisons << " sw: " << sw << '\t';
	cout << "CPU time: "<< endTime-startTime << '\n';
}
template <class T> void prepareArray (T *start, int length, int amount){
	fillRand(start[0],start[0]+length-1);
		for(int i=0 ;i < length; ++i){
		start[2][i] = start[1][i] = start[0][i];
	}
	sorts::insertion(start[0],start[0]+length-1,higher);//a>b>c>...
	sorts::insertion(start[1],start[1]+length-1,lower);//a<b<c<...
	fillRand(start[3],start[3]+length-1);
	fillRand(start[4],start[4]+length-1);
}
template <class T> void test (char const *sort, bool (*testedSort)(T* , T*, bool (*)(T, T)),T start[5][MAX],int length, int amount){
	cout << "Sort: " << sort << '\n';
	for (int i=0;i<amount;++i){
		testSort(testedSort,&start[i][0],&start[i][length-1]);
	}
}
template <class T> void reset (T *start, int length, int amount){
	for (int i=0,j=0;j < amount; ++i){
		if (i == length){ i=-1; ++j;continue;} 
		start[j][i]=array[j][i];
	}
}