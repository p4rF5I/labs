#ifndef ERROR_CODE
	#define ERROR_CODE  1
#endif
#ifndef SUCCESS_CODE
	#define SUCCESS_CODE  0
#endif
#ifdef TEST
	long long int sw=0;
#endif
#include <iostream>
namespace sorts
{
	template <class T> void printArray(T *start, T *end){
	if(start > end) return;
	while (start <= end){
		std::cout << *start << ", ";
		++ start;
	}
	std::cout << "\b\b.\n";
}
template <class T> void swapXor(T *a,T *b){
	#ifdef TEST
		++sw;
	#endif
	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}
template <class T> void swapXor(T& a,T& b){
	#ifdef TEST
		++sw;
	#endif
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
}
template <class T> void swap(T *a,T *b){
	#ifdef TEST
		++sw;
	#endif
	T c = *a;
	*a = *b;
	*b = c;
}
template <class T> void swap(T& a,T& b){
	#ifdef TEST
		++sw;
	#endif
	T c = a;
	a = b;
	b = c;
}
template <class T> bool selection (T *start, int length, bool (*cmp) (T, T)){
	if (selection(start,start+length-1,cmp)==ERROR_CODE) {return ERROR_CODE;}
	return SUCCESS_CODE;
}
template <class T> bool selection (T *start, T *end, bool (*cmp) (T, T)){
	if (start > end) {return ERROR_CODE;}
	T *pointer;	
	T *p; // pointer of max/min element
	++ end; // some optimization
	while (start != end){
		p = start;
		pointer = start+1;
		while (pointer != end){
			if (cmp(*pointer,*p)){
				p = pointer;
			}
			++pointer;
		}
		if (start != p){	// escape swaps like swap(a,a)
			swap(start,p);
		}
		++start;
	}
	return SUCCESS_CODE;
} 
template <class T> bool insertion (T *start, int length, bool (*cmp) (T, T)){
	if (insertion(start,start+length-1,cmp)==ERROR_CODE) {return ERROR_CODE;}
	return SUCCESS_CODE;
}
template <class T> bool insertion (T *start, T *end, bool (*cmp) (T, T)){
	if (start > end) {return ERROR_CODE;}
	T *p;	
	T *pCorrect = start+1; 
	++ end; //some optimizapion
	while (pCorrect != end){
		p = pCorrect;
		while (start != p){
			if (cmp(*p,*(p-1))){
				swap(p-1,p);
			} else {
				break;
			}
			--p;
		}
		++pCorrect;
	}
	return SUCCESS_CODE;
}
template <class T> bool bubble (T *start, int length, bool (*cmp) (T, T)){
	if (bubble(start,start+length-1,cmp)==ERROR_CODE) {return ERROR_CODE;}
	return SUCCESS_CODE;
}
template <class T> bool bubble (T *start, T *end, bool (*cmp) (T, T)){
	if (start > end) {return ERROR_CODE;}
	T *p;	
	++ end; //some optimizapion
	while (start != end){
		p = start+1;
		while (p != end){
			if (cmp(*p,*(p-1))){
				swap(p-1,p);
			}
			++p;
		}
		--end;
	}
	return SUCCESS_CODE;
}  
template <class T> bool shaker (T *start, int length, bool (*cmp) (T, T)){
	if (shaker(start,start+length-1,cmp)==ERROR_CODE) {return ERROR_CODE;}
	return SUCCESS_CODE;
}
template <class T> bool shaker (T *start, T *end, bool (*cmp) (T, T)){
	if (start > end) {return ERROR_CODE;} 
	T *p = start;
	T* a=start, *b = end;
	T *lastSwap = start;
	while (start!=end){
		while(p != end){
			if(cmp(*(p+1),*p)){
				swap(p+1,p);
				lastSwap = p;
			}
			++p;
		}
		end = lastSwap;
		// printArray(a,b);
		// std::cout << *start << '\t' << *end << '\t' << *lastSwap << '\n';
		p = end;
		if (start == end) {break;} // all done
		while (p != start){
			if(cmp(*(p),*(p-1))){
					swap(p,p-1);
					lastSwap = p;
			}
			--p;
		}
		start = lastSwap;
		p = start;
		// printArray(a,b);
		// std::cout << *start << '\t' << *end << '\t' << *lastSwap << '\n';
	}  
	// std::cout << "**********\n";
	return SUCCESS_CODE;
}
template <class T> bool oddEven (T *start, int length, bool (*cmp) (T, T)){
	if (length <= 0) {return ERROR_CODE;} 
	bool isSwaped = true; // was any swaps?
	--length;
	while(isSwaped){
		isSwaped = false;
		for(int i=0;i<length; i+=2){
			if (cmp(*(start+i+1), *(start+i))){
				swap (start+i+1,start+i);
				isSwaped = true;
			}
		}
		for(int i=1;i<length; i+=2){
			if (cmp(*(start+i+1),*(start+i))){
				swap (start+i+1,start+i);
				isSwaped = true;
			}
		}
	}
	return SUCCESS_CODE;
}
template <class T> bool oddEven (T *start, T *end, bool (*cmp) (T, T)){
	if (oddEven(start,end-start+1,cmp)==ERROR_CODE) {return ERROR_CODE;}
	return SUCCESS_CODE;
}
}