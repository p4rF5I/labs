#include <iostream>
template <class T> bool inputPol(T *start, T *end);
template <class T> T *getPointerOfMax(T *start, T *end);
template <class T> T *getPointerOfMin(T *start, T *end);
template <class T> void swap(T &a, T *b);
template <class T> void swap(T *a, T *b);
template <class T> void shakerSort(T *start, T *end, bool (*cmp)(T *a, T *b));
template <class T> void printArray(T *start, T *end);
template <class T> bool inputArray(T *start,T *end);
int gcd(int a,int b);
// ******************************************
template <class T> T *getPointerOfMax(T *start, T *end){
	if(start > end) return 0; 	
	T *max=start;
	while (start < end){
		start ++;
		if(*(start) > *max) {max=start;}
	}
	return max; 
}
template <class T> void swap(T &a, T &b){
	T temp = a;
	a=b;
	b=temp;
}
template <class T> void swap(T *a, T *b){
	T temp = *a;
	*a = *b;
	*b = temp;
}
template <class T> T *getPointerOfMin(T *start, T *end){
	if(start > end) return 0; 	
	T *min = start;
	while (start < end){
		start ++;
		if(*(start) < *min) {min = start;}
	}
	return min; 
}
template <class T> void shakerSort(T *start, T *end, bool (*cmp)(T *a, T *b)){
	if(start > end) return; 
	T *pointer = start;
	bool controller=1;
	while (start <= end){
		if (controller){
			while (pointer < end){
			if (cmp(pointer,pointer+1)) {swap(pointer,pointer+1);}
			++pointer;
			}
			controller= !controller;
			--end;
		} else {
			while (pointer > start){
			if (!cmp(pointer,pointer-1)) {swap(pointer,pointer-1);}
			--pointer;
			}
			controller= !controller;
			++start;
		}
		// printArray(start,end);
	} 
}
template <class T> void printArray(T *start, T *end){
	if(start > end) return;
	while (start <= end){
		std::cout << *start << ", ";
		++ start;
	}
	std::cout << "\b\b.\n";
}
template <class T> bool inputPol(T *start,T *end){
	int t=100;
	srand(time(NULL));
	std::cout << "Wish tandom nambers(use: 0 -NO || t to |x|<=t): ";
	if (!(std::cin >> t) || !t){
		std::cout << "Enter polynomial coefficients(from 0 to degree): ";
		while(start <= end){
			if (!(std::cin >> *start)) {
				return true;
			}
			++start;
		}
		return false;
	} else {
		while(start <= end){
			*start = rand()%t - rand()%t;
			++start;
		}
	}
	return false;
}
int gcd(int a, int b) {
return b ? gcd(b, a%b) : a;
}
template <class T> bool inputArray(T *start,T *end){
	int t=100;
	srand(time(NULL));
	std::cout << "Wish tandom nambers(use: 0 -NO || t to x<=t): ";
	if (!(std::cin >> t) || !t){
		std::cout << "Enter elements: ";
		while(start <= end){
			if (!(std::cin >> *start)) {
				return true;
			}
			++start;
		}
		return false;
	} else {
		while(start <= end){
			*start = rand()%t;
			++start;
		}
	}
	return false;
}