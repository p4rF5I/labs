/*
author:soroka
task: max composition of 3 array elements
testd:	1,2,-1,a-size;
		1,2,3,4;  1 2 3 4 5 6 7 8 9 10 11 12;
		1 1 1 1 1 1 1;
		-1 -1 -1 -1;
		-1 -1 -1 1;
		-1 -6 2 4;
		-1 -2 -3 -4;
		-1 -2 -3 4;
		2,15,22,444,7223,1,233;
		2,15,22,444,-7223,1,233;
		2,15,22,-444,-7223,1,233;
*/
#include <iostream>
#include "lab4StarterPack.h"
using namespace std;
int main(){
	int size;
	cout << "Enter size of array: ";
	if (!(cin >> size) || size < 3){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	int *array = new int [size];
	cout << "Enter array elements: ";
	for (int i = 0;i < size; i++){
		if (!(cin >> array[i])) {
			cout << "[ERROR] array initialization error!\n";
			delete [] array;
			system("pause");
			return 1;
		}
	}
	//move 3 max elements in end
	swap(array+size-1, getPointerOfMax(array,array+size-1));
	swap(array+size-2, getPointerOfMax(array,array+size-2));
	swap(array+size-3, getPointerOfMax(array,array+size-3));
	//move 2 min elements in begin
	swap(array, getPointerOfMin(array,array+size-1));
	swap(array+1, getPointerOfMin(array+1,array+size-1));
	// we have to compare 2 copmositions: MAX*min1*min2 & MAX*max1*max2
	int composition1 = array[size-1]*array[0]*array[1];
	int composition2 = array[size-1]*array[size-2]*array[size-3];
	if (composition2 > composition1) {
		cout << composition2 << '\n';
	} else {
		cout << composition1 << '\n';
	}
	system("pause");
	delete [] array;
	return 0;
}