/*
author:soroka
task: sum and comosition of polynoms 
testd:	random tests;
		0 0 0, 0 0 0;
*/
#include <iostream>
#include <time.h>
#include "lab4StarterPack.h"
template <class T> void printSum(T *start1, T *end1, T *start2, T *end2);
template <class T,class Y> void printSum(T *start1, Y degree1, T *start2, Y degree2);
template <class T> void printComposition(T *start1, T *end1, T *start2, T *end2);
template <class T> void fillWithZero(T *start, T *end);
template <class T,class Y> void printComposition(T *start1, Y degree1, T *start2, Y degree2);
using namespace std;
int main(){
	int degree1, degree2;
	cout << "Enter degree of first polynomial: ";
	if (!(cin >> degree1)){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	system("clear || cls");
	cout << "Enter degree of second polynomial: ";
	if (!(cin >> degree2)){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	int degree = (degree1 > degree2)?degree1:degree2;
	double *pol1= new double [degree+1];
	double *pol2= new double [degree+1];
	if(inputPol(pol1,pol1+degree1) || inputPol(pol2,pol2+degree2)){
		delete[] pol1;
		delete[] pol2;
		system("pause");
		return 1;
	}
	fillWithZero(pol1+degree1+1,pol1+degree);
	fillWithZero(pol2+degree2+1,pol2+degree);
	cout << "from zero degree\n";
	cout << "pol1: ";
	printArray(pol1,pol1+degree1);
	cout << "pol2: ";
	printArray(pol2,pol2+degree2);
	cout << "sum:  ";
	printSum(pol1,degree1,pol2,degree2);
	cout << "comp: ";
	printComposition(pol1,degree1,pol2,degree2);
	delete[] pol1;
	delete[] pol2;
	system("pause");
	return 0;
}
template <class T> void printSum(T *start1, T *end1, T *start2, T *end2){
	int degree = ((end1-start1) > (end2-start2))?(end1-start1):(end2-start2);
	T *polSum= new T [degree+1];
	for (int i=0;i<=degree;++i){
		polSum[i]=start1[i]+start2[i];
	}
	printArray(polSum,polSum+degree);
	delete[] polSum;
}
template <class T,class Y> void printSum(T *start1, Y degree1, T *start2, Y degree2){
	printSum(start1,start1+degree1,start2,start2+degree2);
}
template <class T,class Y> void printComposition(T *start1, Y degree1, T *start2, Y degree2){
	printComposition(start1,start1+degree1,start2,start2+degree2);
}
template <class T> void printComposition(T *start1, T *end1, T *start2, T *end2){
	int degree1 = end1-start1;
	int degree2 = end2-start2;
	int degree = degree1 + degree2;
	T *polComp= new T [degree+1];
	fillWithZero(polComp, polComp+degree);
	for (int p1=0; p1 <= degree1; ++p1){
		for(int p2=0; p2<= degree2; ++p2){
			polComp[p1+p2] += start1[p1]*start2[p2];
		}
	}
	printArray(polComp,polComp + degree);
	delete[] polComp;
}
template <class T> void fillWithZero(T *start, T *end){
	while(start <= end){
		*start = 0;
		++start;
	}
}