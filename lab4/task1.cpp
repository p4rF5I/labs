#include <iostream>
using namespace std;
int main(){
	int size;
	cout << "Enter size of array: ";
	if (!(cin >> size)){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	int *array = new int [size];
	for (int i = 0;i < size; i++){
		if (!(cin >> array[i])) {
			cout << "[ERROR] array initialization error!\n";
			delete [] array;
			system("pause");
			return 1;
		}
	}
	int temp=array[0];
	for (int i=1; i < size; ++i ){
		temp = temp ^ array[i];
	}
	cout << temp << '\n';
	system("pause");
	delete [] array;
	return 0;
}