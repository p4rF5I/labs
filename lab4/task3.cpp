/*
author:soroka
task: find min and do specialSort 
testd:	random tests;
		41 2 3 -4 12 41 22
		-1 2 5 99 11
		1233 41 12 -3
		-3 1 -4 -5 12 -3
*/
#include <iostream>
#include <time.h>
#include "lab4StarterPack.h"
template <class T> bool higher(T *a, T *b);
template <class T> bool lower(T *a, T *b);
template <class T> int specialSort (T *start, T *end);
using namespace std;
int main(){
	int size;
	cout << "Enter size of array: ";
	if (!(cin >> size)){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	int *array = new int [size];
	int t;
	cout << "Wish tandom nambers(use: 0 -NO || t to |x|<=t): ";
	if (!(cin >> t) || !t){
		cout << "Enter array elements: ";
		for (int i = 0;i < size; i++){
			if (!(cin >> array[i])) {
				cout << "[ERROR] array initialization error!\n";
				delete [] array;
				system("pause");
				return 1;
			}
		}
	} else {
		srand(time(NULL));
		for (int i=0; i<size; ++i){
			array[i]= rand()%t - rand()%t;
		}
	}
	printArray(array,array+size-1);
	cout << specialSort(array,array+size-1) << '\n';
	printArray(array,array+size-1);
	system("pause");
	delete [] array;
	return 0;
}
template <class T> int specialSort (T *start, T *end){
	T *min=getPointerOfMin(start,end);
	if (min != start) shakerSort(start,min-1,lower);
	if (min != end) shakerSort(min+1,end,higher);
	return (min-start);
}
template <class T> bool lower(T *a, T *b){
	if (*a < *b){
		return true;
	} else {
		return false;
	}
}
template <class T> bool higher(T *a, T *b){
	if (*a > *b){
		return true;
	} else {
		return false;
	}
}