/*
author:soroka
task: find arithmetic progression with the least number of elements
testd:	random tests;
		22 1 36
		1 1 1
		1 2 1
		2 6 10
		2 10
		1 5 13
		0 0 0
*/
#include <iostream>
#include <time.h>
#include "lab4StarterPack.h"
template <class T> bool cmp(T *a, T *b);
using namespace std;
int main(){
	int amountOfElements;
	cout << "Enter amount of elements: ";
	if (!(cin >> amountOfElements)){
		cout << "[ERROR]Input error!\n";
		system("pause");
		return 1;
	}
	int *el = new int [amountOfElements];
	--amountOfElements; //some optimization,
	if(inputArray(el,el+amountOfElements)){
		delete [] el;
		cout << "[ERROR]Element input error!\n";
		system("pause");
		return 0;
	}
	shakerSort(el,el+amountOfElements,cmp);
	printArray(el,el+amountOfElements);
	if (el[amountOfElements] == el[0]){
		cout << "d: 0; must add: 0 elements\n";
		system("pause");
		return 0;
	} else {
		for (int i=0;i<amountOfElements;++i){
			if (el[i]==el[i+1]) {
				cout << "Very bad case: we cant do anything!\n";
				delete[] el;
				system("pause");
				return 1;
			}
		}
	}
	int d=el[1]-el[0];
	// find correct d
	for (int i=2; i<=amountOfElements; ++i){
		d=gcd(d,el[i]-el[i-1]);
	}
	int index=1;
	int correct=el[0];
	int amount=0; 	// amount of nubers, that we must add
	cout << el[0] << ", ";
	int minElMustBe = (el[amountOfElements]-el[0])/d;
	for(int i=0; i<minElMustBe; ++i){
		correct+=d;
		if(correct==el[index]){
			cout << el[index] << ", ";
			++index;
		} else{
			cout << correct << ", ";
			++amount;
		}
	}
	cout << "\b\b.\n d = " << d << "; must add " << amount << " elements\n";
	delete [] el;
	system("pause");
	return 0;
}
template <class T> bool cmp(T *a, T *b){
	if (*a > *b){
		return true;
	} else {
		return false;
	}
}