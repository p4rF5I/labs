#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;
template <typename T>
class Poly{
private:
	void check_nullptr() const{
		if(coeff == nullptr){
			throw("undef poly");
		}
	}
	int deg;
	T *coeff;
	void reduce(){
		int newDeg = deg;
		if(deg == 0 && !coeff[0]){
			return;
		}
		while(coeff[newDeg] == 0){
			--newDeg;
		}
		if(newDeg != deg){
			deg = newDeg;
			T temp[deg + 1];
			for(int i = 0; i <= deg; ++i){
				temp[i] = coeff[i];
			}
			delete[] coeff;
			coeff = new T[deg + 1];
			for(int i = 0; i <= deg; ++i){
				coeff[i] = temp[i];
			}
		}
		// cout << "hereReduce\n";
	}
public:
	T operator[](int i){
		check_nullptr();
		if(i > deg){
			throw("aout_of_arr");
		} else {
			return coeff[i];
		}
	}
	const T getVal(const T &a) const{
		check_nullptr();
		T temp = 0; 
		for (int i = 0; i <= deg; ++i){
			temp = temp + a * coeff[i];
		}
		return temp;
	}
	const Poly& operator=(const Poly& p){
		p.check_nullptr();
		deg = p.deg;
		coeff = new T[deg + 1];
		for(int i = 0; i <= deg; ++i){
			coeff[i] = p.coeff[i];
		}
		return (*this);
	}
	Poly operator+(const Poly& p){
		check_nullptr();
		p.check_nullptr();
		// cout << "hera\n";
		int maxDeg = (deg > p.deg)?deg : p.deg;
		int minDeg = (deg < p.deg)?deg : p.deg;
		// cout << minDeg << '\t' << maxDeg << '\n';
		T temp[maxDeg + 1];
		for(int i = 0; i <= minDeg; ++i){
			temp[i] = p.coeff[i] + coeff[i];
		}
		if(deg == maxDeg){
			for(int i = minDeg + 1; i <= maxDeg; ++i){
				temp[i] = coeff[i];
			}	
		} else {
			for(int i = minDeg + 1; i <= maxDeg; ++i){
				temp[i] = p.coeff[i];
			}
		}
/*		print();
		cout << "\n+\n";
		cout << p << '\n';
		cout << "result: ";
		cout << Poly(temp,maxDeg); */
		return Poly(temp,maxDeg);
	}
	Poly operator-(const Poly& p){
		check_nullptr();
		p.check_nullptr();
		int maxDeg = (deg > p.deg)?deg : p.deg;
		int minDeg = (deg < p.deg)?deg : p.deg;
		T temp[maxDeg + 1];
		for(int i = 0; i <= minDeg; ++i){
			temp[i] = coeff[i] - p.coeff[i];
		}
		if(deg == maxDeg){
			for(int i = minDeg + 1; i <= maxDeg; ++i){
				temp[i] = coeff[i];
			}	
		} else {
			for(int i = minDeg + 1; i <= maxDeg; ++i){
				temp[i] = p.coeff[i];
			}	
		}
/*		print();
		cout << "\n-\n";
		cout << p << '\n';
		cout << "result: ";
		cout << Poly(temp,maxDeg); */
		return Poly(temp,maxDeg);
	} 
	Poly(){
		deg = -1;
		coeff = nullptr;
	}
	Poly(const string inStr,T (*convert)(string)){
		stringstream ss = stringstream(inStr);
		string temp;
		deg = -1;
		while((ss >> temp)){

			++deg;
		}
		// cout << "[DEBUG] deg: " << deg << '\n';
		if(deg == -1){
			throw("bad string.");
		} else {
			ss = stringstream(inStr);
			coeff = new T[deg + 1];
			for (int i = 0; i <= deg; ++i){
				ss >> temp;
				coeff[i] = convert(temp);
				// cout << "coeff[" << i << "]: " << coeff[i];
			}
		}
		// cout << '\n';
	}
	Poly(const Poly &p){
		deg = p.deg;
		coeff = new T[deg + 1];
		for(int i = 0; i <= deg; ++i){
			coeff[i] = p.coeff[i];
		}
		// cout << "herePolyCopy\n";
		reduce();
	}
	Poly(T* c, int s){
		// cout << "here\n" << s << sizeof(c);
		deg = s;
		coeff = new T[deg + 1];
		for(int i = 0; i <= deg; ++i){
			coeff[i] = c[i];
		}
		reduce();
	}
	~Poly(){
		delete[] coeff;
	}
	void print(std::ostream& o = std::cout) const{
		check_nullptr();
		// cout << "inprint\n";
		// cout << "Deg: " << deg;
		if(deg != 0){
			o << coeff[deg] << "x^" << deg;
		}
		for(int i = deg - 1; i > 0; --i){
			if(coeff[i] != 0){
				if(coeff[i] > 0){
					o << " + " << coeff[i];
				} else {
					o << " - " << -1 * coeff[i];
				}
				o << "x^" << i;
			}
		}
		if(deg != 0 && coeff[0] != 0){
			if(coeff[0] > 0){
				o << " + " << coeff[0];
			} else {
				o << " - " << -1 * coeff[0];
			}
		}
		if(deg == 0){
			o << coeff[deg];
		}
	}
	friend std::ostream& operator<<(std::ostream& o, const Poly &p){
		p.print(o);
		return o;
	}
	int getDeg() const{
		return deg;
	}
};