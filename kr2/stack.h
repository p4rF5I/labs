#ifndef my_stack
#define my_stack
#include <iostream>
template <class T>class Node{
	private:
		T key;
		Node *next = nullptr;
	public:
		Node(T var = 0, Node* ptr = nullptr):next(ptr),key(var){}
		Node<T> *getNext(){return next;}
		void setNext(Node *ptr){
			next = ptr;
		}
		void setKey(T var){
			key = var;
		}
		T getKey(){
			return key;
		}
};
template <class T> class Stack{
	private:
		Node<T> *top = nullptr;
	public:
	~Stack(){
		Node<T> *ptr;
		while(!isEmpty()){
			ptr = top;
			top = top -> getNext();
			delete ptr;
		}
	}
	bool push(T var){
			if(!isEmpty()){
				top = new Node<T>(var, top);
			} else {
				top = new Node<T>(var);
			}
		}		
	T pop(){
			if(!isEmpty()){
				Node<T> *temp = top -> getNext(); 
				T var = top -> getKey();
				delete top;
				top = temp;
				return var;
			} else {
				throw("stack free.");
			}
		}
	bool isEmpty(){
		if(top == nullptr){
			return true;
		} else {
			return false;
		}
	}
};
#endif