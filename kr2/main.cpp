#include <iostream>
#include <fstream>
#include <string>
#include "stack.h"
#include <sstream>
#include "poly.h"
using namespace std;
template<class T> Poly<T> calcOddSum(Stack<Poly<T>> &s, T (*convert)(string));
int convert(string a);
int main(int argc, char *argv[]){
	ifstream inFile;
	if(argc == 2){
		inFile.open(argv[1], fstream::in);
	}
	if(!inFile.is_open()){
		string filename;
		cout << "Enter input file name: ";
		cin >> filename;
		inFile.open(filename, fstream::in);
		if(!inFile.is_open()){
			cout << "cant open input file.\n";
			system("pause");
			return 1;
		}
	}
	string fileLine;
	Stack<Poly<int>> allPol;
	while(!inFile.eof()){
		getline(inFile,fileLine);
		try{
			allPol.push(Poly<int>(fileLine, convert));
		}
		catch(...){
			cout << "[ERROR] err detected.\n";
		}
	}
	cout << calcOddSum<int>(allPol, convert) << '\n';
	system("pause");
	return 0;
}
int convert(string a){
	return stoi(a);
}
template<class T> Poly<T> calcOddSum(Stack<Poly<T>> &s, T (*convert)(string)){
	Poly<T> temp;
	Poly<T> sum((string)"0", convert);
	while(!s.isEmpty()){
		temp = s.pop();
		cout << "temp(" << !(temp.getDeg()%2) << "): " << temp << '\n';
		if(temp.getDeg() != -1 && !(temp.getDeg()%2)){
			sum = sum + temp;
		}
	}
	return sum;
}