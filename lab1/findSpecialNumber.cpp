/*
task: find absolutlu highest a |  num%dev==rem
tested:
		4(1 2 4 -8) 2 0
		4(1 2 4 -8) 3 0
		4(1 2 8 -8) 2 1		
p.s. a'm working on more beutiful wtc version;
return version looks more attractive;
*/
#include <conio.h>
#include <iostream>
using namespace std;
const int MAXTRIES = 4;
long int getIndexOfAbsSpecialMax(long int *number,int n, int devisior = 2, int remainder = 0);
void printArray(long int *number, int n);
bool wishToContinue();
int main() {
	int n=-1;
	short int tries=0;
	bool wtc = 1;		//wish to continue
	while (wtc && (n <= 0)) {
		//**************START WISH TO CONTINUTE MODULE************
		if (tries >= MAXTRIES) {
			tries = 0;
			if (!wishToContinue()) {
				wtc = false; continue;
			}
			else {
				wtc = true;
			}
		}
		++tries;
		//**************END WISH TO CONTINUTE MODULE***************
		cout << "Enter amount of numbers(n>0): ";
		cin >> n;
		system("cls");
	}
	if (wtc) {
		long int *number = new long int[n];
		system("cls");
		for (int i = 0; i < n; i++) {
			cout << "Be careful (all numbers are converted to integers)!\n";
			cout << "Enter " << i + 1 << " number: ";
			cin >> number[i];
			system("cls");
		}
		int devisior = -1, remainder = -1;
		tries = 0;						//Enter devisior
		while (wtc && devisior <= 0) {
			//**************START WISH TO CONTINUTE MODULE************
			if (tries >= MAXTRIES) {
				tries = 0;
				if (!wishToContinue()) {
					wtc = false; continue;
				}
				else {
					wtc = true;
				}
			}
			++tries;
			//**************END WISH TO CONTINUTE MODULE***************
			cout << "Enter correct devisior(dev>0): ";
			cin >> devisior;
			system("cls");
		}
		tries = 0;
		while (wtc && (remainder >= devisior || remainder < 0)) {		//Enter remainder
			//**************START WISH TO CONTINUTE MODULE************
			if (tries >= MAXTRIES) {
				tries = 0;
				if (!wishToContinue()) {
					wtc = false; continue;
				}
				else {
					wtc = true;
				}
			}
			++tries;
			//**************END WISH TO CONTINUTE MODULE***************
			cout << "Enter correct remainder(r>=0 && r<" << devisior << "): ";
			cin >> remainder;
			system("cls");
		}
		if (wtc) {
			printArray(number, n);
			long int result = getIndexOfAbsSpecialMax(number, n, devisior, remainder);
			if (result == -1) {
				cout << "\"not found\" such number, that num%" << devisior << "==" << remainder << endl;
			}
			else {
				cout << "Highest absolute number such that num%" << devisior << "==" << remainder << " is " << number[result] << endl;
			}
		}
		delete[] number;
	}
	system("pause");
	return 0;

}
long int getIndexOfAbsSpecialMax(long int *number,int n, int devisior, int remainder) {
	long int iMax = 0;
	long int max = abs(number[iMax]);
	int i = 0;
	for (i; i < n; i++) {
		if ((abs(number[i]) % devisior == remainder) && (max < abs(number[i]))) {
			iMax = i;
			max = abs(number[iMax]);	//some optimization
		}
	}
	if (iMax == 0 && (abs(number[iMax]) % devisior != remainder)) return -1;  // if no such element in number[]
	return iMax;
}
void printArray(long int *number, int n) {
	for (int i = 0; i < n; i++) {
		cout << i << '\t';
	}
	cout << '\n';
	for (int i = 0; i < n; i++) {
		cout << number[i] << '\t';
	}
	cout << endl;
}
bool wishToContinue() {
	cout << "so many unsuccessful attempts... continue?(Y/y-Yes) ";
	wchar_t ans;
	while (true) {
		char ans=0;
		cin >> ans;
		system("cls");
		if (ans == 'Y' || ans == 'y') { 
			return true;
		}else {
			return false;
		}
	}
}