/*
author: sorokaEA
condition: is Possible Circle In Square and is Possible Square In Cirlce
test: 
    0	-1		9pi-1	37		2pi+1	4
	0	 0		9pi+1	36		2pi-1	4
	-1	-2
p.s. a'm working on more beutiful wtc version;
return version looks more attractive;
*/
#include<iostream>
using namespace std;
const int MAXTRIES = 4;
const double PI = 3.14159265359;
bool isPossibleCircleInSquare (double cmp);
bool isPossibleSquareInCirlce(double cmp);
bool wishToContinue();
int main() {
	bool wtc = 1;		//wish to continue
	short int tries = 0;
	//setlocale(0, "");
	double sqCircle = -1;
	double sqSquare = -1;
	while (wtc && (sqCircle < 0)) {
		//**************START WISH TO CONTINUTE MODULE************
		if (tries >= MAXTRIES) {
			tries = 0;
			if (!wishToContinue()) {
				wtc = false; continue;
			}
			else {
				wtc = true;
			}
		}
		++tries;
		//**************END WISH TO CONTINUTE MODULE***************
		cout << "Enter square of circle: ";
		cin >> sqCircle;
		system("cls");
	}
	tries = 0;
	while (wtc && (sqSquare <= 0)) {
		//**************START WISH TO CONTINUTE MODULE************
		if (tries >= MAXTRIES) {
			tries = 0;
			if (!wishToContinue()) {
				wtc = false; continue;
			}
			else {
				wtc = true;
			}
		}
		++tries;
		//**************END WISH TO CONTINUTE MODULE***************
		cout << "Enter square of square:) : ";
		cin >> sqSquare;
		system("cls");
	}
	//calc module
	if (wtc) {
		system("cls");
		cout.precision(9);
		cout << "Input: SqCircle = " << sqCircle << "\tSqSquare = " << sqSquare << endl;
		double cmp = sqCircle / sqSquare;
		if (isPossibleSquareInCirlce(cmp)) {
			cout << "Square in Circle - possible!\n";
		}
		else {
			cout << "Square in Circle - impossible!\n";
		}
		if (isPossibleCircleInSquare(cmp)) {
			cout << "Circle in Square  - possible!\n";
		}
		else {
			cout << "Circle in Square - impossible!\n";
		}
	}
	system("pause");
	return 0;
}
bool isPossibleCircleInSquare(double cmp) {
	if (cmp <= PI / 4) return true;				//easy-proved formula
	return false;
}
bool isPossibleSquareInCirlce(double cmp) {
	if (cmp >= PI / 2) return true;				//easy-proved formula
	return false;
}
bool wishToContinue() {
	cout << "so many unsuccessful attempts... continue?(Y/y-Yes) ";
	wchar_t ans;
	while (true) {
		char ans = 0;
		cin >> ans;
		system("cls");
		if (ans == 'Y' || ans == 'y') {
			return true;
		}
		else {
			return false;
		}
	}
}