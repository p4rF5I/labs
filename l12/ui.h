#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include "bin_tree.h"
#include "rationalNumber.h"
#define T_TYPE int 
using namespace std;
class UserInterface{
private:
	ifstream in;
	BinaryTree<T_TYPE> tree;
	void getDataFromFile();
public:
	void chooseFileMenu();
	void findDataMenu() const;
	void printData() const;
};