#include "ui.h"
void UserInterface::printData() const{
	tree.print();
}
void UserInterface::findDataMenu() const{
	T_TYPE temp;
	string wtc;
	while(true){
		cout << "find: ";
		cin >> temp;
		if(tree.find(temp) != nullptr){
			cout << *tree.find(temp) << '\n';
		} else {
			cout << "can't find it\n";
		}
		cout << "wish to conitue?(y to conitue)";
		cin >> wtc;
		if(wtc[0] != 'y'){
			break;
		}
	}
}
void UserInterface::chooseFileMenu(){
	string filename;
	do{
		cout << "Enter filename: ";
		cin >> filename;
		in = ifstream(filename);
		if(!in.is_open()){
			cout << "cant open input file, choose again.";		
		}
	} while(!in.is_open());	
	getDataFromFile();
}
void UserInterface::getDataFromFile(){
	T_TYPE temp;
	while(in >> temp){
		tree.add(temp);
	}
}