#include "rationalNumber.h"
using namespace std;
std::istream& operator>>(std::istream& i, Rational &r){
	double temp1, temp2;
	try{
		i >> temp1 >> temp2;
		// cout << temp1 << temp2 << '\n';
		r = Rational(temp1,temp2);
	}
	catch(...){
		cout << "[ERROR]cant read some data.\n";
	}
	return i;
}
bool Rational::operator>(const Rational& r) const{
	return numerator * r.denominator - denominator * r.numerator > 0;
}
bool Rational::operator==(const Rational& r) const{
	return numerator * r.denominator - denominator * r.numerator == 0;
}
const Rational Rational::reduce (int a, int b){
	int d = gcd(a,b); 
	if(d < 0){
		d*=-1;
	}
	if(d != 1 && d != 0){
		a /= d;
		b /= d;
	}
	return Rational(a,b);
}
void Rational::print() const{
	if(denominator == 1){
		cout << numerator;
	} else {
		cout << numerator << '/' << denominator;
	}
	// cout << '\n';
}
void Rational::print(ofstream& out){
	if(out.is_open()){
		if(denominator == 1){
			out << numerator;
		} else {
			out << numerator << '/' << denominator;
		}
		out << '\n';
	}
}
Rational::Rational(const string r){
	// cout 
	size_t rStart = r.find_first_not_of(" ");
	size_t length = r.find_first_of(" ",rStart) - rStart;
	string str = r.substr(rStart,length);
	if(r.find_first_not_of(" ",rStart + length) != string::npos){
		throw("errorInRationa");
	}
	if(str.find_first_not_of("-/1234567890") != string::npos){
		throw("errorInRationa");
	}
	size_t separator = str.find_first_of('/');
	if(str.find_first_of('/',separator + 1) != string::npos){
		throw("errorInRationa");
	}
	size_t secondMinusId = str.find_first_of('-',1);
	if(secondMinusId != string::npos && secondMinusId != separator + 1){
		throw("errorInRationa");
	}
	if(separator != string::npos){
		numerator = stoi(str.substr(0, separator));
		denominator = stoi(str.substr(separator + 1, str.size()));
		if(denominator == 0){
			throw("errorInRationa");
		}
		if(denominator < 0){
			denominator *= -1;
			numerator *= -1;
		}
		reduce();
	} else {
		numerator = stoi(str);
		denominator = 1;
	}
}
Rational::Rational(const Rational &a){
	numerator = a.numerator;
	denominator = a.denominator;
	reduce();
}
Rational::Rational(const int a,const int b): numerator(a),denominator(b){
	if(b == 0){
		throw("errorInRationa");
	}
	if(b < 0){
		numerator *= -1;
		denominator *= -1;
	}
	reduce();
}
const Rational Rational::operator+(const Rational &a){
/*	if(denominator == 0 || a.denominator == 0){
		throw("errorInRationa");
	}*/
	return reduce(numerator * a.denominator + denominator * a.numerator,denominator * a.denominator);
}
const Rational Rational::operator-(const Rational &a){
/*	if(denominator == 0 || a.denominator == 0){
		throw("errorInRationa");
	}*/
	return reduce(numerator * a.denominator - denominator * a.numerator,denominator * a.denominator);
}
const Rational Rational::operator*(const Rational &a){
/*	if(denominator == 0 || a.denominator == 0){
		throw("errorInRationa");
	}*/
	return reduce(numerator * a.numerator,denominator * a.denominator);
}
const Rational Rational::operator=(const Rational &a){
	numerator = a.numerator;
	denominator = a.denominator;
}
const Rational Rational::operator/(const Rational &a){
	if(denominator == 0 || a.numerator == 0){
		throw("errorInRationa");
	}
	return reduce(numerator * a.denominator, denominator * a.numerator);
}
int Rational::getNumerator(){
	return numerator;
}
int Rational::getDenominator(){
	return denominator;
}
void Rational::reduce(){
	int d = gcd(numerator,denominator); 
	if(d < 0){
		d*=-1;
	}
	if(d != 1){
		numerator /= d;
		denominator /= d;
	}
}
int gcd(int a,int b){
	return b?gcd(b,a%b):a;
}
ostream& operator<<(ostream& os, const Rational& r){
	if(r.denominator != 1){
		os << r.numerator << '/' << r.denominator;
	} else {
		os << r.numerator;
	}
	return os;
}