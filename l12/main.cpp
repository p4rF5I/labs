#include <iostream>
#include <string>
#include "ui.h"
using namespace std;
int main(int argc, char *argv[]){
	UserInterface ui;
	try{
		ui.chooseFileMenu();
	}
	catch(...){
		cout << "[ERROR] cant read data from file.\n";
		return 1;
	}
	ui.printData();
	ui.findDataMenu();
	return 0;
}