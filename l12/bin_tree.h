#pragma once
#include <iostream>
#include <queue>
#include "rationalNumber.h"
using namespace std;
template <typename T> class BinaryTree;
template<typename T> class Node{
	friend class BinaryTree<T>;
	T key;
	int amount;
	Node<T>* l = nullptr;
	Node<T>* r = nullptr;
public:
	friend ostream& operator<<(ostream &o, const Node<T> &b){
		o << b.key << ':' << b.amount;
		return o;
	}
	Node<T>(const T k): key(k),amount(1){
		// cout << "inconstr\n";
	}
	void inc(){
		++amount;
	}
	int getAmount() const{
		return amount;
	}
	T getKey() const{
		return key;
	}
	Node<T>* getl() const{
		return l;
	}
	Node<T>* getr() const{
		return r;
	}
};
template <typename T> class BinaryTree{
private:
	Node<T>* head;
public:
	void print() const{
		queue<Node<T>*> temp;
		temp.push(head);
		Node<T> *tempPtr;
		while(!temp.empty()){
			tempPtr = temp.front();
			// cout << tempPtr -> getKey() << '\n';
			temp.pop();
			if(tempPtr != nullptr){
				if(tempPtr -> l != nullptr){
					temp.push(tempPtr -> l);
				}
				if(tempPtr -> r != nullptr){
					temp.push(tempPtr -> r);
				}
				cout << *tempPtr << '\t';
			}
		}
		cout << '\n';
	}
	void add(const T a){
		// cout << "add:\n";
		if(head == nullptr){
			head = new Node<T>(a);
			return;
		}
		Node<T>* correct = head;
		while(true){
			if(a > correct -> key){
				if(correct -> r == nullptr){
					correct -> r = new Node<T>(a);
					break;
				} else {
					correct = correct -> r;
				}
			} else {
				if(a == correct -> key){
					correct -> inc();
					// cout << "already here;\n";
					break;
				}
				if(correct -> l == nullptr){
					correct -> l = new Node<T>(a);
					break;
				} else {
					correct = correct -> l;
				}
			}
		}
	}
	Node<T>* find(const T a) const{
		// cout << "find\n";
		if(head == nullptr){
			return nullptr;
		}
		Node<T>* correct = head;
		while(true){
			if(a == correct -> key){
				return correct;
			}
			if(a > correct -> key){
				if(correct -> r != nullptr){
					// cout << "R\n";
					correct = correct -> r;
				} else {
					break;
				}
			} else {
				if(correct -> l != nullptr){
					// cout << "L\n";
					correct = correct -> l;
				} else {
					break;
				}
			}
		}	
		return nullptr;
	} 
	BinaryTree<T>():head(nullptr){
	}
	~BinaryTree<T>(){
		queue<Node<T>*> temp;
		temp.push(head);
		Node<T> *tempPtr;
		while(!temp.empty()){
			tempPtr = temp.front();
			temp.pop();
			if(tempPtr != nullptr){
				// cout << tempPtr -> getKey() << '\n';
				if(tempPtr -> l != nullptr){
					temp.push(tempPtr -> l);
				}
				if(tempPtr -> r != nullptr){
					temp.push(tempPtr -> r);
				}
				delete tempPtr;
			}
		} 
	}
};