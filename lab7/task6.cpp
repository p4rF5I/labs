/*
author: soroka
task: (2 балла) В данной строке все вхождения подстроки str1 заменить подстрокой str2.
tested: 
asd qwert as d asd qwa zasdf qqwe asd | asd 123
asd qwert as d asd qwa zasdf qqwe asd | asd 123456
asd qwert as d asd qwa zasdf qqwe asd | asd 1
*/
#include <iostream>
#include <cstring>
using namespace std;
void replace(char *str, int strMaxSize, const char* str1, const char* str2);
int main(){
	char str1[100];
	char str2[100];
	char a[1024] = "22235678922 13";
	cout << "Enter your line: ";
	cin.getline(a,sizeof(a));
	cout << "Replace a to b (input:a b): ";
	cin >> str1 >> str2;
	// cout << a << '\n';
	replace (a, sizeof(a), str1, str2);
	cout << "answer: " << a << '\n';
	system("pause");
	return 0;
}
void replace(char *str, int strMaxSize, const char* str1, const char* str2){
	int lengthStr1 = strlen(str1);
	if(lengthStr1 == 0){
		cout << "[ERROR] str1 is empty\n";
		return;
	}
	int lengthStr2 = strlen(str2);
	char *p = strstr(str,str1);
	int count = 0;
	while (p != NULL){
		++count;
		p = strstr(p + lengthStr1,str1); 
	}
	if (strMaxSize < strlen(str) + count * (lengthStr2 - lengthStr1)){ //we use <= because \0
		cout << "[ERROR]Not enough free space in your string!\n";
		return;
	}
	char tempStr[strMaxSize];
	strcpy(tempStr,str);
	char *strP = str - lengthStr2;	//offset for simple looping
	char *tempStrP = tempStr - lengthStr1; //offset for simple looping
	p = strstr(tempStr, str1);	// set p as start-replace pointer
	while (p != NULL){
		strP += lengthStr2;	//skip replaced symbols in str
		tempStrP += lengthStr1;	//skip replaced symbols in tempStr
		while(tempStrP < p){	//just copy unchanged part of string
			*strP = *tempStrP;
			++strP;
			++tempStrP;  
		}
		for(int i = 0; i < lengthStr2; ++i){	// replacing
			strP[i] = str2[i];
		}
		p = strstr(tempStrP + lengthStr1,str1);	// find next str1 entry
	}
	strP += lengthStr2;
	tempStrP += lengthStr1;
	while(*tempStrP){	//copying end of string
		*strP = *tempStrP;	
		++strP;
		++tempStrP;  
	}
	*strP = '\0';	//set end of string
}