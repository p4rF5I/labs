/*
author: soroka
task:(2 балла) Дан код программы на С++.  Вывести построчно все операции и частоту их использования в программе.
tested: prev programs
*/
#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;
int amInStr(char* str,const char* substr);
void printAllOperations(ifstream& in);
int main(){
	char fileName[120];
	cout << "Enter file name: ";
	cin >> fileName;
	ifstream in(fileName);
	if(in.is_open()){
		printAllOperations(in);
	} else {
		cout << "cant open file\n";
	}
	return 0;
}
// 0 1 2 3 4 5 6  7
// + - * / % = ++ -- 
void printAllOperations(ifstream& in){
	int op[8];
	for(int i = 0; i < 8; ++i){
		op[i] = 0;
	}
	char buff[1024] = "\0";
	while((in.getline(buff, 1024))){
		// cout << buff << '\n';
		op[0] += amInStr(buff," + ");
		op[1] += amInStr(buff," - ");
		op[2] += amInStr(buff," * ");
		op[3] += amInStr(buff," / ");
		op[4] += amInStr(buff," % ");
		op[5] += amInStr(buff," = ");
		op[6] += amInStr(buff,"++");
		op[7] += amInStr(buff,"--");
	}
	cout << "+\t-\t*\t/\t%\t=\t++\t--\n";
	for(int i = 0; i < 8; ++i){
		cout << op[i] << '\t';
	}
	cout << '\n';

}
int amInStr(char* str,const char* substr){
	int am = -1;
	do{
		++am;
		str = strstr(str,substr);
		if(str != NULL){
			++str;
		}
	} while(str != NULL);
	return am;
}