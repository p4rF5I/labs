/*
author: soroka
task: delete word
tested: 
1 23445 1234 321 1 2323 1 1 1 1 1 1 . 1;
123 123 321 321 . 123;
1 1 1 1 1 1 123 11 . 1;
123 1                       1    23 . 1
*/
#include <iostream>
#include <cstring>
using namespace std;
void deleteWords(char *str, const char* word);
int main(){
	char str[1024]; // = "hello hello1 1hello1 1hello hello world 1 hello";
	// deleteWords(str,"hello");
	cout << "Enter string: ";
	cin.getline(str,1024);
	char word[1024];
	cout << "delete: ";
	cin >> word;
	deleteWords(str,word);
	cout << "Result: " << str << '\n';
	system("pause");
	return 0;
}
void deleteWords(char *str, const char* word){
	char tempStr[strlen(str)];
	strcpy(tempStr,str);
	char *p = str;
	str[0] = '\0';
	char *wStart = tempStr, *wEnd = wStart;
	while (*wStart){
		wEnd = wStart;
		while(*wEnd && *wEnd != 0x20){ ++wEnd;}; // 0x20 == ' '
		--wEnd;
		if(strncmp(wStart, word, wEnd - wStart + 1)){
			strncat(p, wStart, wEnd - wStart + 1);
			p += wEnd - wStart + 1; 
			strcat(p," ");
			++p;
		}
		wStart = wEnd + 1;
		while(*wStart && *wStart == 0x20){ ++wStart;}
	}
}