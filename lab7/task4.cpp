/*
author: soroka
task: reverse in all word
tested: full tested, qwerty qw, a a a, asd asd asd, wert          qwe;
*/
#include <iostream>
#include <cstring>
void inWordReverse(const char *str);
void reverse(char *start ,char *end);
using namespace std;
int main(){
	char  fw[1024];
	cout << "Enter line: ";
	cin.getline(fw,1024);
	cout << "Answer: ";
	inWordReverse(fw);
	system("pause");
	return 0;
}
void inWordReverse(const char *str){
	int length = strlen(str);
	char localStr[length];
	strcpy(localStr,str);
	char *end = localStr + length;
	char *a = localStr,*b;
	while (a < end){
		while (*a == 0x20 && *a){ ++a;} // skip spaces & set world start
		b = a;
		while (*b != 0x20 && *b){ // set world end 
			++b;
		}
		reverse(a,b - 1); // (b-1) because now b is space || \0
		a = b;
	}
	cout << localStr << '\n';
}
void reverse(char *start ,char *end){
	char temp;
	while (start < end){
		temp = *start;
		*start = *end;
		*end = temp; 
		++start;
		--end; 
	}
}