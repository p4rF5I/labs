/*
author: soroka
task: find all combinations(ab = AB ...)
tested: asdqweasd, qwerty, asdfgh, asasasasas, qeq, q,qwertyuiopasdfghjklzxcvbnm
*/
#include <iostream>
#include <cstring>
void calcLetterCombinations(const char *str);
using namespace std;
int main(){
	char str[1024];
	cout << "Enter line: ";
	cin.getline(str,1024);
	cout << "answer:\n";
	calcLetterCombinations(str);
	system("pause");
	return 0;
}
void calcLetterCombinations(const char *str){
	int combination[26][26];
	for (int i = 0; i < 26; ++i){
		for(int j = 0; j < 26; ++j){
			combination[i][j] = 0;
		}
	}
	for (int i = 1; str[i]; ++i){
		if(((str[i] >= 0x41 &&   str[i] <= 0x5A ) || (str[i] >= 0x61 && str[i] <= 0x7A))
			&& ((str[i-1] >= 0x41 &&   str[i-1] <= 0x5A ) || (str[i-1] >= 0x61 && str[i-1] <= 0x7A))){
			// cout << "HERE!\n";
			++combination[((str[i-1] >= 0x61)?(str[i-1] ^ 0x20):str[i-1]) - 0x41][((str[i] >= 0x61)?(str[i] ^ 0x20):str[i]) - 0x41];
		}
	}
	for (int i = 0; i < 26; ++i){
		for (int j = 0; j < 26; ++j){
				if(combination[i][j]){cout << (char)(0x61 + i) << (char)(0x61 + j) << ": " << combination[i][j] << '\n';}
			}
	}
}