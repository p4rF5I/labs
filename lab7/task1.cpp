/*
author: soroka
task: find litter & digits & other
tested: full tested
*/
#include <iostream>
#include <cstring>
unsigned int charactersInLine(const char *str,const char *symblos);
using namespace std;
int main(){
	bool isOK = true;
	char str[1024]; //= "1234567890asdfghjkl<>]]]";
	cout << "Enter sting: ";
	if(!cin.getline(str,1024)){
		cout << "[ERROR] Too much characters for one string!\n";
		isOK = false;
	} 
	if(isOK){
		unsigned int amount = charactersInLine(str, "1234567890") +
				charactersInLine(str, "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM");
		cout << "Digits & letter: " << amount << '\n';
		cout << "Other: " << strlen(str) - amount << '\n';
	}
	system("pause");
	return !isOK;
}
unsigned int charactersInLine(const char *str,const char *symblos){
	unsigned int count = 0;
	int i = 0;
	int j = 0;
	while (symblos[i]){
		while(str[j]){
			if (symblos[i] == str[j]){
				++count; 
			}
			++j;
		}
		++i;
		j = 0;
	}
	return count;
}