/*
author:soroka
task: Digit's sum of number
tested: 0,10,-2,dd,9494933
*/
#include <iostream>
const unsigned int SIZE = 1024;
bool isCorrect(unsigned char *a);
unsigned int sumOfDigits(unsigned char* a);
using namespace std;
int main() {
	unsigned char *a = new unsigned char[SIZE];
	a[0]='a';
	while (!isCorrect(a)) {
		cout << "Enter number: ";
		cin >> a;
		system("cls | clear");
	}
	cout << "Digits sum of " << a << " = " << sumOfDigits(a) << endl;
	delete[] a;
	system("pause");
	return 0;
}
bool isCorrect(unsigned char *a){
	int i = 0;
	while (a[i]) {
		if (((int)(a[i]) < 48) || ((int)a[i] > 57)) return false;
		i++;
	}
	return true;
}
unsigned int sumOfDigits(unsigned char *a) {
	unsigned int sum = 0, i = 0;
	while (a[i]) {
		sum += a[i]-48;
		i++;
	}
	return sum;
}