//stremitsya k e
/*
author:soroka
task: culc this: 1+1/1!+1/2!+..+1/n! (Napier's number)
		//that's easy to proof thar 1+1/1!+1/2!+..+1/n!+..=lim(1+1/n)^n
tested: -3,0,2,1,30
*/
#include <iostream>
double calcNapiersNumber (int n);
using namespace std;
unsigned long long int fact(unsigned long long int num);    // do 20! vkl*/
int main() {
	int n=0;
	while (n<=0) {
		cout << "Enter number: ";
		cin >> n;
		if (n >= 20) n = 20;		//20! enough to get 15-digits precision
		system("cls | clear");
	}
	cout.precision(15);
	cout << calcNapiersNumber(n)<< endl;
	system("pause");
	return 0;
}
unsigned long long int fact(unsigned long long int num) {
	if ((num == 0) || (num == 1)) return 1;
	return fact(num - 1)*num;
}
double calcNapiersNumber(int n) {
	if (n == 1) return 2;
	return (1 / (double(fact(n))) + calcNapiersNumber(n - 1));
}