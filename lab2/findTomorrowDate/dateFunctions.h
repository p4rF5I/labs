bool isDateCorrect(int day, int month, int year);
bool isYearLeap(int year);
void setTomorrow(int &day, int&month, int &year);
int lastDayOfMonth(int month, int year);
void printDate(int day, int month, int year);