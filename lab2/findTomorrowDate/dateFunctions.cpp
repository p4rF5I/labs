#include <iostream>
#include "dateFunctions.h"
using namespace std;
bool isYearLeap(int year) {
	if (!(year % 400)) return true;
	if (!(year % 100)) return false;
	if (!(year % 4)) return true;
	return false;
}
bool isDateCorrect(int day, int month, int year) {
	if (day <= 0 || month <= 0 || year <= 0) return false;
	if (month > 12 || day > 31) return false;
	if (lastDayOfMonth(month, year) < day) return false;
	return true;
}
int lastDayOfMonth(int month, int year) {
	switch (month) {
	case 4:
	case 6:
	case 9:
	case 11:
		return 30;
	case 2:
		if (isYearLeap(year)) return 29;
		return 28;
	default: 	//	cases: 1,3,5,7,8,10,12
		return 31;
	}
}
void setTomorrow(int &day, int&month, int &year) {
	day++;
	if (!isDateCorrect(day, month, year)) {
		day = 1;
		month++;
		if (!isDateCorrect(day, month, year)) {
			month = 1;
			year++;
		}
	}
}
void printDate(int day, int month, int year) {
	if (day < 10)	cout << '0' << day << '.';
	else	cout << day << '.';
	if (month < 10)		cout << '0' << month;
	else	cout << month;
	cout << '.' << year << endl;
}