/*
author: soroka
task:find tomorrow date
tested: full cases tested(leap yeas too)
*/
/*
1 	January 	31
2 	February 	28 (29 in leap)
3 	March 	31
4 	April 	30
5 	May 	31
6 	June 	30
7 	July 	31
8 	August 	31
9 	September 	30
10 	October 	31
11 	November 	30
12 	December 	31 
/400 v
/100 *
/4   v
*/
#include <iostream>
#include "dateFunctions.h"
using namespace std;
int main() {
	int day, month, year;
	do  {
		system("cls | clear");
		cout << "Enter correct date(dd mm yyyy): ";
		cin >> day >> month >> year;
	} while (!isDateCorrect(day, month, year));
	setTomorrow(day, month, year);
	printDate(day, month, year);
	system("pause");
	return 0;
}