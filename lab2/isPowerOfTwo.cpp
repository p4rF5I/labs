/*
author:soroka
task: is 't' power of 2- "YES"
else - "NO"
tested:  1,2,10,11,512,-3,2^20
*/
#include <iostream>
using namespace std;
int main() {
	long int t=0;
	while (t <= 0) {
		cout << "Enter a number: ";
		cin >> t;
		system("cls | clear");
	}
	while (!(t&1)){ // some optimization
		t/=2;
	}
	if (t==1) {
		cout << "YES;\n";
	}
	else {
		cout << "NO!\n";
	}
	system("pause");
	return 0;
}