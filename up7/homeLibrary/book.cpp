#include "book.h"
#include <iostream>
using namespace std;
void Book::print(){
	cout << "Name: " << name << '\n';
	if(author != "null"){
		cout << "Author: " << author << '\n';
	}
	if(year.year != -1){
		cout << "Year: " << year.year << '\n';
	}
	if(price.price != -1){
		cout << "Price: " << price.price << '\n';
	}
}
Book& Book::operator=(const Book &a){
	// cerr << "[DEBUG] Book::operator=\n";
	author = a.author;
	name = a.name;
	year = a.year;
	price = a.price;
	return *this;
}
Book::Book(const Book &a){
	// cerr << "[DEBUG] Book(const Book &a)\n";
	author = a.author;
	name = a.name;
	year = a.year;
	price = a.price;
}
