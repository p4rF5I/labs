#pragma once
	#include <vector>
	#include <string>
	#include "book.h"
	#include <algorithm> 
	using namespace std;
	class HomeLibrary{
	public:
		HomeLibrary(){}
		void find(string name);
		void find(string name, string author);
		void find(string name, string author, Year year);
		void find(Price priceFrom, Price priceTo);
		void find(Year yearFrom, Year yearTo);
		void add(const Book &a){
			lib.emplace_back(a);
			++amount;
		}
		void del(string name, string author, int year, int price);
		void print();
		void sortPrice();
		void sortName();
	private:
		vector<Book> lib;
		int amount = 0;
	};