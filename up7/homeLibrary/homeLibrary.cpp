#include "homeLibrary.h"
#include <cctype>
void HomeLibrary::find(string name){
	for(int i = 0; i < amount; ++i){
		if(lib[i].getName() == name){
			lib[i].print();
		}
	}
}
void HomeLibrary::find(string name, string author){
	for(int i = 0; i < amount; ++i){
		if(lib[i].getName() == name && lib[i].getAuthor() == author){
			lib[i].print();
		}
	}
}
void HomeLibrary::find(string name, string author, Year year){
		for(int i = 0; i < amount; ++i){
		if(lib[i].getName() == name && lib[i].getAuthor() == author
			&& year == lib[i].getYear()){
			lib[i].print();
		}
	}
}
void HomeLibrary::find(Price priceFrom, Price priceTo){
	for(int i = 0; i < amount; ++i){
		if(lib[i].getPrice() >= priceFrom && lib[i].getPrice() <= priceTo){
			lib[i].print();
		}
	}
}
void HomeLibrary::find(Year yearFrom, Year yearTo){
		for(int i = 0; i < amount; ++i){
		if(lib[i].getYear() >= yearFrom && lib[i].getYear() <= yearTo){
			lib[i].print();
		}
	}
}
void HomeLibrary::del(string name, string author, int year, int price){
	for(int i = 0; i < amount; ++i){
		if(lib[i].getName() == name && lib[i].getAuthor() == author
			&& lib[i].getYear() == year && lib[i].getPrice() == price){
			lib.erase(lib.begin() + i);
			--i;
			--amount;
		}
	}
}
void HomeLibrary::print(){
	for(int i = 0; i < amount; ++i){
		lib[i].print();
	}
}
bool cmpPrice(const Book &a, const Book &b){
	return (a.getPrice() > b.getPrice());
}
void HomeLibrary::sortPrice(){
	sort(lib.begin(), lib.begin() + amount, cmpPrice);
}
bool cmpName(const Book &a, const Book &b){
	string a_name = a.getName();
	string b_name = b.getName();
	for(int i = 0; i < a_name.length() && i < b_name.length(); ++i){
		// cout << "hhh: " << a_name << '\t' << b_name << '\t' << a_name[i] << '\t' << tolower(a_name[i]) << '\t' << b_name[i] << '\t' << tolower(b_name[i]) << '\n';
		if(tolower(a_name[i]) == tolower(b_name[i])){continue;}
		if (tolower(a_name[i]) < tolower(b_name[i])){
			// cout << "ggg: " << a_name << '\t' << b_name << '\t' << a_name[i] << '\t' << b_name[i] << '\n';
			return true;
		} else {
			return false;
		}
	}
	if(a_name.length() > b_name.length()){
		return false;
	} else {
		return true;
	}
	// return a.getName().compare(b.getName());
}
void HomeLibrary::sortName(){
	sort(lib.begin(), lib.begin() + amount, cmpName);
}