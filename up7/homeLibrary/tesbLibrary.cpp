/*
author: soroka
task: class library
tested: module tested.
*/
#include <iostream>
#include "homeLibrary.h"
#include "book.h"
using namespace std;
int main(){
	cout << (int) 'n' << '\t' << (int) 'q' << '\n';
	Book myBook("qwerty","Egor", 999 , 2081);
	HomeLibrary myLibrary;
	myLibrary.add(myBook);
	myLibrary.add(Book("name", "author", 228, 339));
	myLibrary.add(Book("name", "author4", 111, 222));
	myLibrary.add(Book("name2", "author2", 229, 340));
	myLibrary.add(Book("name2", "author2", 1000, 221));
	myLibrary.add(Book("name3", "author3", 227, 337));
	myLibrary.add(Book("o", "author3", 227, 337));
	myLibrary.add(Book("a", "author3", 227, 337));
	myLibrary.add(Book("b", "author3", 227, 337));
	myLibrary.add(Book("name", "author66", 333333, 222));
	myLibrary.add(Book("qaerty","Egor", 999 , 2081));
	myLibrary.add(Book("qwer","Egor", 999 , 2081));
	myLibrary.print();
	cout << "-------------\n";
	myLibrary.find("name");
	myLibrary.del("name", "author66", 333333, 222);
	cout << "+++++++++++++++++++++++\n";
	myLibrary.find("name");
	cout << "**********************\n";
	myLibrary.find(Price(100), Price(300));
	cout << "[][][][][][][][][][][][][][]\n";
	myLibrary.find(Year(100), Year(300));
	cout << "&&&&&&&&&&&&&&&&&&&&&&&\n";
	myLibrary.sortPrice();
	myLibrary.print();
	cout << "iiiiiiiiiiiiiiiiiiiiiii\n";
	myLibrary.sortName();
	myLibrary.print();
	// myBook.print();
	system("pause");
	return 0;
}