#pragma once
#include <string>
#include <iostream>
using namespace std;
class Year{
public:
	int year;
	Year(){year = -1;}
	Year(int a){year = a;}
	bool operator==(const Year &a){
		return year == a.year;
	}
	bool operator>=(const Year &a){
		return year >= a.year;
	}
	bool operator<=(const Year &a){
		return year <= a.year;
	}
	bool operator>(const Year &a){
		return year > a.year;
	}
	bool operator<(const Year &a){
		return year < a.year;
	}
	bool operator==(const int a){
		return year == a;
	}
	bool operator>=(const int a){
		return year >= a;
	}
	bool operator<=(const int a){
		return year <= a;
	}
	bool operator>(const int a){
		return year > a;
	}
	bool operator<(const int a){
		return year < a;
	}
};
class Price{
public:
	int price;
	Price(){price = -1;}
	Price(int a){price = a;}
	bool operator==(const Price &a){
		return price == a.price;
	}
	bool operator>=(const Price &a){
		return price >= a.price;
	}
	bool operator<=(const Price &a){
		return price <= a.price;
	}
	bool operator>(const Price &a){
		return price > a.price;
	}
	bool operator<(const Price &a){
		return price < a.price;
	}
	bool operator==(const int a){
		return price == a;
	}
	bool operator>=(const int a){
		return price >= a;
	}
	bool operator<=(const int a){
		return price <= a;
	}
	bool operator>(const int a){
		return price > a;
	}
	bool operator<(const int a){
		return price < a;
	}
};
class Book{
public:
	void print();
	Book(const string name_,const string author_, 
		const int year_, const int price_): name(name_), author(author_){
		year.year = year_;
		price.price = price_;
		// cerr << "[DEBUG] book constr\n";
	}
	Book(const Book &a);
	Book& operator=(const Book &a);
	string getAuthor() const{
		return author;
	}
	string getName() const{
		return name;
	}
	Year getYear() const{
		return year;
	}
	Price getPrice() const{
		return price;
	}
private:
	string author;
	string name;
	Price price;
	Year year;
	// int amount = -1;
};