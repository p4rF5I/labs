#include <iostream>
#include "menu.h"
using namespace std;
void f1();
void f2();

int main(){
	std::vector<Choice> v;
	v.emplace_back(Choice("hi",&f1));
	v.emplace_back(Choice("egor",&f2));
	Menu menu(v);
	menu.print();
	menu.makeChoice();
	return 0;
}
void f1(){
	cout << "f1\n";
}
void f2(){
	cout << "f2\n";
}