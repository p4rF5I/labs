/*
author: soroka
task: vect class
tested: with menu;
*/
#include <iostream>
#include "vect.h"
// #include "testMenu.h"
#include <assert.h>
void printMenu();
void execute(int op, Vect<int> &a, Vect<int> &b);
using namespace std;
int main(){
	cout << "Enter sizes of vector's: ";
	int a_size;
	int b_size;
	cin >> a_size >> b_size;
	Vect<int> a(a_size);
	cout << "Enter a vect elements:\n";
	for(int i = 0; i < a_size; ++i){
		cout << i << ": ";
		cin >> a[i];
	}
	int *b_array = new int[b_size];
	cout << "Enter b vect elements:\n";
	for(int i = 0; i < b_size; ++i){
		cout << i << ": ";
		cin >> b_array[i];
	}
	Vect<int> b(b_array, b_size);
	delete[] b_array;
	bool isExit = false;
	int operation;
	while(!isExit){
		cout << "-------------------\n";
		a.print();
		b.print();
		cout << "-------------------\n";
		printMenu();
		cout << "Enter operation: ";
		cin >> operation;
		system("clear || cls");
		if(operation != 0){
			execute(operation, a, b);
		} else {
			isExit = true;
		}
	}
	system("pause");
	return 0;
}
void printMenu(){
	cout << "0 - exit\n";
	cout << "1 - =\n";
	cout << "2 - +=\n";
	cout << "3 - -=\n";
	cout << "4 - *=\n";
	cout << "5 - /=\n";
	cout << "6 - +\n";
	cout << "7 - -\n";
	cout << "8 - ub\n";
	cout << "9 - print\n";
	cout << "10 - []\n";
}
void execute(int op, Vect<int> &a, Vect<int> &b){
	int temp = 0;
	switch(op){
		case 1:
			a = b;
			break;
		case 2:
			a += b;
			break;
		case 3:
			a -= b;
			break;
		case 4:
			cout << "mul: ";
			cin >> temp;
			a *= temp;
			break;
		case 5:
			cout << "div: ";
			cin >> temp;
			a /= temp;
			break;
		case 6: 
			a = a + b;
			break;
		case 7:
			a = a - b;
			break;
		case 8:
			cout << "b.ub(): " << a.ub() << '\n';
			cout << "a.ub(): " << b.ub() << '\n';
			break;
		case 9:
			a.print();
			b.print();
			break;
		case 10:
			cout << "enter i: ";
			cin >> temp;
			cout << "a[" << temp << "]: " << a[temp] << '\n';
			cout << "b[" << temp << "]: " << b[temp] << '\n';
			break;
	}
}