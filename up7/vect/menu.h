#ifndef class_menu
#define class_menu
#include <string>
#include <iostream>
#include <vector>
// int amount = 0;
class Choice{
public:
	Choice(const std::string message_, void (*operation_)());
	void print() const;
	void exec() const;
private:
	// static int amount;
	int id;
	std::string message; 
	void (*operation)() = nullptr;
};
class Menu{
public:
	int getLastChoice() const{
		return lastChoice;
	}
	Menu(const std::vector<Choice> choices_);
	void print();
	int makeChoice();
	void exec();
	// void exec(int);

private:
	int lastChoice;
	int choice = -1;
	std::vector<Choice> choices;
};
// int amount = 0;

#endif