#include "menu.h"
int amount = 0;
Menu::Menu(const std::vector<Choice> choices_){
	choices = choices_;
}
void Menu::print(){
	for(int i = 0; i < choices.size(); ++i){
		choices[i].print();
	}
	std::cout << "Enter your choice: ";
}
	int Menu::makeChoice(){
		lastChoice = choice;
		std::cin >> choice;
	}
Choice::Choice(const std::string message_, void (*operation_)()){
	id = amount;
	++amount;
	message = message_;
	operation = operation_;
	// for (int i = 0; i < n; ++i) 
}
void Choice::exec() const{
	std::cerr << "[DEBUG] " << id << " executing ...\n";
	operation();
}
void Choice::print() const{
	std::cerr << "[DEBUG] " << id << " print\n";
	std::cout << id << " - " << message << '\n';
}
void Menu::exec(){
	std::cerr << "[DEBUG] choice: " << choice << "executing\n";
	if(choice != -1){
		choices[choice].exec();
	}
}