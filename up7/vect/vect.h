#ifndef vect_class
#define vect_class
#include <iostream>
#include <assert.h>
using namespace std;
template <typename T> class Vect{
	int size = -1;
	T* p;
public:
	Vect();
	Vect(const Vect& v);
	Vect(const T a[], int n);
	Vect(int i);
	~Vect();
	Vect& operator=(const Vect<T> &v);
	T& operator[](int i) const;
	void print() const;
	Vect& operator*=(const T var);
	Vect& operator/=(const T var);
	Vect& operator+=(Vect<T> &v);
	Vect& operator-=(Vect<T> &v);
	Vect operator+(const Vect<T> &a);
	Vect<T> operator-(const Vect<T> &a);
	int ub() const{return (size-1);}
	void fillWithZero();
};
template <typename T>
Vect<T>::Vect(){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect<T>::Vect()\n";
	#endif
	size = 10;
	p = new T[size];
	fillWithZero();
}
template <typename T>
Vect<T>::Vect(const Vect& v){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect(const Vect& v)\n";
	#endif
	size = v.ub() + 1;
	p = new T[size];
	for(int i = 0; i < size; ++i){
		p[i] = v[i];
	}
}
template <typename T>
Vect<T>::Vect(int i){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect(int i)\n";
	#endif
	size = i;
	p = new T[size];
	fillWithZero();
}
template <typename T>
Vect<T>::Vect(const T a[], int n){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect(const T a[], T n)\n"; 
	#endif
	size = n;
	p = new T[size];
	for(int i = 0; i < size; ++i){
		p[i] = a[i];
	}
}
template <typename T>
Vect<T>::~Vect(){
	#ifdef DEBUG
	cerr << "[DEBUG] destr.\n";
	#endif
	size = -1;
	delete[] p;
}
template <typename T>
Vect<T>& Vect<T>::operator=(const Vect<T> &v){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect& Vect::operator=(T v)\n";
	#endif
	if (size != v.ub() + 1){
		size = v.ub() + 1;
		delete[] p;
		p = new T[size];
	}
	for(int i = 0; i < size; ++i){
		p[i] = v[i];
	}
	return *this;
}
template <typename T>
T& Vect<T>::operator[](int i) const{
	#ifdef DEBUG
	cerr << "[DEBUG] T& Vect::operator[](int i)\n";
	#endif
	assert(i >= 0 && i < this -> size);
	return p[i];
}
template <typename T>
void Vect<T>::print() const{
	#ifdef DEBUG
	cerr << "[DEBUG] print()\n";
	#endif
	for (int i = 0; i < size; ++i){
		cout << p[i] << ", ";
	}
	cout << "\b\b.\n";
}
template <typename T>
Vect<T>& Vect<T>::operator*=(const T var){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect<T>::operator*(const T i)\n";
	#endif
	for (int i = 0; i < size; ++i){
		p[i] *= var;
	}
	return *this;
}
template <typename T>
void Vect<T>::fillWithZero(){
	#ifdef DEBUG
	cerr << "[DEBUG] fillWithZero()\n";
	#endif
	for(int i = 0; i < size; ++i){
		p[i] = 0;
	}
}
template<typename T>
Vect<T>& Vect<T>::operator/=(const T var){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect<T>& operator/=(const T i)\n";
	#endif
	assert(var != 0);
	for (int i = 0; i < size; ++i){
		p[i] /= var;
	}
	return *this;
}
template <typename T>
Vect<T>& Vect<T>::operator+=(Vect<T> &v){
	assert(size == (v.ub() + 1));
	for(int i = 0; i < size; ++i){
		p[i] += v[i];
	}
	return *this;
}
template <typename T>
Vect<T>& Vect<T>::operator-=(Vect<T> &v){
	#ifdef DEBUG
	cerr << "[DEBUG] Vect<T>& Vect<T>::operator-=(Vect<T> &v)\n";
	#endif
	assert(size == v.ub() + 1);
	for(int i = 0; i < size; ++i){
		p[i] -= v[i];
	}
	return *this;
}
template <typename T>
Vect<T> Vect<T>::operator+(const Vect<T> &a){
	#ifdef DEBUG
	cerr << "[DEBUG] operator+\n";
	#endif
	assert((size - 1) == a.ub());
	Vect t(a);
	t += *this;
	return t;
}
template <typename T>
Vect<T> Vect<T>::operator-(const Vect<T> &a){
	#ifdef DEBUG
	cerr << "[DEBUG] operator-\n";
	#endif
	assert(size - 1 == a.ub()); 
	Vect t(a);
	t -= *this;
	return t;
}
#endif