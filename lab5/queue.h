#ifndef my_queue
#define my_queue
#include <iostream>
template <class T>class Node{
	private:
		T key;
		Node *prev = nullptr;
	public:
		Node(T var, Node* prevPtr): prev(prevPtr), key(var){}
		Node<T> *getPrev() const {
			return prev;
		}
		void setPrev(Node *ptr){
			prev = ptr;
		}
		void setKey(T var){
			key = var;
		}
		T getKey() const{
			return key;
		}
};
template <class T> class Queue{
	Node<T> *head = nullptr;
	Node<T> *tail = nullptr;
public:
	~Queue(){
		while(!isEmpty()){
			pop();
		}
	}
	void push(T var){
		if(!isEmpty()){
			tail -> setPrev(new Node<T>(var,nullptr));
			tail = tail -> getPrev();
		} else {
			tail = head = new Node<T>(var,nullptr);
		}
	}		
	T pop(){
		if(!isEmpty()){
			T temp = head -> getKey();
			// delete head;
			if(head == tail){
				delete head;
				//delete tail;
				head = tail = nullptr;
				return temp;
			} else {
				Node<T> *tempPtr = head -> getPrev();
				delete head;
				head = tempPtr;
				return temp;
			}
		} else {
			std::cout << "\n**********************************\n"
				 << "******[ERROR] Queue is free!******\n"
				 << "**********************************\n";
		}
	}
	bool isEmpty(){
		if(head == nullptr){
			return true;
		} else {
			return false;
		}
	}
};
#endif
