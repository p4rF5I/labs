/*
author:soroka
task: fill with even numbers
	testd: 10, 7, -1, 1 , 4, 0, 20, 4, 5 - test passed
*/
#include <iostream>
void specialMatrixFilling (int **matrix,int size);
template <class T> void printMatrix(T **start, int iMAX, int jMAX);
using namespace std;
int main(){
	int size;
	bool isOK=true;
	cout << "Enter size of matrix(nxn),n = ";
	if (!(cin >> size) || size <= 0){
		cout << "[ERROR]Input error!\n";
		isOK = false;
	}
	if(isOK){
		int **matrix = new int *[size];
		for(int i = 0; i < size;++i){
			matrix[i]= new int [size];
		}
		system("clear || cls");
		specialMatrixFilling(matrix,size);
		printMatrix(matrix,size-1,size-1);
		for (int i = 0; i < size;++i){
			delete[] matrix[i];
		}
		delete [] matrix;
	}
	system("pause");
	return !isOK;
}
template <class T> void printMatrix(T **start, int iMAX, int jMAX){
	for (int i=0;i<=iMAX;++i){
		for (int j=0;j<=jMAX;++j){
			cout << start[i][j] << '\t';
		}
		cout << '\n';
	} 
}
void specialMatrixFilling (int **matrix,int size){
	int endI = size/2, endJ = size/2 + 1 - ((size + 1) & 1);
	// cout << endI << '\t' << endJ;
	int i = 0,j = 0;
	int num = 2;
	int border = 0;
	// cout << endI << '\t' << endJ << '\n';
	while ((j != endJ) || (i != endI)){
		// **->**
		// ******
		// ******
		while(j < size){
			matrix[i][j] = num;
			++j; 
			num+=2;
		}
		--j;
		++i;
		//****
		//***V
		//**** 
		while(i < size){
			matrix[i][j] = num;
			++i;
			num+=2;
		}
		--i;
		--j;
		//****** 
		//******
		//**<-**
		while(j > border-1){
			matrix[i][j] = num;
			--j;
			num+=2;
		}
		--i;
		++j;
		//****
		//^***
		//****
		while(i > border){
			matrix[i][j] = num;
			--i;
			num+=2;
		}
		++i;
		++j;
		++border;
		--size;
		// cout << i << '\t' << j << '\n';
	}
	// cout << i << '\t' << j << '\n';
}