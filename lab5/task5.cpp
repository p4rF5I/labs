/*
author:soroka
task: ways
testd:  random tests - passed;
*/
#include <iostream>
#include <time.h>
#include "myQueue.h"
using namespace std;
void printPossibleWays(int **matrix, bool **isNotUsed, int stringsAmount , int columnsAmount, int i, int j);
template <class T> void printMatrix(T **matrix, int stringsAmount, int columnsAmount);
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount);
int main (){
    bool isOK = true;
    int stringsAmount,columnsAmount;
    cout << "Enter size of matrix(strings columns): ";
    if (!(cin >> stringsAmount) || !(cin >> columnsAmount) 
        || stringsAmount <= 0 || columnsAmount <= 0){
        cout << "[ERROR]Input error!\n";
        isOK = false;
    }
    if (isOK){
        bool **isNotUsed; 
        createMatrix(isNotUsed,stringsAmount,columnsAmount);
        int **matrix; 
        createMatrix(matrix,stringsAmount,columnsAmount);
        cout << "Wish random nambers(use: 0 -NO || t to 0=<x<=t): ";
        int t;
        if (!(cin >> t) || !t){
            cout << "Enter array elements: ";
            for (int i = 0;i < stringsAmount && isOK; ++i){
                for(int j= 0;j < columnsAmount && isOK; ++j){
                    isNotUsed[i][j] = 1; // just filling isNotUsed with 1
                    cout << "Enter matrix[" << i << "][" << j << "]\n";
                    if (!(cin >> matrix[i][j])) {
                        cout << "[ERROR] array initialization error!\n";
                        for (int i = 0;i < stringsAmount; ++i){
                            delete[] matrix[i];
                        }
                        delete [] matrix;
                        isOK = false;
                    }
                }
            }
        } else {
            srand(time(NULL));
            for (int i = 0;i < stringsAmount; ++i){
                for(int j= 0;j < columnsAmount; ++j){
                    isNotUsed[i][j] = 1; // just filling isNotUsed with 1
                    matrix[i][j] = rand()%t+1;
                }
            }
    }
        if (isOK){
            system("clear || cls");
            printMatrix(matrix,stringsAmount,columnsAmount);
            cout << "Enter your point(i j) :";
            int i,j;
            cin >> i >> j;
            printPossibleWays(matrix,isNotUsed,stringsAmount,columnsAmount,i,j);
                // free memore
            for (int i = 0 ;i < stringsAmount; ++i){
                delete[] matrix[i];
                delete[] isNotUsed[i];
            }
            delete [] matrix;
            delete [] isNotUsed;
        }
    }
    system("pause");
    return !isOK;
}
template <class T> void printMatrix(T **matrix, int stringsAmount, int columnsAmount){
    for (int i=0;i < stringsAmount; ++i){
        for (int j = 0;j < columnsAmount; ++j){
            cout << matrix[i][j] << '\t';
        }
        cout << '\n';
    } 
}
void printPossibleWays(int **matrix, bool **isNotUsed, int stringsAmount , int columnsAmount, int i, int j){
    queue::addToBegin(i);
    queue::addToBegin(j);
    while(queue::Queue.end != NULL){
        i = queue::takeFromEnd();
        j = queue::takeFromEnd();
        if (columnsAmount > j + 1 && isNotUsed[i][j+1] && matrix[i][j] > matrix[i][j+1]){
            cout << '[' << i <<  ']' << '[' << j+1 <<  ']' << ',';
            isNotUsed[i][j+1] = false;
            queue::addToBegin(i);
            queue::addToBegin(j+1);
        }
        if (0 <= j - 1 && isNotUsed[i][j-1] && matrix[i][j] > matrix[i][j-1]){
            cout << '[' << i <<  ']' << '[' << j-1 <<  ']' << ',';
            isNotUsed[i][j-1] = false;
            queue::addToBegin(i);
            queue::addToBegin(j-1);
        }
        if (stringsAmount > i + 1 && isNotUsed[i+1][j] && matrix[i][j] > matrix[i+1][j]){
            cout << '[' << i+1 <<  ']' << '[' << j <<  ']' << ',';
            isNotUsed[i+1][j] = false;
            queue::addToBegin(i+1);
            queue::addToBegin(j);
        }
        if (0 <= i - 1 && isNotUsed[i-1][j] && matrix[i][j] > matrix[i-1][j]){
            cout << '[' << i-1 <<  ']' << '[' << j <<  ']' << ',';
            isNotUsed[i-1][j] = false;
            queue::addToBegin(i-1);
            queue::addToBegin(j);
        }
            // printMatrix(isNotUsed,stringsAmount,columnsAmount);
    }
    cout << "\b.\nFarest point: [" << i << "][" << j << "]\n";

}
template <class T> void createMatrix(T** &matrix,int stringsAmount,int columnsAmount){
        matrix = new T *[stringsAmount];
        for(int i = 0; i < stringsAmount;++i){
            matrix[i]= new T [columnsAmount];
        }  
}