/*
author:soroka
task: insetrion sort with binary searh
testd:  1, 92, 4, 23, 4, 1, 0, 2, 2, 2
64, 51, 33, 83, 59, 33, 82, 86, 10, 73
14, 3, 7, 11, 7, 15, 9, 17, 3, 19
//
500, 541, 573, 364, 27, 205, 336, 795, 437, 128, 185, 148, 348, 751, 785, 174, 318,
518, 408, 137, 738, 325, 750, 717, 28, 202, 549, 869, 481, 52, 85, 980, 944, 9, 343,
322, 213, 30, 116, 1, 158, 652, 148, 857, 402, 285, 30, 71, 802, 789, 559, 891, 465,
308, 607, 844, 509, 156, 713, 341, 559, 149, 321, 854, 157, 15, 176, 721, 397, 643,
722, 554, 295, 221, 762, 48, 505, 791, 471, 306, 931, 29, 196, 396, 689, 155, 239,
197, 310, 303, 890, 220, 803, 562, 73, 311, 576, 600, 32, 972.
length: -1, 0, -100
*/
#include <iostream>
#include <time.h>
using namespace std;
void insertionSort (int *start, int *end);
void mySwap(int *a,int *b);
template <class T> void printArray(T *start, T *end);
int *binarySearch(int *array,int size, int element);
int main (){
    int size;
    bool isOK = true;
    cout << "Enter size of array: ";
    if (!(cin >> size) || size <= 0){
        cout << "[ERROR]Input error!\n";
        isOK = false;
    }
    if(isOK){
        int *array = new int [size];
        int t;
        cout << "Wish random nambers(use: 0 - NO || t to 1=<x<=t): ";
        if (!(cin >> t) || !t){
            cout << "Enter array elements: ";
            for (int i = 0;i < size; i++){
                 if (!(cin >> array[i])) {
                    cout << "[ERROR] array initialization error!\n";
                    delete [] array;
                    isOK = false;
                    break;
                }
            }
        } else {
            srand(time(NULL));
            for (int i=0; i<size; ++i){
                array[i]= rand()%t+1;
            }
        }
        if(isOK){
            system("clear || cls");
            printArray(array,array + size - 1);
            insertionSort(array,array + size - 1);
            printArray(array,array + size - 1);
            system("pause");
            delete [] array;
        }
    }
    return 0;
}
void insertionSort(int *start,int *end){
    if(end <= start) return;
    int *p;   
    int *pCorrect = start + 1; 
    ++end; //some optimizapion
    while (pCorrect != end){
        p = binarySearch(start, pCorrect - start, *(pCorrect));
        // vstavlyaem p i sdvigaem vse vpravo
        while (p < pCorrect){
            mySwap(pCorrect,p);
            ++p;
        }
    ++pCorrect;
    }
}
void mySwap(int *a,int *b){
    int c = *a;
    *a = *b;
    *b = c;
}
template <class T> void printArray(T *start, T *end){
    if(start > end) return;
    while (start <= end){
        std::cout << *start << ", ";
        ++ start;
    }
    std::cout << "\b\b.\n";
}
int *binarySearch(int *array,int size, int element){
    // cout << "here: " << *array << '\t' << *(array + size -1) << '\t' << element << '\n';
    int *end = array + size - 1;
    int *left = array;
    int *right = end;
    if (*end <= element){
        return array + size;
    }
    if (*array >= element){
        return array;
    }
    int *mid;
    while (right - left > 0) {
        mid = left + (right - left)/2;
        if (element <= *mid) {
            right = mid;
        } else {
            left = mid + 1;
        }
    }
    return right;
}