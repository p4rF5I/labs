/*
author:soroka
task: is magic
testd:	4 9 2	0 0 0	1 2 3	7 8 9	3 9 2	1 1 1
		3 5 7	0 0 0	4 5 6	4 5 6	3 5 7	1 1 1
		8 1 6	0 0 0	7 8 9	1 2 3	8 1 6	1 1 1

	16	3	2	13	
	5	10	11	8	
	9	6	7	12	
	4	15	14	1	
*/
#include <iostream>
long int clacColumnSum(int **start,int j,int iMAX);
long int clacStringSum(int **start,int i,int jMAX);
bool isMagic(int **matrix, int szie);
template <class T> void printMatrix(T **start, int iMAX, int jMAX);
template <class T> bool enterMatrix(T **matrix,int iMAX, int jMAX);
template <class T> void deleteMatrix(T **matrix, int iMAX);
using namespace std;
int main(){
	int size;
	bool isOK = true;
	cout << "Enter size of matrix(nxn),n = ";
	if (!(cin >> size)){
		cout << "[ERROR]Input error!\n";
		isOK = false;
	}
	if(isOK){
		int **matrix = new int *[size];
		for(int i = 0; i < size; ++i){
			matrix[i] = new int [size];
		}
		if(enterMatrix(matrix, size - 1, size - 1)){
			cout << "[ERROR] array initialization error!\n";
			deleteMatrix(matrix, size - 1);
			isOK = false;
		}
		if(isOK){
			system("clear || cls");
			printMatrix(matrix,size-1,size-1);
			if (isMagic(matrix,size)){
				cout << "Y\n";
			} else {
				cout << "N\n";
			}
			deleteMatrix(matrix, size - 1);
		}
	}
	system("pause");
	return !isOK;
}
template <class T> void printMatrix(T **start, int iMAX, int jMAX){
	for (int i = 0;i <= iMAX; ++i){
		for (int j = 0 ; j <= jMAX; ++j){
			cout << start[i][j] << '\t';
		}
		cout << '\n';
	} 
}
bool isMagic(int **matrix,int size){
	// equal element check
	for(int i = 0;i < size; ++i){
		for(int j = 0; j < size; ++j){
			for(int j2 = j + 1; j2 < size; ++j2){
				if (matrix[i][j] == matrix[i][j2]){
					return false;
				}
			}
			for (int i2 = i + 1; i2 < size; ++i2){
				for(int j2 = 0; j2 < size; ++j2){
					if (matrix[i][j] == matrix[i2][j2]){
						return false;
					}
				}
			}
		} 
	}
	long int magicConst = clacStringSum(matrix, 0, size - 1);
	long int sumDiagonal1 = 0, sumDiagonal2 = 0;
	// diagonals sum check
	for (int i = 0; i < size; ++i){
		sumDiagonal1 += matrix[i][i];
		sumDiagonal2 += matrix[size - i - 1][i];
	}
	if(magicConst != sumDiagonal1 || magicConst != sumDiagonal2){
		return false;
	}
	// columns and stings sum check
	for (int k = 0; k < size; ++k){
		if (clacColumnSum(matrix, k, size - 1) != magicConst ||
		 clacStringSum(matrix, k, size - 1) != magicConst){
			return false;
		}
	}
	return true;

}
long int clacColumnSum(int **start, int j ,int iMAX){
	long int sum = 0;
	for (int i = 0; i <= iMAX; ++i){
		sum += start[i][j];
	}
	return sum;
}
long int clacStringSum(int **start,int i,int jMAX){
	long int sum=0;
	for (int j = 0; j <= jMAX; ++j){
		sum += start[i][j];
	}
	return sum;
}
template <class T> bool enterMatrix(T **matrix, int iMAX, int jMAX){
	cout << "Enter array elements: ";
	for (int i = 0;i <= iMAX; ++i){
		for(int j= 0;j <= jMAX; ++j){
			cout << "Enter matrix[" << i << "][" << j << "]\n";
			if (!(cin >> matrix[i][j])) {
				return true;
			}
		system("clear || cls");
		}
	}
	return false;	
}
template <class T> void deleteMatrix(T **matrix, int iMAX){
	for (int i = 0; i <= iMAX; ++i){
		delete[] matrix[i];
	}	
	delete [] matrix;
}