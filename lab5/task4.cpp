/*
author:soroka
task: find min and do specialSort 
testd:	random tests;
		1, 2, 3, 4.		8, 9.;
		1, 2, 3, 4. 	1, 2, 3, 4.;
		1, 2, 3, 4.		1, 2, 3, 4, 5.;
		1, 2, 3, 4. 	4, 3, 2, 1.;
		1, 2, 3, 4. 	1.;
		1				1
		1. 				2, 2.
		*incorrect size tests*

*/
#include <iostream>
template <class T> T *substrFind(T *strA, int  sizeA, T *strB, int sizeB);
template <class T> bool subsetFind(T *strA,int sizeA, T *strB, int sizeB);
template<class Y> bool enterArray(Y *array, int size,const char *arrayName = "array");
template <class T> void printArray(T *start, T *end);
bool enterSize(int &size,const char *enterSizeMessage = "Enter size of first array: ");
using namespace std;
int main(){
	bool isOK = true;
	int sizeA,sizeB;
	if(enterSize(sizeA, "Enter size of first array: ")
		|| enterSize(sizeB, "Enter size of second array: ")){
		isOK = false;
	}
	if (isOK){
		int *arrayA = new int[sizeA];
		int *arrayB = new int[sizeB];
		if (enterArray(arrayA,sizeA, "A") || enterArray(arrayB,sizeB,"B")){
			delete[] arrayA;
			delete[] arrayB;
			isOK = false;
		}
		if(isOK){
			int *p = substrFind(arrayA,sizeA,arrayB,sizeB);
			printArray(arrayA,arrayA+sizeA-1);
			printArray(arrayB,arrayB+sizeB-1);
			cout << "p: " << p << '\n';
			if(p) cout <<  "*p: " << *p << '\n';
			if (subsetFind(arrayA,sizeA,arrayB,sizeB)){
				cout << "Y\n";
			} else {
				cout << "N\n";
			}
			delete[] arrayA;
			delete[] arrayB;
		}
	}
	// system("pause");
	return !isOK;
}
template <class T> T *substrFind(T *strA, int sizeA, T *strB, int sizeB){
	// cout << strA << '\t' << strB << '\n';
	int j = 0;
	int end = sizeA - sizeB;
	for (int i = 0, temp = 0 ; i <= end; ++i){
		temp = i;
		j = 0;
		// cout << "x: "<< *(strA+i) << '\n';
		while(strA[temp] == strB[j] && j < sizeB){
			++j; ++temp;
		}
		if(sizeB == j){
			// cout << "yep!\n";
			return strA + i;
		}
	}
	// cout << "nope!\n";
	return 0;
}
template <class T> bool subsetFind(T *strA,int sizeA, T *strB, int sizeB){
	bool *isUsed = new bool[sizeA];
	for(int i = 0; i < sizeA; ++i){
		isUsed[i]=false;
	}
	for(int i = 0; i < sizeB; ++i){
		for (int j = 0; j < sizeA; ++j){
			if (strB[i] == strA[j] && !isUsed[j]){
				isUsed[j] = true;
				break;
			} 
			if(j == (sizeA - 1)){
				delete[] isUsed;
				return false;
			}
		}
	}
	delete[] isUsed;	
	return true;
}
bool enterSize(int &size,const char *enterSizeMessage){
	cout << enterSizeMessage;
    if (!(cin >> size) || size <= 0){
    	cout << "[ERROR]Input size error!\n";
        return true;
    }
    return false;
}
template<class Y> bool enterArray(Y *array, int size, const char* arrayName){
/*	Y t;
	cout << "Wish random nambers(use: 0 -NO || t to 1=<x<=t): ";
    if (!(cin >> t) || !t){*/
        cout << "Enter array elements: \n";
        for (int i = 0;i < size; i++){
        	cout << "Enter " << arrayName << " [" << i << "]: ";
            if (!(cin >> array[i])) {
                cout << "[ERROR] array initialization error!\n";
                return true;
            }
        }
        system("clear || cls");
    /*} else {
        srand(time(NULL));
        for (int i=0; i<size; ++i){
            array[i]= rand()%t+1;
        }
    }*/
    return false;
}
template <class T> void printArray(T *start, T *end){
    if(start > end) return;
    while (start <= end){
        std::cout << *start << ", ";
        ++ start;
    }
    std::cout << "\b\b.\n";
}