/*
author: soroka
task: Дано математическое выражение в виде строки
символов. Напишите программу, которая определит, правильно ли
расставлены скобки в выражении, если оно состоит из скобок
типа: ( ) [ ] { }. (использовать структуру данных стек).
tested: 
()
())
(()
({[]})
[]()
[]]()
()()()()[]{}{}
[{[[[[[(((((())))))]]]]]}]
(){[](})
))
(()asd)
*/
#include <iostream>
#include <cstring>
#include "stack.h"
using namespace std;
bool isBracketOk(char *a);
int main(){
	char str[1024] = "\0";
	cout << "Enter yout str: ";
	cin >> str;
		if(isBracketOk(str)){
			cout << "Y\n";
		} else {
			cout << "N\n";
		}
	system("pause");
	return 0;
}
bool isBracketOk(char *a){
	Stack<char> tempStack;
	char temp;
	int len = strlen(a);
	for (int i = 0; i < len; ++i){
		if(a[i] == '('  || a[i] == '{' || a[i] == '['){
			tempStack.push(a[i]);
		}
		if(a[i] == ')'  || a[i] == '}' || a[i] == ']'){
			if(tempStack.isEmpty()){
				return false;
			}
			temp = tempStack.pop();
			if(a[i] == ')'){
				if(temp != '('){
					return false;
				}
				continue;
			}
			if(a[i] == ']'){
				if(temp != '['){
					return false;
				}
				continue;
			}
			if(a[i] == '}'){
				if(temp != '{'){
					return false;
				}
				continue;
			}
		}
	}
	if(tempStack.isEmpty()){
		return true;
	} else {
		return false;
	}
}