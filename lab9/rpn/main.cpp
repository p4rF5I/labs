#include <iostream>
#include "stack.h"
// #include "item.h"
#include "expression.h"
using namespace std;
int main(int argc, char* argv[]){
	string buff;
	if(argc != 2){
		cout << "Enter expression(without ' '): ";
		cin >> buff;
	} else {
		buff = argv[1];
	}
	Expression rpn(buff);
	rpn.print();
	return 0;
}
/*int calc(Stack<int> &a){
	// reverse 
	Stack<int> temp;
	while(!a.isEmpty()){
		temp.push(a.pop());
	}
	int tempVar;
	while(!temp.isEmpty()){
		tempVar = temp.pop();
		if(isOperation(tempVar)){
			if(tempVar == '$'){
				// cout << "minus\n";
				a.push(executeOperation(tempVar,a.pop()));
				continue;
			}
			// cout << "binop: \n";
			a.push(executeOperation(tempVar,a.pop(),a.pop()));
		} else {
			// cout << "a.push: " << tempVar << '\n';
			a.push(tempVar);
		}
	}
	return a.pop();
}*/