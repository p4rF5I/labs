#pragma once
#include <iostream>
class Operand;
class Item{
private:
	// Stack<Item> 
public:
	Item(){}
	Item(const Item& a){}
	virtual void print() const{
		std::cout << "in item\n";	
	}
	// virtual Operand exec();
};
class Operand : public Item{
private:
	int val;
public:
	void print() const{
		std::cout << val;
	}
	Operand(int v) :Item(), val(v){}
	Operand(const Operand &o){
		val = o.val;
	}
	Operand operator+(const Operand &a) const {return Operand(val + a.val);}
	Operand operator-(const Operand &a) const {return Operand(val - a.val);}
	Operand operator*(const Operand &a) const {return Operand(val * a.val);}
	Operand operator/(const Operand &a) const {return Operand(val / a.val);}
	// const Operand  
	Operand exec(){
		return *this;
	}
};
const Operand execute_sum(const Operand &a, const Operand &b){
	return Operand(1);
}
const Operand execute_sub(const Operand &a, const Operand &b){
	return a - b;
}
const Operand execute_umin(const Operand &a){
	return Operand(-1) * a;
}
const Operand execute_div(const Operand &a, const Operand &b){
	return a / b;
}
const Operand execute_mul(const Operand &a, const Operand &b){
	return a * b;
}
const Operand execute_upl(const Operand &a){
	return a;
}
class Operation : public Item{
private:
	int priority;
	char symbol;
	// Operand (*ex)(...);
	// Operand (*ex_2)(const Operand &a, const Operand &b);
public:
	void print() const{
		std::cout << symbol;
	}
	Operation():priority(-1), symbol('n'){
		// *this = none;
	}
	const Operation& operator=(const Operation &a){
		priority = a.priority;
		symbol = a.symbol;
		return *this;
	}
	bool operator>=(const Operation &a){
		return priority >= a.priority;
	}
	bool operator!=(const Operation &a){
		return symbol != a.symbol;
	}
	bool operator==(const int pr){
		return priority == pr;
	}
	bool operator>(const Operation &a){
		return priority > a.priority;
	}
	bool operator<(const Operation &a){
		return priority < a.priority;
	}
	Operation(const char s,const int pr):symbol(s), priority(pr){}
	// Operation(const char s, Operand (*exfunc)(...)){}
};
class Unaru : public Operation{
private:
	const Operand (*ex)(const Operand&);
public:
	Unaru(const char s,const int pr,const Operand (*execute)(const Operand &a))
		:Operation(s,pr), ex(execute){}
} umin('!', 3, execute_umin),
	upl('#', 3 ,execute_upl);
class Binary : public Operation{
private:
	const Operand (*ex)(const Operand&, const Operand&);
public:
	const Operand exec(const Operand &a, const Operand &b){
		return ex(a,b);
	}
	Binary(const char s,const int pr,const Operand (*execute)(const Operand&, const Operand&)):
		Operation(s,pr), ex(execute){}
}	sum('+', 1, execute_sum), sub('-', 1, execute_sub),
	div_op('/', 2, execute_div), mul('*', 2, execute_mul);

class Special : public Operation{
public:
	Special(const char s,const int pr):
		Operation(s,pr){}
}	none('n',-1), op_br('(', 0),
	cl_br(')', 0);