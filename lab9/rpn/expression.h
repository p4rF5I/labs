#pragma once 
#include "stack.h"
#include "item.h"
#include <string>
#include <iostream>
using namespace std;
class Expression{
private:
	bool isDigit(char a) const{
		if(a >= 0x30 && a <= 0x39){
			return true;
		}
		return false;
	}
	Operation* getOperation(const char a){
		if(a == '+'){
			return &sum;
		}
		if(a == '-'){
			return &sub;
		}
		if(a == '*'){
			return &mul;
		}
		if(a == '/'){
			return &div_op;
		}
	}
	void pushOperation(const char a, Stack<Operation> &st){
		st.push(*getOperation(a));		
	}
	size_t pushOperand(const string str, size_t pos){
		// cout << "pos: " << pos << '\n';
		size_t end = str.find_first_not_of("1234567890",pos);
		// cout << str[pos] << str[end - 1];
		// cout << stoi(str.substr(pos, pos - end)) << '\n';
		expr.push(Operand(stoi(str.substr(pos, pos - end))));
		return end;
	}
	bool isUnaru(const string str,const size_t pos) const{
		return str[pos] == '-' || str[pos] == '+' 
		&& (pos == 0 || (isOperation(str[pos-1]) && str[pos-1] != ')'));
	}
	bool isOperation(char a) const{
		return a == '+' || a == '-' ||
			a == '/' || a == '*' ||
			a == '(' || a == ')';
	}
	Stack<Item> expr;
public:
	void print(){
		Stack<Item> temp;
		while(!expr.isEmpty()){
			temp.push(expr.pop());
		}
		Item tempItem;
		Item *tempP; 
		while(!temp.isEmpty()){
			tempItem = temp.pop();
			cout << sizeof(tempItem) << '\n';
			// tempItem.print();
			tempP = dynamic_cast<Operand*>(&tempItem);
			try{
				tempP -> print();
			}
			catch(...){
				cout << "here\n";
				tempP = dynamic_cast<Operation*>(&tempItem);
				tempP -> print();
			}
			expr.push(tempItem);
		}
	}
	Operand exec(){
		// while z
	}
	Expression(const string str){
		cout << "str: " << str;
		Stack<Operation> op;
		Operation *corOp;
		Operation tempOperation;
		for (int i = 0; i <= str.size(); ++i){
			if(isDigit(str[i])){
				i = pushOperand(str, i) - 1;
				continue;
			}
			if(isOperation(str[i])){
				if(isUnaru(str, i)){
					if(str[i] == '-'){
						op.push(umin);
					}
					if(str[i] == '+'){
						op.push(upl);
					}
					continue;
				}
				if(!op.isEmpty()){
					corOp = getOperation(str[i]);
					if(corOp == 0){
						if(str[i] == '('){
							pushOperation(str[i],op);
							continue;
						}
						if (str[i] == ')'){
							tempOperation = op.pop();
							while (tempOperation != cl_br){
								expr.push(tempOperation);
								tempOperation = op.pop();
							}
							continue;
						}
					}else {
						tempOperation = op.pop();
						if (tempOperation >= *corOp){
							expr.push(tempOperation);
							pushOperation(str[i],op);
						}	else {
							op.push(tempOperation);
							pushOperation(str[i],op);
						}
					}
				}else {
					pushOperation(str[i],op);
				}
			}
		}
		while(!op.isEmpty()){
			expr.push(op.pop());
		}
	}
};