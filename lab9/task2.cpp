/*author: soroka
task: Пусть дана без ошибок формула, имеющая следующий
синтаксис:
<формула>::=<цифра> | max(<формула>, < формула >) | min (<формула>,< формула >)
<цифра>::=0 1 2 3 4 5 6 7 8 9
Например, 8 или max(4, min(3, 5)) или min(min(3, 5), max(2, max(3, 4))).
Вычислить значение формулы, используя структуру данных стек.
tested: x29; 9; x2n9x37;x1x2x3x4x5x6x7x8x91 ; m9m8m7m6m5n49 ...

*/
#include <iostream>
#include <cstring>
#include "stack.h"
using namespace std;
char max(char a, char b);
char min(char a, char b);
bool isDigit(char a);
char calc(const char * str);
int main(){
	char str[1024];
	cout << "Enter expression: ";
	cin >> str;
	cout << calc(str) << '\n';
	system("pause");
	return 0;
}
char calc(const char * str){
	Stack<char> operand;
	int i = strlen(str);
	while(i >= 0){
		if(isDigit(str[i])){
			operand.push(str[i]);
			--i;
			continue;
		}
		if(str[i] == 'x'){
			operand.push(max(operand.pop(),operand.pop()));
			--i;
			continue;
		}
		if(str[i] == 'n'){
			operand.push(min(operand.pop(),operand.pop()));
			--i;
			continue;
		}
		--i;
	}
	return operand.pop();
}
char max(char a, char b){
	return (a>b)?a:b;
}
char min(char a, char b){
	return (a<b)?a:b;
}
bool isDigit(char a){
	if(a >= 0x30 && a <= 0x39){
		return true;
	}
	return false;
}