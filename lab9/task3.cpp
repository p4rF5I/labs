/*
author: soroka
task: rpn
tested: 1+2
1+2+3
1+2+3/3
(1+2+3)/3
1+(2+3)/3
1+2*3
(1+2)*3
(1-2)*3
3+4*(2-1)
3+4*2/(1-5)*2
1
-1+1
-1-1
*/
#include <iostream>
#include <cstring>
#include "stack.h"
using namespace std;
bool isDigit(char a);
bool isOperation(int a);
bool isOperation(char a);
int calc(Stack<int> &a);
int operationPriority(char a);
int executeOperation (char op, int a, int b);
int executeOperation (char op, int a);
void printPolishNotation(Stack<int> &a);
int main(int argc, char *argv[]){
	Stack<int> operand;
	Stack<char> operation;
	char buff[1024];
	if(argc != 2){
		cout << "Enter expression(without ' '): ";
		cin >> buff;
	} else {
		strcpy(buff,argv[1]);
	}
	int j = 0;
	char tempOperation;
	char number[12];	// max int	+ \0 && -
	for (int i = 0; buff[i]; ++i){
		// cout << "iteration: " << i << '\n';
		if(isDigit(buff[i])){
			j = i;
			while (buff[j] && isDigit(buff[j])){
				// cout << "HEREEEEE\n";
				// cout << "isD: " << (int) buff[j] << '\n';
				number[j - i] = buff[j];
				++j;
			}
			number[j - i] = '\0';
			operand.push(atoi(number));
			// continue;
			i = j -1;
			// cout << "number: " << number << '\n';
		}
		if(isOperation(buff[i])){
			if(buff[i] == '-' && (i == 0 || (isOperation(buff[i-1]) && buff[i-1] != ')'))){
				// cout << "minus operation!\n";
				operation.push('$');
				continue;
			}
			if(!operation.isEmpty()){
				if(operationPriority(buff[i]) == 0){
					if(buff[i] == '('){
						operation.push(buff[i]);
						continue;
					}
					if (buff[i] == ')'){
						tempOperation = operation.pop();
						while (tempOperation != '('){
							operand.push(tempOperation);
							tempOperation = operation.pop();
						}
						continue;
					}
				}else {
					tempOperation = operation.pop();
					//cout << "tempOperation: " << tempOperation << '\n';
					if (operationPriority(tempOperation) >= operationPriority(buff[i])){
						operand.push(tempOperation);
						//cout << "operand.push1: " << tempOperation << '\n';
						operation.push(buff[i]);
						//cout << "operatoin.push1: " << buff[i] << '\n';
					}	else {
						operation.push(tempOperation);
						//cout << "operation.push2: " << tempOperation << '\n';
						operation.push(buff[i]);
						//cout << "operation.push2: " << buff[i] << '\n';
					}
				}
			}else {
				//cout << "push: " << buff[i] << '\n';
				operation.push(buff[i]);
			}
		}
		//cout << "end: " << i << '\n';
	}
	//cout << "cycle out!\n";
	while(!operation.isEmpty()){
		operand.push(operation.pop());
	}
	//cout << "***************************\n";
	printPolishNotation(operand);
	cout << calc(operand) << '\n';
	system("pause");
	return 0;
}
bool isDigit(char a){
	if(a >= 0x30 && a <= 0x39){
		return true;
	}
	return false;
}
bool isOperation(char a){
	return a == '+' || a == '-' ||
			a == '/' || a == '*' ||
			a == '(' || a == ')' ||
			a == '$';
}
bool isOperation(int a){
	if(a  <= 128 && a >= -127){
		return  a == '+' || a == '-' ||
				a == '/' || a == '*' ||
				a == '(' || a == ')'|| 
				a == '$';
	}else {
		return false;
	}
}
int operationPriority(char a){
	switch (a){
		case '+':
		case '-':
			return 1;
		case '/':
		case '*':
			return 2;
		case '(':
		case ')':
			return 0;
		case '$':
			return 3;
	}
}
void printPolishNotation(Stack<int> &a){
	Stack<int> temp;
	while(!a.isEmpty()){
		temp.push(a.pop());
	}
	int tempVar;
	while (!temp.isEmpty()){	
		tempVar  = temp.pop();
		if (tempVar  <= 128 && tempVar >= -127 && isOperation(tempVar)){
			cout << (char) tempVar;
		} else {
			cout << tempVar;			
		}
		a.push(tempVar);
	}
	cout << '\n';
}
int calc(Stack<int> &a){
	// reverse 
	Stack<int> temp;
	while(!a.isEmpty()){
		temp.push(a.pop());
	}
	int tempVar;
	while(!temp.isEmpty()){
		tempVar = temp.pop();
		if(isOperation(tempVar)){
			if(tempVar == '$'){
				// cout << "minus\n";
				a.push(executeOperation(tempVar,a.pop()));
				continue;
			}
			// cout << "binop: \n";
			a.push(executeOperation(tempVar,a.pop(),a.pop()));
		} else {
			// cout << "a.push: " << tempVar << '\n';
			a.push(tempVar);
		}
	}
	return a.pop();
}
int executeOperation (char op, int a, int b){
	if(op == '+') return a+b;
	if(op == '-') return a-b;
	if(op == '*') return a*b;
	if(op == '/') return a/b;
}

int executeOperation (char op, int a){
	if(op == '$') return -a;
}