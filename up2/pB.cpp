/*
author: sorokaEA
condition: roots of quadratic equtiaon
test:
	printEquation-full tested(all cases)
	sovleQuadracticEquatio-full tested(all cases)
	solveLinearEquation-full tested(all cases)
*/
#include <iostream>
#include<math.h>
using namespace std;
void printMonomial(double q, int n,bool previous);
void sovleQuadracticEquation(double a, double b, double c);
void printEquation(double a, double b, double c);
void solveEquation(double a, double b, double c);
void solveLinearEquation(double a, double b, double c);
int main() {
	cout << "Enter the coefficients of the first parabola ax^2+bx+c=0(syntax: a b c)\n";
	double a, b, c;
	cin >> a >> b >> c;
	cout << "Enter the coefficients of the second parabola dx^2+ex+f=0(syntax: d e x)\n";
	double d, e, f;
	cin >> d >> e >> f;
	system("cls || clear");
	printEquation(a, b, c);
	cout << '\n';
	printEquation(d, e, f);
	cout << '\n';
	solveEquation(a-d,b-e,c-f);
	system("pause");
	return 0;
}
void sovleQuadracticEquation(double a, double b, double c) {
	double d = (b*b - 4 * a * c);
	double r1, r2;
	if (d >= 0) {
		d = sqrt(d);
		r1 = (-b + d) / (2 * a); r2 = (-b - d) / (2 * a);
		if (r1 == r2) { 
			cout << "Сommon point is (" << (double) r1 << ',' << a*r1*r1+b*r1+c << ")\n";
		} else{
			cout << "Сommon point are (" << (double) r1 << ',' << a*r1*r1+b*r1+c << ") , (" << r2 << ',' << a*r2*r2+b*r2+c <<")\n"  ;
		}
	}else {
		cout << "No common points!\n";
	}
}
void solveLinearEquation(double a, double b, double c) {
	 if (b) {
	 	double q=(-c/b);
		 cout << "Сommon point is (" << (double) -c / b << ',' << a*q*q+b*q+c << ")\n";
		 cout << '\n';
	 }
	 else {
		 if (!c) { cout << "They are identical!\n"; }
		 else { cout << "No common points!\n";}
	 }
}
void printEquation(double a, double b, double c) {
	if (a == 0) {
		if (b==0) {
			if (c == 0) {
				cout << '0';
			}else{
				cout << c;
			}
		}else {
			printMonomial(b, 1, 1);
			printMonomial(c, 0, 0);
		}
	}else {
		printMonomial(a, 2, 1);
		printMonomial(b, 1, 0);
		printMonomial(c, 0, 0);
	}
	cout << "=0";
}
void printMonomial(double q, int n,bool isFirstMonomial) {
	unsigned char str [4];
	switch (n){
	case 0:
		str[0] = '\0';
		break;
	case 1:
		str[0] = 'x';
		str[1] = '\0';
		break;
	case 2:
		str[0] = 'x';
		str[1] = '^';
		str[2] = '2';
		str[3] = '\0';
		break;
	}
	if (q == 0) return;
	if (isFirstMonomial) {
		if (q == 1 && str[0] != '\0') {
			cout << str; return;
		}
		if (q == -1 && str[0] != '\0') {
			cout << '-' << str; 
			return; 
		}
		cout << q << str;
	}else {
		if (q > 0) {
			if (q == 1 && str[0] != '\0') {
				cout <<'+'<< str; 
				return;
			}
			cout <<'+'<< q << str;
		}
		else {
			if (q == -1 && str[0] != '\0') {
				cout << '-' << str;
				return;
			}
			cout << q << str;
		}
	}
}
void solveEquation(double a, double b, double c) {
	if (a == 0 ) {
		solveLinearEquation(a, b, c);
    } else{
		sovleQuadracticEquation(a, b, c);
	}
}