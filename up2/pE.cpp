/*
author:soroka
task: name numbers from 100-999
tested: all cases
*/
#include <iostream>
using namespace std;
bool isCorrect(char *a);
bool isCorrect(int num);
void strcpy(char *to ,const char *from);	//there is no stdio.h in linux
class Number{
	int num;
public:
	char digitName[3][30];
	Number(int n);
	friend ostream& operator<<(ostream& os, const Number& n);  
};

int main(int argc,char **argv){
	setlocale(0," ");
	int number=0;
	if (argc==2 && isCorrect(argv[1])){ number=atoi(argv[1]);
	} else{
		while (!isCorrect(number)){
			cout << "Enter correct number(99<n<1000): ";
			cin >> number;
			system("cls || clear");
		}
	}
	Number n=Number(number);
	cout <<  n << '\n';
	/*for (int i=100;i<=999;++i){
		n=Number(i);
	cout <<  n << '\n';
	}*/
	system("pause");
	return 0;
}
bool isCorrect(char *a){
	int i=0;																				
	while (a[i]) {																																						
		if (((int)(a[i]) < 48) || ((int)a[i] > 57)) return false;								
		i++;
	}
	if (isCorrect(atoi(a))) return true;
	return false;
}
bool isCorrect(int num){
	if (num>=100 && num<=999){return true;}
	return false;
}
ostream& operator<<(ostream& os, const Number& num)  
{   
	os << num.digitName[2]  << ' ' << num.digitName[1] << ' ' << num.digitName[0];
    return os;  
}  
Number::Number(int n){  
    num=n; 
    strcpy(digitName[0],"\0"); strcpy(digitName[1],"\0"); strcpy(digitName[2],"\0");
    switch (num/100){
        case 1:	strcpy(digitName[2],"сто"); break;
        case 2:	strcpy(digitName[2],"двести"); break;
        case 3:	strcpy(digitName[2],"триста"); break;
        case 4:	strcpy(digitName[2],"четыреста"); break;
        case 5:	strcpy(digitName[2],"пятьсот"); break;
        case 6:	strcpy(digitName[2],"шестьсот"); break;
        case 7:	strcpy(digitName[2],"семьсот"); break;
        case 8:	strcpy(digitName[2],"восмьсот"); break;
        case 9:	strcpy(digitName[2],"девятьсот"); break;
    }
   	switch (((num%100)-num%10)/10){
   		case 2: strcpy(digitName[1],"двадцать"); break;
		case 3: strcpy(digitName[1],"тридцать"); break;
		case 4: strcpy(digitName[1],"сорок"); break;
		case 5: strcpy(digitName[1],"пятьдесят"); break;
		case 6: strcpy(digitName[1],"шестьдесят"); break;
		case 7: strcpy(digitName[1],"семьдесят"); break;
		case 8: strcpy(digitName[1],"восемьдесят"); break;
		case 9: strcpy(digitName[1],"девяносто"); break;
   	}
    switch (num%10){
		case 1: strcpy(digitName[0],"один"); break;
		case 2: strcpy(digitName[0],"два"); break;
		case 3: strcpy(digitName[0],"три"); break;
		case 4: strcpy(digitName[0],"четыре"); break;
		case 5: strcpy(digitName[0],"пять"); break;
		case 6: strcpy(digitName[0],"шесть"); break;
		case 7: strcpy(digitName[0],"семь"); break;
		case 8: strcpy(digitName[0],"восемь"); break;
		case 9: strcpy(digitName[0],"девять"); break;
    }
    if ((((num%100)-num%10)/10)==1){
    	switch (num%10){
            case 0: strcpy(digitName[0],"десять"); break;
            case 1: strcpy(digitName[0],"одиннадцать"); break;
            case 2: strcpy(digitName[0],"двенадцать"); break;
            case 3: strcpy(digitName[0],"тринадцать"); break;
            case 4: strcpy(digitName[0],"четырнадцать"); break;
            case 5: strcpy(digitName[0],"пятнадцать"); break;
            case 6: strcpy(digitName[0],"шестнадцать"); break;
            case 7: strcpy(digitName[0],"семнадцать"); break;
            case 8: strcpy(digitName[0],"восмнадцать"); break;
            case 9: strcpy(digitName[0],"девятнадцать"); break;
    	}
	}
}
void strcpy(char *to ,const char *from){
	int i = 0;
	while(from[i]){
		to[i]=from[i];
		i++;
	}
	to[i]='\0';
}