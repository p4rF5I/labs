/*
author: soroka
task: qrarter of point -?
tested: (1,1) (-1,1) (-1,-1) (1,-1)
		(0,1) (0,-1) (-1,0)	  (-1,0)
		+ some double value
		and so on
*/
/*
getQrFlags use flag system 
		*
		*			
 0010  0011	 0001
		*
-0110--1111--1001---
		*
 0100  1100  1000
		*
		*/
#include <iostream>
using namespace std;
void t(char **f);
bool isNumber(char **a,int c);
char getQrFlags(double x,double y);	 //use 4-bit flug return
int main(int argc,char **argv){
	double x,y;
	if ((argc == 3) && (isNumber(argv,1)) && (isNumber(argv,2))) {
		x= atof(argv[1]);
		y= atof(argv[2]);
	} else{
	cout << "Enter coordinates of your point(x y): ";
	cin >> x >> y;
	}
	cout << "Your point A("<< x << ',' << y << ") belongs to ";
	char qrFlags= getQrFlags(x,y);
	for (int i=1,c= 1;i<=8;i*=2,++c){
		if (qrFlags & i) cout << c << ", ";
	}
	cout << "\b\b quarter(s).\n";
	system ("pause");
	return 0;
}
char getQrFlags(double x,double y){		//use 4-bit flug return
	char qrFlags=0;
	if (x>=0 && y>=0){ qrFlags=qrFlags|1;}		// I
	if (x<=0 && y>=0){ qrFlags=qrFlags|2;}		// II
	if (x<=0 && y<=0){ qrFlags=qrFlags|4;}		// III
	if (x>=0 && y<=0){ qrFlags=qrFlags|8;}		// IV	b
	return qrFlags;
}
bool isNumber(char **a,int c){
	if (a[c][0]!='-'&& a[c][0]!='+' && ((int)(a[c][0]) < 48) || ((int)a[c][0] > 57)) return false;	//is  0-symbol corret(+||- are correct cases)
	int i = 1;																						// i think that input like (+ -) correct 
	bool dotFl=false;																				//so that case not considered there	
	while (a[c][i]) {																				//input like (+ -) converted by atof in (0 0)
		if (a[c][i]=='.'){
			if (dotFl) return false;
			dotFl=true;
		}																			
		if ((a[c][i]!='.') && (((int)(a[c][i]) < 48) || ((int)a[c][i] > 57))) return false;								
		i++;
	}
	return true;
}