/*
author: sorokaEA
condition: diferent dig in number
test: 1233331,0,,asd,12121,1234569
*/
#include <iostream>
using namespace std;
const unsigned int SIZE=256;
bool isDigitInNumber(int digit, unsigned char *a);
bool isDigitInNumber(int digit,int a);
int  digitsInNumber(int number);
int main() {
	int number;
	cout << "Enter number : ";
	if (!(cin >> number)) {cout << "Input error!\n"; system("pause"); return 1;}
	// without arrays
	int ans = 0;
	for (int i=0; i  <= 9; ++i) {
		if (isDigitInNumber(i, number)) ans++;
	}
	cout << "There is " << ans << " different digits in " << number << '\n';
	// with arrays
	int digits = digitsInNumber(number);
	unsigned char *charNumber = new unsigned char[digits+1];
	for (int i = 0,t=1;i < digits;++i, t *= 10){
		charNumber[i]=(char) (((number-number%t)/t)%10)+48;
	}
	charNumber[digits]='\0';
	ans = 0;
	for (int i = 0; i  <= 9; ++i) {
		if (isDigitInNumber(i, charNumber)) ans++;
	}
	cout << "There is " << ans << " different digits in " << number << '\n';
	delete[] charNumber;
	system("pause");
	return 0;
}
bool isDigitInNumber(int digit,unsigned char *a) {
	int i = 0;
	while (a[i]) {
		// cout << a[i];
		if (a[i] == digit + 48) return true;
		i++;
	}
	return false;
}
bool isDigitInNumber(int digit,int number) {
	int i = 0;
	int digits=digitsInNumber(number);
	for (int i=1,t=1;i<=digits;++i,t*=10){
		// cout << number/t%10 << '\n';
		if (digit==(number/t)%10) return true;
	}
	return false;
}
int  digitsInNumber(int number){
	static int last=33;
	static int digits=2;
	if (number==last){
		return digits;
	} else {
		last = number;
		for (digits=1;number>=10;number/=10, ++digits);
		return digits;
	}
}