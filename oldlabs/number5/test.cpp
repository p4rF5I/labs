#define FROM_TO_OPERATION ".."
#define AND_OPERATION ","
#define CALCULATE_PARAMETER "-n"	//	0
#define SET_LANGUAGE_PARAMETER "-l"	//	1
#define GET_HELP_PARAMETER "-h"			//	2
#define AMOUNT_OF_PARAMETERS 3
#define MAX(a,b) ((a>b) ? a : b)
#define MIN(a,b) ((a<b) ? a : b)
//***************************************
#define MAX_COLOR_WORD_SIZE 20	//example "Hi"-2+'/0'=3 
#define MAX_ANIMAL_WORD_SIZE 20
#define MAX_MESSAGE_SENTENSE_SIZE 200
#define DEFAULT_LANGUAGE "EN\0"
#include <iostream>
using namespace std;
char language[4] = DEFAULT_LANGUAGE;
char supportedLanguages[][3]={
	"RU","EN","GE"
};
const int numberOfSuppportedLaguages = sizeof(supportedLanguages)/3;	//3-is max size of slanguage name
char messegesRU[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Введите год(>0): ",
		" - год "
	};
char messegesEN[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Enter correct year: ",
		" - year of "
	};
char messegesGE[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Jahr eingeben(>0): ",
		" - Jahr des "
	};
char colorsRU[5][3][MAX_COLOR_WORD_SIZE]={		//one russian sybol is 2-byte? what?(8*2+1) It's UNICODE?
		{"зеленой",	"зеленого",	"NONE!"},
		{"красной",	"красного",	"NONE!"},
		{"желтой",	"жёлтого" ,	"NONE!"},
		{"белой",	"белого"  ,	"NONE!"},
		{"чёрной",	"чёрного" ,	"NONE!"}
	};
char colorsEN[5][3][MAX_COLOR_WORD_SIZE]={
		{"green",	"NONE!",	"NONE!"},
		{"red",		"NONE!",	"NONE!"},
		{"yellow",	"NONE!",	"NONE!"},
		{"white",	"NONE!",	"NONE!"},
		{"black",	"NONE!",	"NONE!"}
	};
char colorsGE[5][3][MAX_COLOR_WORD_SIZE]={
		{"grüne",	"grüner",	"grünes"},
		{"rote",	"roter",	"rotes"},
		{"gelbe",	"gelber",	"gelbes"},
		{"weiße",	"weißer",	"weißes"},
		{"schwarze","schwarzer","schwarzes"}
	};
char animalsRU[12][MAX_ANIMAL_WORD_SIZE]{
		"крысы",
		"быка",
		"тигра",
		"кролика",
		"дракона",
		"змеи",
		"лоошади",
		"овцы",
		"обезьяны",
		"курицы",
		"собаки",
		"свиньи"
 	};
char animalsEN[12][MAX_ANIMAL_WORD_SIZE]{
		"rat",
		"bull",
		"tiger",
		"rabbit",
		"dragon",
		"snake",
		"horse",
		"sheep",
		"monkey",
		"chicken",
		"dog",
		"pig"
	};
char animalsGE[12][MAX_ANIMAL_WORD_SIZE]{
		"Ratte",
		"Stier",
		"Tiger",
		"Hase",
		"Drachen",
		"Schlange",
		"Pferd",
		"Schaf",
		"Affe",
		"Hähnchen",
		"Hund",
		"Schwein"
	};
char (*animals)[MAX_ANIMAL_WORD_SIZE];
char (*colors)[3][MAX_COLOR_WORD_SIZE];
char (*messeges)[MAX_MESSAGE_SENTENSE_SIZE];
int	getFormOfWord(char *language, int animalIndex);
void setLanguageEnterface(const char * language);
bool isLangueageSupported(char *language);
void displayNameOfYearInEasternCalendar(char *language,int year);
void printSupportedLanguages();
//***************************************
int startIndex[AMOUNT_OF_PARAMETERS];
int endIndex[AMOUNT_OF_PARAMETERS];
int lastOperandValue;
char supportedParameters[AMOUNT_OF_PARAMETERS][3]={
	CALCULATE_PARAMETER, SET_LANGUAGE_PARAMETER,GET_HELP_PARAMETER
};
int getParameterIndex(const char *parameter);
void printStartEndIndexes();
bool strcmp(const char *a,const char *b);
void getHelpNote();
void fillAllIndexesWithZero();
void execute(int argc,char **argv);
void executeOperationAnd(int a,int b);
void executeOperationFromTo(int a,int b);
bool setParametertIndex(int argc,int index,char *parameter);
bool setStartEndIndexes(int argc,char **argv);	//return 0 if there is more than 2 same parametes here
										//set 0 if element wasn't found
bool isParameter(const char* parameter);	
bool isOperation(const char* parameter);
bool isGetHelpSyntaxCorrect(int argc);
bool isCalculateSyntaxCorrect(char **argv);
bool isSetLanguageSyntaxCorrect(int argc, char **argv);
bool checkAllParametersSytax(int argc,char **argv);
bool isOperand(char *a);
bool isOperand(int num);
using namespace std;
int main(int argc,char **argv){
	setlocale(0," ");
	if (argc==1){
		int year=0;
		setLanguageEnterface(language);
		while (!isOperand(year)){
			system("cls || clear");
			cout << messeges[0];
			cin >> year;
		}
		displayNameOfYearInEasternCalendar(language,year);
		system ("pause");
		return 1;
	}
	if (setStartEndIndexes(argc,argv)){
		if(checkAllParametersSytax(argc,argv)){
			execute(argc,argv);
		} else {
		cout << "[!]Argument syntax error\n";
		cout << "[!]Use \"" << argv[0] << ' ' << GET_HELP_PARAMETER << "\" to get help note!\n"; 
		}
	} else {
		cout << "[!]Syntax error\n";
		cout << "[!]Use \"" << argv[0] << ' ' << GET_HELP_PARAMETER << "\" to get help note!\n";
	}
	return 0;
}
bool isParameter(const char* parameter){
	if (getParameterIndex(parameter)==-1) return false;
	return true;
}
bool isOperation(const char* parameter){
	if (strcmp(AND_OPERATION,parameter)){
		return true;
	}
	if (strcmp(FROM_TO_OPERATION,parameter)){
		return true;
	}
	return false;	//[ERROR]It's isn't operation or not all operations are declared here
}
bool strcmp(const char *a,const char *b){
	int i=0;
	while(a[i] && b[i]){
		if (a[i]!=b[i]) return false;
		++i;
	}
	if (a[i]==b[i]) return true;
	return false;
}
bool setStartEndIndexes(int argc,char **argv){
	fillAllIndexesWithZero();
	if (argc==1){return false;}					//[ERROR_CHECK]No seted parametes case
	if (!isParameter(argv[1])){return false;}	//[ERROR_CHECK]first should be parameter
	for (int i=1; i < argc; i++){
		if (isParameter(argv[i])){
			if (!setParametertIndex(argc,i,argv[i])) {return false;}	//[ERROR_CHECK] 2 or more same parameters 
		}
	}
	return true;
}
void executeOperationAnd(int b){
	displayNameOfYearInEasternCalendar(language,b);
	lastOperandValue=b;
}
void executeOperationFromTo(int a,int b){
	int max=MAX(a,b);
	int i=MIN(a,b);
	if (i==lastOperandValue){++i;}
	for (;i<=max;++i){
		displayNameOfYearInEasternCalendar(language,i);
	}
	lastOperandValue=b;
}
void getHelpNote(){
	cout << "*******************WARNING*******************\n";
	cout << "[!!!]It's not the final version of help note!\n";
	cout << "*******************WARNING*******************\n";
	void printSupportedLanguages();
	cout << "[h] Use " << CALCULATE_PARAMETER << ' ' << FROM_TO_OPERATION << " num>0 " <<
	AND_OPERATION << " num>0 " << AND_OPERATION << " num>0\n";
	cout << "[h] " << AND_OPERATION << " - and operatin(print a and b)\n";
	cout << "[h] "<< FROM_TO_OPERATION << " - from-to operatin(print from a to b)\n";
	cout << "[h] Operations in " << CALCULATE_PARAMETER << " can combined to get the necessary result\n";
	cout << "[h] Example: -n .. 10 , 20 , 30 .. 12\n";
	cout << "[h] Use: " << SET_LANGUAGE_PARAMETER << " to set preferred language\n";
}
int getParameterIndex(const char * parameter){
	if (strcmp(CALCULATE_PARAMETER,parameter)){
		return false;
	}
	if (strcmp(SET_LANGUAGE_PARAMETER,parameter)){
		return true;
	}
	if (strcmp(GET_HELP_PARAMETER,parameter)){
		return 2;
	}
	return -1;	//[ERROR]It's isn't parameter or not all parameters are declared here
}
bool setParametertIndex(int argc, int index,char *parameter){
	static int previousCommand;
	for (int i=0; i < AMOUNT_OF_PARAMETERS; ++i){		//find command number "i"
		if(strcmp(parameter,supportedParameters[i])){	//is it right command number?
			if(!startIndex[i]) { // is parameter has already initialized?
				startIndex[i]=index;
				endIndex[i]=argc-1;
				if (i != previousCommand){	// performed for all except first, because they differ from each other
					endIndex[previousCommand]=index-1;	// if its isn't last parameter, then set correct end 
				}
				previousCommand = i;
				return true;
			}
			return false;	//[ERROR]parameter's start index has already seted//that's mean,that we have >=2 same parameters
		}
	}
}
void printStartEndIndexes(){
		for (int i=0;i<AMOUNT_OF_PARAMETERS;++i){
		cout << i << '\t' << startIndex[i]	<< '\t' <<endIndex[i] << '\n';
	}
}
bool isGetHelpSyntaxCorrect(int argc){
	// Get help parameter doesn't requare any arguments
	int helpIndex = getParameterIndex(GET_HELP_PARAMETER);
	if (argc!=2){
		//if we have others parameters or GET_HELP_PARAMETER have any arguments
		return false;
	}
	return true; //[OK] User want's help!
}
bool isSetLanguageSyntaxCorrect(int argc, char **argv){
	int setLanguageIndex = getParameterIndex(SET_LANGUAGE_PARAMETER);
	// Language parameter requre just one argument
	if((endIndex[setLanguageIndex] - startIndex[setLanguageIndex])==1){
		if(isLangueageSupported(argv [endIndex[setLanguageIndex]])){
			return true;	//[OK] User want's change lanuage (language syntax correct and language supported) 
		} else {
			return false; //[ERROR]Language isn's supported
		}
	} else {
		return false;//[ERROR] No agruments or more than one argument
	}
}
bool isCalculateSyntaxCorrect(char **argv){
	int calculateIndex = getParameterIndex(CALCULATE_PARAMETER);
	//[ERROR_CHECK]  parameter must have at least one argument
	if (startIndex[calculateIndex] == endIndex[calculateIndex]){ 
	return false;
	}	
	//[ERROR_CHECK] if the argument is one, then it must be an operand
	if (endIndex[calculateIndex]- startIndex[calculateIndex] == 1){ 
		if (isOperation(argv[endIndex[calculateIndex]])) {
			return false; //[ERROR] not operand
		} else {
			if (isOperand (argv[endIndex[calculateIndex]])) {
				return true;	//case with one argument is considered
			} else {
				return false;	//incorrect argument
			} 
		}
	}
	if (isOperation(argv[endIndex[calculateIndex]])) {return false;}	//[ERROR_CHECK] the last argument must be an operand
	if (strcmp(argv[startIndex[calculateIndex]+1],AND_OPERATION)) {return false;} 	//[ERROR_CHECK] The first argument should not be "AND"
	int actualOperationIndex = startIndex[calculateIndex]+1;
	int actualOperandIndex = startIndex[calculateIndex]+2;
	if (!isOperation(argv[actualOperationIndex])){	// actual indexes correction
		++actualOperationIndex;
		--actualOperandIndex;
	}
	if (!isOperand(argv[endIndex[calculateIndex]])) {return false;}	//[ERROR_CHEK] last operand must be correct; It makes next construction more easer
	for (; actualOperationIndex < endIndex[calculateIndex]; actualOperationIndex+=2, actualOperandIndex+=2){
		if (isOperation(argv[actualOperationIndex]) && isOperand(argv[actualOperandIndex])){
			continue;
		} else {
			return false;
		}
	}
	return true;
}
bool checkAllParametersSytax(int argc,char **argv){
	if (startIndex[getParameterIndex(CALCULATE_PARAMETER)] && !isCalculateSyntaxCorrect(argv)) {return false;}
	if (startIndex[getParameterIndex(SET_LANGUAGE_PARAMETER)] && !isSetLanguageSyntaxCorrect(argc,argv)) {return false;}
	if (startIndex[getParameterIndex(GET_HELP_PARAMETER)] && !isGetHelpSyntaxCorrect(argc)) {return false;}
	return true;
}
void execute(int argc,char **argv){
	if (startIndex[getParameterIndex(GET_HELP_PARAMETER)]){
		getHelpNote();
		return;
	}
	if (startIndex[getParameterIndex(SET_LANGUAGE_PARAMETER)]){
		setLanguageEnterface(argv[startIndex[getParameterIndex(SET_LANGUAGE_PARAMETER)]+1]);
	}	else{
		setLanguageEnterface(DEFAULT_LANGUAGE);
	}
	if (startIndex[getParameterIndex(CALCULATE_PARAMETER)]){
	int commandIndex;
	if(isOperation(argv[startIndex[getParameterIndex(CALCULATE_PARAMETER)]+1])) {
		commandIndex = startIndex[getParameterIndex(CALCULATE_PARAMETER)]+1;
		executeOperationFromTo(0,atoi(argv[startIndex[getParameterIndex(CALCULATE_PARAMETER)]+2]));
	}else {
		commandIndex = startIndex[getParameterIndex(CALCULATE_PARAMETER)]+2;
	}
	executeOperationAnd(atoi(argv[commandIndex-1]));
	for (;commandIndex < endIndex[getParameterIndex(CALCULATE_PARAMETER)];commandIndex+=2){
		if(strcmp(argv[commandIndex],AND_OPERATION)){
			executeOperationAnd(atoi(argv[commandIndex+1]));
		}
		if(strcmp(argv[commandIndex],FROM_TO_OPERATION)){
			executeOperationFromTo(atoi(argv[commandIndex-1]),atoi(argv[commandIndex+1]));
		}
	}
	}
}
bool isOperand(char *a){
	int i = 0;																				
	while (a[i]) {																																						
		if (((int)(a[i]) < 48) || ((int)a[i] > 57)) return false;								
		i++;
	}
	if (isOperand(atoi(a))) return true;
	return false;
}
bool isOperand(int num){
	if ( num > 0 ){return true;}
	return false;
}
void fillAllIndexesWithZero(){
	for (int i=0;i<AMOUNT_OF_PARAMETERS;++i){
		startIndex[i] = 0;	endIndex[i] = 0;
	}	
}
//*********************************
void setLanguageEnterface(const char * language){
	//cout << language;
	if(strcmp(language,"RU")){
		animals = &animalsRU[0];
		colors = &colorsRU[0];
		messeges = &messegesRU[0];
	}
	if(strcmp(language,"EN")){
		animals = &animalsEN[0];
		colors = &colorsEN[0];
		messeges = &messegesEN[0];
	}
	if(strcmp(language,"GE")){
		animals = &animalsGE[0];
		colors = &colorsGE[0];
		messeges = &messegesGE[0];
	}
}
bool isLangueageSupported(char *language){
	int i=0;
	while(i<numberOfSuppportedLaguages){
		if(strcmp(supportedLanguages[i],language)) return true;
		i++;
	}
	return false;
}
int getFormOfWord(char *language, int animalIndex){
	if (strcmp(language,"RU")){
		switch (animalIndex){
			//male
			case 1:
			case 2:
			case 3:
			case 4:
				return true;
			//female
			case 6:
			case 7:
			case 9:
			case 11:
				return 3;
			default:
				return 2;
		}
	}
	if (strcmp(language,"EN")){
		return false;
	}
	if (strcmp(language,"GE")){
		switch (animalIndex){
			case 0:
			case 5:
				return false;
			case 6:
			case 7:
			case 9:
			case 11:
				return 2;
			default:
				return 1;
		}	
	}
}
void displayNameOfYearInEasternCalendar(char *language,int year){	
	unsigned short int animalIndex;
	unsigned short int colorIndex;
	colorIndex = (((year+56)%60)%10)/2; //very simple ideas prooved by bruteforce
	animalIndex = ((year+56)%60)%12;
	if (strcmp(language,"RU")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"EN")){
		cout << year << messeges[1] << colors[colorIndex][0]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"GE")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
}
void printSupportedLanguages(){
	cout << "[h]Suppornted langueges are: ";
	for (int i=0; i < numberOfSuppportedLaguages; ++i) cout << supportedLanguages[i] << ", ";
	cout << "\b\b.\n"; 
}
//*********************************