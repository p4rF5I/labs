#define MAX_COLOR_WORD_SIZE 20	//example "Hi"-2+'/0'=3 
#define MAX_ANIMAL_WORD_SIZE 20
#define MAX_MESSAGE_SENTENSE_SIZE 200
#define DEFAULT_LANGUAGE "EN\0"
#define DEBUG_LANGUAGE "77"		// [WARNING]Not more than 2 symbols!
extern char language[4] = DEFAULT_LANGUAGE;
extern char supportedLanguages[][3]={
	"RU","EN","GE",DEBUG_LANGUAGE			//[WARNING] DEBUG languege("77") should to be last of them!!!
};
const int numberOfSuppportedLaguages = sizeof(supportedLanguages)/3;	//3-is max size of slanguage name
extern char messegesRU[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Введите год(>0): ",
		" - год "
	};
extern char messegesEN[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Jahr eingeben(>0): ",
		" - year of "
	};
extern char messegesGE[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Enter correсt year(>0): ",
		" - Jahr des "
	};
extern char colorsRU[5][3][MAX_COLOR_WORD_SIZE]={		//one russian sybol is 2-byte? what?(8*2+1) It's UNICODE?
		{"зеленой",	"зеленого",	"NONE!"},
		{"красной",	"красного",	"NONE!"},
		{"желтой",	"жёлтого" ,	"NONE!"},
		{"белой",	"белого"  ,	"NONE!"},
		{"чёрной",	"чёрного" ,	"NONE!"}
	};
extern char colorsEN[5][3][MAX_COLOR_WORD_SIZE]={
		{"green",	"NONE!",	"NONE!"},
		{"red",		"NONE!",	"NONE!"},
		{"yellow",	"NONE!",	"NONE!"},
		{"white",	"NONE!",	"NONE!"},
		{"black",	"NONE!",	"NONE!"}
	};
extern char colorsGE[5][3][MAX_COLOR_WORD_SIZE]={
		{"grüne",	"grüner",	"grünes"},
		{"rote",	"roter",	"rotes"},
		{"gelbe",	"gelber",	"gelbes"},
		{"weiße",	"weißer",	"weißes"},
		{"schwarze","schwarzer","schwarzes"}
	};
extern char animalsRU[12][MAX_ANIMAL_WORD_SIZE]{
		"крысы",
		"быка",
		"тигра",
		"кролика",
		"дракона",
		"змеи",
		"лоошади",
		"овцы",
		"обезьяны",
		"курицы",
		"собаки",
		"свиньи"
 	};
extern char animalsEN[12][MAX_ANIMAL_WORD_SIZE]{
		"rat",
		"bull",
		"tiger",
		"rabbit",
		"dragon",
		"snake",
		"horse",
		"sheep",
		"monkey",
		"chicken",
		"dog",
		"pig"
	};
extern char animalsGE[12][MAX_ANIMAL_WORD_SIZE]{
		"Ratte",
		"Stier",
		"Tiger",
		"Hase",
		"Drachen",
		"Schlange",
		"Pferd",
		"Schaf",
		"Affe",
		"Hähnchen",
		"Hund",
		"Schwein"
	};
extern char (*animals)[MAX_ANIMAL_WORD_SIZE];
extern char (*colors)[3][MAX_COLOR_WORD_SIZE];
extern char (*messeges)[MAX_MESSAGE_SENTENSE_SIZE];
int	getFormOfWord(char *language, int animalIndex);
void setLanguageEnterface(const char * language);
bool isLangueageSupported(char *language);
bool strcmp(const char *a,const char *b);
void displayNameOfYearInEasternCalendar(char *language,int year);
void debugDispay(int );