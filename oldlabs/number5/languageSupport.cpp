#include <iostream>
#include "languageSupport.h"
using namespace std;
void setLanguageEnterface(const char * language){
	//cout << language;
	if(strcmp(language,"RU")){
		animals = &animalsRU[0];
		colors = &colorsRU[0];
		messeges = &messegesRU[0];
	}
	if(strcmp(language,"EN")){
		animals = &animalsEN[0];
		colors = &colorsEN[0];
		messeges = &messegesEN[0];
	}
	if(strcmp(language,"GE")){
		animals = &animalsGE[0];
		colors = &colorsGE[0];
		messeges = &messegesGE[0];
	}
	if(strcmp(language,DEBUG_LANGUAGE)){
		cout << "[!!!] DEBUG MODE\n";
	}
}
bool isLangueageSupported(char *language){
	int i=0;
	while(i<numberOfSuppportedLaguages){
		if(strcmp(supportedLanguages[i],language)) return true;
		i++;
	}
	return false;
}
int getFormOfWord(char *language, int animalIndex){
	if (strcmp(language,"RU")){
		switch (animalIndex){
			//male
			case 1:
			case 2:
			case 3:
			case 4:
				return true;
			//female
			case 6:
			case 7:
			case 9:
			case 11:
				return 3;
			default:
				return 2;
		}
	}
	if (strcmp(language,"EN")){
		return false;
	}
	if (strcmp(language,"GE")){
		switch (animalIndex){
			case 0:
			case 5:
				return false;
			case 6:
			case 7:
			case 9:
			case 11:
				return 2;
			default:
				return 1;
		}	
	}
}
void displayNameOfYearInEasternCalendar(char *language,int year){	
	unsigned short int animalIndex;
	unsigned short int colorIndex;
	colorIndex = (((year+56)%60)%10)/2; //very simple ideas prooved by bruteforce
	animalIndex = ((year+56)%60)%12;
	if (strcmp(language,DEBUG_LANGUAGE)){		//[WARNING] Debug language shoud be the first of them!
		debugDispay(year);
	}
	if (strcmp(language,"RU")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"EN")){
		cout << year << messeges[1] << colors[colorIndex][0]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"GE")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
}
void debugDispay(int year){
		int i=0;
		for (;i<numberOfSuppportedLaguages-1; ++i){
			cout << "[DEBUG] " << i << ":\t";
			setLanguageEnterface(supportedLanguages[i]);
			displayNameOfYearInEasternCalendar(supportedLanguages[i],year);
		}
		 i=-1;
}
bool strcmp(const char *a,const char *b){
	int i=0;
	while(a[i] && b[i]){
		if (a[i]!=b[i]) return false;
		++i;
	}
	if (a[i]==b[i]) return true;
	return false;
}