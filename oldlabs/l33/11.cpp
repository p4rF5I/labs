#include <iostream>
#define E 0.0000000001
using namespace std;
 
int gcd(int a, int b);//greatest common divisor
 
void fraction(int k){
    
    double eps = 1;
    for (int i = 2; i <= k + 1; ++i)
        eps /= (i + 0.);
    int n_min = k - 1;
    int d_min = k;
    double min = (k - 1.)/k;
    double previous = 0;
    while (previous < min - eps){
        for (int n = 1; n <= k - 1; ++n){
            for (int d = k; d > n; --d){
                if (previous + eps < n/(d + 0.) && n/(d + 0.) < min + eps){
                    if (gcd(n, d) == 1){
                        min = n/(d + 0.);
                        n_min = n;
                        d_min = d;
                    }
                }
            }
        }
        cout << n_min << "/" << d_min << " = " << n_min/(d_min + 0.) << endl;
        previous = min;
        min = (k - 1.)/k;
    }
}
 
int main(){
    
    int n;
    cout << "n = ";
    cin >> n;
    fraction(n);
    // cout << gcd(n,330);
        double eps = 1;
   /* for (int i = 2; i <= 10 + 1; ++i)
        eps /= (i);
    cout << eps  << '\n';*/
}
int gcd(int a, int b) {
  int t;
  while (b != 0) {
    t = b;
    b = a % b;
    a = t;
  }
    // cout << a << '\t' << b << '\n';
    // last nonzero remainder - our gcd
    //Euclidean algorithm will end when one of the numbers 0
  return a;
}