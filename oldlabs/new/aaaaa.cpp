    #include <iostream>
     
    void go() {
    	volatile int x = 42;
    	std::cout << x << std::endl;
    	x++;
    	if(x!=50){go();}
    }
     
    int main() {
    	go();
    	go();
    	go();
    }