#include <iostream>
#define FROM_TO_OPERATION ".."
#define AND_OPERATION ","
#define CALCULATE_PARAMETER "-n"	//	0
#define SET_LANGUAGE_PARAMETER "-l"	//	1
#define GET_HELP_PARAMETER "-h"			//	2
#define AMOUNT_OF_PARAMETERS 3
#define MAX(a,b) ((a>b) ? a : b)
#define MIN(a,b) ((a<b) ? a : b)
//***************************************
#define MAX_COLOR_WORD_SIZE 20	//example "Hi"-2+'/0'=3 
#define MAX_ANIMAL_WORD_SIZE 20
#define MAX_MESSAGE_SENTENSE_SIZE 200
#define DEFAULT_LANGUAGE "EN\0"
#define START_INDEX 0
#define END_INDEX 1
#define DEBUG_LANGUAGE "77"		// [WARNING]Not more than 2 symbols!
#include <iostream>
using namespace std;
char language[4] = DEFAULT_LANGUAGE;
char supportedLanguages[][3]={
	"RU","EN","GE",DEBUG_LANGUAGE			//[WARNING] DEBUG languege("77") should to be last of them!!!
};
const int numberOfSuppportedLaguages = sizeof(supportedLanguages)/3;	//3-is max size of slanguage name
char messegesRU[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Введите год(>0): ",
		" - год "
	};
char messegesEN[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Jahr eingeben(>0): ",
		" - year of "
	};
char messegesGE[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Enter correсt year(>0): ",
		" - Jahr des "
	};
char colorsRU[5][3][MAX_COLOR_WORD_SIZE]={		//one russian sybol is 2-byte? what?(8*2+1) It's UNICODE?
		{"зеленой",	"зеленого",	"NONE!"},
		{"красной",	"красного",	"NONE!"},
		{"желтой",	"жёлтого" ,	"NONE!"},
		{"белой",	"белого"  ,	"NONE!"},
		{"чёрной",	"чёрного" ,	"NONE!"}
	};
char colorsEN[5][3][MAX_COLOR_WORD_SIZE]={
		{"green",	"NONE!",	"NONE!"},
		{"red",		"NONE!",	"NONE!"},
		{"yellow",	"NONE!",	"NONE!"},
		{"white",	"NONE!",	"NONE!"},
		{"black",	"NONE!",	"NONE!"}
	};
char colorsGE[5][3][MAX_COLOR_WORD_SIZE]={
		{"grüne",	"grüner",	"grünes"},
		{"rote",	"roter",	"rotes"},
		{"gelbe",	"gelber",	"gelbes"},
		{"weiße",	"weißer",	"weißes"},
		{"schwarze","schwarzer","schwarzes"}
	};
char animalsRU[12][MAX_ANIMAL_WORD_SIZE]{
		"крысы",
		"быка",
		"тигра",
		"кролика",
		"дракона",
		"змеи",
		"лоошади",
		"овцы",
		"обезьяны",
		"курицы",
		"собаки",
		"свиньи"
 	};
char animalsEN[12][MAX_ANIMAL_WORD_SIZE]{
		"rat",
		"bull",
		"tiger",
		"rabbit",
		"dragon",
		"snake",
		"horse",
		"sheep",
		"monkey",
		"chicken",
		"dog",
		"pig"
	};
char animalsGE[12][MAX_ANIMAL_WORD_SIZE]{
		"Ratte",
		"Stier",
		"Tiger",
		"Hase",
		"Drachen",
		"Schlange",
		"Pferd",
		"Schaf",
		"Affe",
		"Hähnchen",
		"Hund",
		"Schwein"
	};
char (*animals)[MAX_ANIMAL_WORD_SIZE];
char (*colors)[3][MAX_COLOR_WORD_SIZE];
char (*messeges)[MAX_MESSAGE_SENTENSE_SIZE];
int	getFormOfWord(char *language, int animalIndex);
void setLanguageEnterface(const char * language);
bool isLangueageSupported(char *language);
void displayNameOfYearInEasternCalendar(char *language,int year);
void debugDispay(int );
void printStartEndIndexes();

//***************************************
int indexOfParameter[AMOUNT_OF_PARAMETERS][2];
int lastOperandValue;
char supportedParameters[AMOUNT_OF_PARAMETERS][3]={
	CALCULATE_PARAMETER, SET_LANGUAGE_PARAMETER,GET_HELP_PARAMETER
};
int getParameterIndex(const char *parameter);
bool strcmp(const char *a,const char *b);
void getHelpNote();
void fillAllIndexesWithZero();
void execute(int argc,char **argv);
void executeOperationAnd(int a,int b);
void executeOperationFromTo(int a,int b);
bool setParametertIndex(int argc,int index,char *parameter);
bool setStartEndIndexes(int argc,char **argv);	//return 0 if there is more than 2 same parametes here
										//set 0 if element wasn't found
bool isParameter(const char* parameter);	
bool isOperation(const char* parameter);
bool isGetHelpSyntaxCorrect(int argc);
bool isCalculateSyntaxCorrect(char **argv);
bool isSetLanguageSyntaxCorrect(int argc, char **argv);
bool checkAllParametersSytax(int argc,char **argv);
bool isOperand(char *a);
bool isOperand(int num);
using namespace std;
int main(int argc,char **argv){
	setlocale(0," ");
	if (setStartEndIndexes(argc,argv)){
		if(checkAllParametersSytax(argc,argv)){
			execute(argc,argv);
		} else {
		cout << "[!]Argument syntax error\n";
		cout << "[!]Use \"" << argv[0] << ' ' << GET_HELP_PARAMETER << "\" to get help note!\n"; 
		}
	} else {
		cout << "[!]Syntax error\n";
		cout << "[!]Use \"" << argv[0] << ' ' << GET_HELP_PARAMETER << "\" to get help note!\n";
	}
	return 0;
}
bool isParameter(const char* parameter){
	if (getParameterIndex(parameter)==-1) return false;
	return true;
}
bool isOperation(const char* parameter){
	if (strcmp(AND_OPERATION,parameter)){
		return true;
	}
	if (strcmp(FROM_TO_OPERATION,parameter)){
		return true;
	}
	return false;	//[ERROR]It's isn't operation or not all operations are declared here
}
bool strcmp(const char *a,const char *b){
	int i=0;
	while(a[i] && b[i]){
		if (a[i]!=b[i]) return false;
		++i;
	}
	if (a[i]==b[i]) return true;
	return false;
}
bool setStartEndIndexes(int argc,char **argv){
	fillAllIndexesWithZero();
	if (argc==1){return false;}					//[ERROR_CHECK]No seted parametes case
	if (!isParameter(argv[1])){return false;}	//[ERROR_CHECK]first should be parameter
	for (int i=1; i < argc; i++){
		if (isParameter(argv[i])){
			if (!setParametertIndex(argc,i,argv[i])) {return false;}	//[ERROR_CHECK] 2 or more same parameters 
		}
	}
	return true;
}
void executeOperationAnd(int b){
	displayNameOfYearInEasternCalendar(language,b);
	lastOperandValue=b;
}
void executeOperationFromTo(int a,int b){
	int max=MAX(a,b);
	int i=MIN(a,b);
	if (i==lastOperandValue){++i;}
	for (;i<=max;++i){
		displayNameOfYearInEasternCalendar(language,i);
	}
	lastOperandValue=b;
}
void getHelpNote(){
	cout << "\nSOME HELP NOTE!\n";
}
int getParameterIndex(const char * parameter){
	if (strcmp(CALCULATE_PARAMETER,parameter)){
		return false;
	}
	if (strcmp(SET_LANGUAGE_PARAMETER,parameter)){
		return true;
	}
	if (strcmp(GET_HELP_PARAMETER,parameter)){
		return 2;
	}
	return -1;	//[ERROR]It's isn't parameter or not all parameters are declared here
}
bool setParametertIndex(int argc, int index,char *parameter){
	static int previousCommand;
	for (int i=0; i < AMOUNT_OF_PARAMETERS; ++i){		//find command number "i"
		if(strcmp(parameter,supportedParameters[i])){	//is it right command number?
			if(!indexOfParameter[i][START_INDEX]) { // is parameter has already initialized?
				indexOfParameter[i][START_INDEX]=index;
				indexOfParameter[i][END_INDEX]=argc-1;
				if (i != previousCommand){	// performed for all except first, because they differ from each other
					indexOfParameter[previousCommand][END_INDEX]=index-1;	// if its isn't last parameter, then set correct end 
				}
				previousCommand = i;
				return true;
			}
			return false;	//[ERROR]parameter's start index has already seted//that's mean,that we have >=2 same parameters
		}
	}
}
void printStartEndIndexes(){
		for (int i=0;i<AMOUNT_OF_PARAMETERS;++i){
		cout << i << '\t' << indexOfParameter[i][START_INDEX]	<< '\t' <<indexOfParameter[i][END_INDEX] << '\n';
	}
}
//*********************************
void setLanguageEnterface(const char * language){
	//cout << language;
	if(strcmp(language,"RU")){
		animals = &animalsRU[0];
		colors = &colorsRU[0];
		messeges = &messegesRU[0];
	}
	if(strcmp(language,"EN")){
		animals = &animalsEN[0];
		colors = &colorsEN[0];
		messeges = &messegesEN[0];
	}
	if(strcmp(language,"GE")){
		animals = &animalsGE[0];
		colors = &colorsGE[0];
		messeges = &messegesGE[0];
	}
	if(strcmp(language,DEBUG_LANGUAGE)){
		cout << "[!!!] DEBUG MODE\n";
	}
}
bool isLangueageSupported(char *language){
	int i=0;
	while(i<numberOfSuppportedLaguages){
		if(strcmp(supportedLanguages[i],language)) return true;
		i++;
	}
	return false;
}
int getFormOfWord(char *language, int animalIndex){
	if (strcmp(language,"RU")){
		switch (animalIndex){
			//male
			case 1:
			case 2:
			case 3:
			case 4:
				return true;
			//female
			case 6:
			case 7:
			case 9:
			case 11:
				return 3;
			default:
				return 2;
		}
	}
	if (strcmp(language,"EN")){
		return false;
	}
	if (strcmp(language,"GE")){
		switch (animalIndex){
			case 0:
			case 5:
				return false;
			case 6:
			case 7:
			case 9:
			case 11:
				return 2;
			default:
				return 1;
		}	
	}
}
void fillAllIndexesWithZero(){
	for (int i=0;i<AMOUNT_OF_PARAMETERS;++i){
		indexOfParameter[i][START_INDEX]=0;	indexOfParameter[i][END_INDEX]=0;
	}	
}
void displayNameOfYearInEasternCalendar(char *language,int year){	
	unsigned short int animalIndex;
	unsigned short int colorIndex;
	colorIndex = (((year+56)%60)%10)/2; //very simple ideas prooved by bruteforce
	animalIndex = ((year+56)%60)%12;
	if (strcmp(language,DEBUG_LANGUAGE)){		//[WARNING] Debug language shoud be the first of them!
		debugDispay(year);
	}
	if (strcmp(language,"RU")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"EN")){
		cout << year << messeges[1] << colors[colorIndex][0]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"GE")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
}
void debugDispay(int year){
		int i=0;
		for (;i<numberOfSuppportedLaguages-1; ++i){
			cout << "[DEBUG] " << i << ":\t";
			setLanguageEnterface(supportedLanguages[i]);
			displayNameOfYearInEasternCalendar(supportedLanguages[i],year);
		}
		 i=-1;
}
//*********************************
bool isGetHelpSyntaxCorrect(int argc){
	// Get help parameter doesn't requare any arguments
	int helpIndex = getParameterIndex(GET_HELP_PARAMETER);
	if (argc!=2){
		//if we have others parameters or GET_HELP_PARAMETER have any arguments
		return false;
	}
	return true; //[OK] User want's help!
}
bool isSetLanguageSyntaxCorrect(int argc, char **argv){
	int setLanguageIndex = getParameterIndex(SET_LANGUAGE_PARAMETER);
	// Language parameter requre just one argument
	if((indexOfParameter[setLanguageIndex][END_INDEX] - indexOfParameter[setLanguageIndex][START_INDEX])==1){
		if(isLangueageSupported(argv [indexOfParameter [setLanguageIndex] [END_INDEX]])){
			return true;	//[OK] User want's change lanuage (language syntax correct and language supported) 
		} else {
			return false; //[ERROR]Language isn's supported
		}
	} else {
		return false;//[ERROR] No agruments or more than one argument
	}
}
bool isCalculateSyntaxCorrect(char **argv){
	int calculateIndex = getParameterIndex(CALCULATE_PARAMETER);
	//[ERROR_CHECK]  parameter must have at least one argument
	if (indexOfParameter[calculateIndex][START_INDEX] == indexOfParameter[calculateIndex][END_INDEX]){ 
	return false;
	}	
	//[ERROR_CHECK] if the argument is one, then it must be an operand
	if (indexOfParameter[calculateIndex][END_INDEX] - indexOfParameter[calculateIndex][START_INDEX] == 1){ 
		if (isOperation(argv[indexOfParameter[calculateIndex][END_INDEX]])) {
			return false; //[ERROR] not operand
		} else {
			if (isOperand (argv[indexOfParameter[calculateIndex][END_INDEX]])) {
				return true;	//case with one argument is considered
			} else {
				return false;	//incorrect argument
			} 
		}
	}
	if (isOperation(argv[indexOfParameter[calculateIndex][END_INDEX]])) {return false;}	//[ERROR_CHECK] the last argument must be an operand
	if (strcmp(argv[indexOfParameter[calculateIndex][START_INDEX]+1],AND_OPERATION)) {return false;} 	//[ERROR_CHECK] The first argument should not be "AND"
	int actualOperationIndex = indexOfParameter[calculateIndex][START_INDEX]+1;
	int actualOperandIndex = indexOfParameter[calculateIndex][START_INDEX]+2;
	if (!isOperation(argv[actualOperationIndex])){
		++actualOperationIndex;
		--actualOperandIndex;
	}
	for (; actualOperationIndex < indexOfParameter[calculateIndex][END_INDEX]; actualOperationIndex+=2, actualOperandIndex+=2){
		if (isOperation(argv[actualOperationIndex]) && isOperand(argv[actualOperandIndex])){
			continue;
		} else {
			return false;
		}
	}
	return true;
}
bool checkAllParametersSytax(int argc,char **argv){
	if (indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX] && !isCalculateSyntaxCorrect(argv)) {return false;}
	if (indexOfParameter[getParameterIndex(SET_LANGUAGE_PARAMETER)][START_INDEX] && !isSetLanguageSyntaxCorrect(argc,argv)) {return false;}
	if (indexOfParameter[getParameterIndex(GET_HELP_PARAMETER)][START_INDEX] && !isGetHelpSyntaxCorrect(argc)) {return false;}
	return true;
}
void execute(int argc,char **argv){
	if (indexOfParameter[getParameterIndex(GET_HELP_PARAMETER)][START_INDEX]){
		getHelpNote();
		return;
	}
	if (indexOfParameter[getParameterIndex(SET_LANGUAGE_PARAMETER)][START_INDEX]){
		setLanguageEnterface(argv[indexOfParameter[getParameterIndex(SET_LANGUAGE_PARAMETER)][START_INDEX]+1]);
	}	else{
		setLanguageEnterface(DEFAULT_LANGUAGE);
	}
	if (indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX]){
	int commandIndex;
	if(isOperation(argv[indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX]+1])) {
		commandIndex = indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX]+1;
		executeOperationFromTo(0,atoi(argv[indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX]+2]));
	}else {
		commandIndex = indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][START_INDEX]+2;
	}
	executeOperationAnd(atoi(argv[commandIndex-1]));
	for (;commandIndex < indexOfParameter[getParameterIndex(CALCULATE_PARAMETER)][END_INDEX];commandIndex+=2){
		if(strcmp(argv[commandIndex],AND_OPERATION)){executeOperationAnd(atoi(argv[commandIndex+1]));}
		if(strcmp(argv[commandIndex],FROM_TO_OPERATION)){executeOperationFromTo(atoi(argv[commandIndex-1]),atoi(argv[commandIndex+1]));}
	}
	}
}
bool isOperand(char *a){
	int i = 0;																				
	while (a[i]) {																																						
		if (((int)(a[i]) < 48) || ((int)a[i] > 57)) return false;								
		i++;
	}
	if (isOperand(atoi(a))) return true;
	return false;
}
bool isOperand(int num){
	if ( num > 0 ){return true;}
	return false;
}