/*
author:soroka
task: name the months using the eastern calendar
tested: all cases
*/
#define MAX_COLOR_WORLD_SIZE 20	//example "Hi"-2+'/0'=3 
#define MAX_ANIMAL_WORLD_SIZE 20
#define MAX_MESSAGE_SENTENSE_SIZE 200
#define DEFAULT_LANGUAGE "EN\0"
#define DEBUG_LANGUAGE "77"		// [WARNING]Not more than 2 symbols!
#include <iostream>
using namespace std;
char supportedLanguages[][3]={
	"RU","EN","GE",DEBUG_LANGUAGE			//[WARNING] DEBUG languege("77") should to be last of them!!!
};
const int numberOfSuppportedLaguages = sizeof(supportedLanguages)/3;	//3-is max size of slanguage name
char messegesRU[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Введите год(>0): ",
		" - год "
	};
char messegesEN[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Jahr eingeben(>0): ",
		" - year of "
	};
char messegesGE[2][MAX_MESSAGE_SENTENSE_SIZE]={
		"Enter correсt year(>0): ",
		" - Jahr des "
	};
char colorsRU[5][3][MAX_COLOR_WORLD_SIZE]={		//one russian sybol is 2-byte? what?(8*2+1) It's UNICODE?
		{"зеленой",	"зеленого",	"NONE!"},
		{"красной",	"красного",	"NONE!"},
		{"желтой",	"жёлтого" ,	"NONE!"},
		{"белой",	"белого"  ,	"NONE!"},
		{"чёрной",	"чёрного" ,	"NONE!"}
	};
char colorsEN[5][3][MAX_COLOR_WORLD_SIZE]={
		{"green",	"NONE!",	"NONE!"},
		{"red",		"NONE!",	"NONE!"},
		{"yellow",	"NONE!",	"NONE!"},
		{"white",	"NONE!",	"NONE!"},
		{"black",	"NONE!",	"NONE!"}
	};
char colorsGE[5][3][MAX_COLOR_WORLD_SIZE]={
		{"grüne",	"grüner",	"grünes"},
		{"rote",	"roter",	"rotes"},
		{"gelbe",	"gelber",	"gelbes"},
		{"weiße",	"weißer",	"weißes"},
		{"schwarze","schwarzer","schwarzes"}
	};
char animalsRU[12][MAX_ANIMAL_WORLD_SIZE]{
		"крысы",
		"быка",
		"тигра",
		"кролика",
		"дракона",
		"змеи",
		"лоошади",
		"овцы",
		"обезьяны",
		"курицы",
		"собаки",
		"свиньи"
 	};
char animalsEN[12][MAX_ANIMAL_WORLD_SIZE]{
		"rat",
		"bull",
		"tiger",
		"rabbit",
		"dragon",
		"snake",
		"horse",
		"sheep",
		"monkey",
		"chicken",
		"dog",
		"pig"
	};
char animalsGE[12][MAX_ANIMAL_WORLD_SIZE]{
		"Ratte",
		"Stier",
		"Tiger",
		"Hase",
		"Drachen",
		"Schlange",
		"Pferd",
		"Schaf",
		"Affe",
		"Hähnchen",
		"Hund",
		"Schwein"
	};
char (*animals)[MAX_ANIMAL_WORLD_SIZE];
char (*colors)[3][MAX_COLOR_WORLD_SIZE];
char (*messeges)[MAX_MESSAGE_SENTENSE_SIZE];
bool isCorrect(char *a);
bool isCorrect(int num);
int	getFormOfWord(char *language, int animalIndex);
bool strcmp(const char *a,const char *b);
void setLanguageEnterface(char * language);
bool isLangueageSupported(char *language);
void displayNameOfYearInEasternCalendar(char *language,int year);
void debugDispay(int );
int main(int argc,char **argv){
	setlocale(0," ");
	char language[4] = DEFAULT_LANGUAGE;
	if(argc>=3){
		if (isLangueageSupported(argv[2]) ){
			language[0] = argv[2][0];
			language[1] = argv[2][1];
			language[2] = '\0';
		} else{
				cout << "[!]Suppornted langueges are: ";
				for (int i=0; i < numberOfSuppportedLaguages; ++i) cout << supportedLanguages[i] << ", ";
				cout << "\b\b.\n"; 
			cout << "[!] Sorry, but your language is't supported\n"; 
			cout << "[!] Language was seted to "<< language << '\n';
		}
	}
	//cout << language;
	setLanguageEnterface(language);
	/*int year = -1;				
	if (argc>=2 && isCorrect(argv[1])){ 
		year=atoi(argv[1]);
	} else{
		while (!isCorrect(year)){
			system("cls || clear");
			cout << messeges[0];
			cin >> year;
		}
	}*/
	//for(year = 1984;year < 2044; ++year){
	//displayNameOfYearInEasternCalendar(language,year);
//}
	//system("pause");
	return 0;
}
bool isCorrect(char *a){
	int i = 0;																				
	while (a[i]) {																																						
		if (((int)(a[i]) < 48) || ((int)a[i] > 57)) return false;								
		i++;
	}
	if (isCorrect(atoi(a))) return true;
	return false;
}
bool isCorrect(int num){
	if ( num >= 0 ){return true;}
	return false;
}
void setLanguageEnterface(char * language){
	//cout << language;
	if(strcmp(language,"RU")){
		animals = &animalsRU[0];
		colors = &colorsRU[0];
		messeges = &messegesRU[0];
}
	if(strcmp(language,"EN")){
		animals = &animalsEN[0];
		colors = &colorsEN[0];
		messeges = &messegesEN[0];
	}
	if(strcmp(language,"GE")){
		animals = &animalsGE[0];
		colors = &colorsGE[0];
		messeges = &messegesGE[0];
	}
	if(strcmp(language,DEBUG_LANGUAGE)){
		cout << "[!!!] DEBUG MODE\n";
	}
}
bool strcmp(const char *a,const char *b){
	int i=0;
	while(a[i] && b[i]){
		if (a[i]!=b[i]) return false;
		++i;
	}
	if (a[i]==b[i]) return true;
	return false;
}
bool isLangueageSupported(char *language){
	int i=0;
	while(i<numberOfSuppportedLaguages){
		if(strcmp(supportedLanguages[i],language)) return true;
		i++;
	}
	return false;
}
int getFormOfWord(char *language, int animalIndex){
	if (strcmp(language,"RU")){
		switch (animalIndex){
			//male
			case 1:
			case 2:
			case 3:
			case 4:
				return 1;
			//female
			case 6:
			case 7:
			case 9:
			case 11:
				return 3;
			default:
				return 2;
		}
	}
	if (strcmp(language,"EN")){
		return 0;
	}
	if (strcmp(language,"GE")){
		switch (animalIndex){
			case 0:
			case 5:
				return 0;
			case 6:
			case 7:
			case 9:
			case 11:
				return 2;
			default:
				return 1;
		}	
	}
}
void displayNameOfYearInEasternCalendar(char *language,int year){	
	unsigned short int animalIndex;
	unsigned short int colorIndex;
	colorIndex = (((year+56)%60)%10)/2; //very simple ideas prooved by bruteforce
	animalIndex = ((year+56)%60)%12;
	if (strcmp(language,DEBUG_LANGUAGE)){		//[WARNING] Debug language shoud be the first of them!
		debugDispay(year);
	}
	if (strcmp(language,"RU")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"EN")){
		cout << year << messeges[1] << colors[colorIndex][0]<<	' ' << animals[animalIndex] << '\n';
	}
	if (strcmp(language,"GE")){
		cout << year << messeges[1] << colors[colorIndex][getFormOfWord(language,animalIndex)]<<	' ' << animals[animalIndex] << '\n';
	}
}
void debugDispay(int year){
		int i=0;
		for (;i<numberOfSuppportedLaguages-1; ++i){
			cout << "[DEBUG] " << i << ":\t";
			setLanguageEnterface(supportedLanguages[i]);
			displayNameOfYearInEasternCalendar(supportedLanguages[i],year);
		}
		 i=-1;
}