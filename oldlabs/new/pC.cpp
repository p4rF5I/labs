/*
author: soroka
task: some turns -?
tested: full tested
*/
/*	N0
W3		E1
	S2
*/	
#include <iostream>
#define DEFAULT_DIRECTION 'N'
#define EXIT_COMMAND 77
#define RESET_DIRECTION_COMMAND 7
unsigned char execute(unsigned char direction,short int command);
int getDirectionIndex(unsigned char direction);
unsigned char getDirection(int directionIndex);
bool isCommandCorrect(short int command);
void enterDirection(unsigned char &direction,unsigned char &previousDirection);
void printMenu(unsigned char previousDirection,unsigned char direction,short int command);
void printMap(unsigned char direction);
using namespace std;
int main(){
	unsigned char direction='*';
	unsigned char previousDirection='*';
	cout << "note: 'N'-north,'E' - eash,'S' - south,'W' - west\n";
	enterDirection(direction,previousDirection);
	short int command=77;
	while(true){
		printMenu(previousDirection,direction,command);
		cout << "Enter new command: ";
		cin >> command;
		if (isCommandCorrect(command)){
			if ((command == RESET_DIRECTION_COMMAND) || (command == EXIT_COMMAND)){
				if (command==RESET_DIRECTION_COMMAND){
					system("clear || cls");
					enterDirection(direction,previousDirection);
				}else {
					system("pause");
					return 0;
				}
		}
		else{
			previousDirection=direction;
			direction=execute(direction,-command); //we use -commande because it simplifies calculations
			system("clear || cls");	
			}
		} else {
			system("clear || cls");
		}
	}
}
bool isCommandCorrect(short int command){
	if (command==1 || command==-1 || command==2 || command==RESET_DIRECTION_COMMAND || command==EXIT_COMMAND) return true;
	return false;
}
unsigned char execute(unsigned char direction,short int command){
	int directionIndex=getDirectionIndex(direction);
	directionIndex+=command+4;
	directionIndex%=4;
	//cout << directionIndex<< '\n';
	return getDirection(directionIndex);

}
unsigned char getDirection(int directionIndex){
	switch(directionIndex){
	case 0:
		return 'N';
	case 1:
		return 'E';
	case 2:
		return 'S';
	case 3:
		return 'W';
	}
}
int getDirectionIndex(unsigned char direction){
	switch(direction){
	case 'N':
		return 0;
	case 'E':
		return 1;
	case 'S':
		return 2;
	case 'W':
		return 3;
	}
}
void printMenu(unsigned char previousDirection,unsigned char direction,short int command){
	cout << "Use: '1' -turn left,'-1' - turn right,'2' - trun back, '7' - set new direction,'77' - exit\n";
	cout << "cr./pr.::last command:\n " << direction <<'/'<< previousDirection  << "::"<< command<< '\n';
	printMap(direction);
}
void enterDirection(unsigned char &direction,unsigned char &previousDirection){
	system ("cls || clear");
	cout << "Enter your direction: ";
	previousDirection=direction;
	cin.clear();
	cin >> direction;
	if (direction!='N' && direction!='S' && direction!='W' && direction!='E'){
		cout << "Incorrect direction!\n";
		cout << "Your direction was setted to: " << DEFAULT_DIRECTION << '\n';
		direction=DEFAULT_DIRECTION;
		return;
	}
	system("clear || cls");
}
void printMap(unsigned char direction){
	unsigned char t;
	switch (direction){
	case 'N':
		t='^';
		break;
	case 'E':
		t='>';
		break;
	case 'S':
		t='V';
		break;
	case 'W':
		t='<';
		break;
	}
	cout <<"\tN\n";
	cout <<"\t*\n";
	cout <<"\t*\n";
	cout << "W*******" << t << "*******E\n";
	cout <<"\t*\n";
	cout <<"\t*\n";
	cout <<"\tS\n";
}