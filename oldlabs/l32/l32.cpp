#include <iostream>
using namespace std;
bool isDigitInNumber(int digit,int a);
int  digitsInNumber(int number);
int numberOfDifferentDigits(int number);
int main(){
	int number;
	cout << "Enter number : ";
	if (!(cin >> number)){
		cout << "[ERROR]Incorrect input!\n"; 
		system("pause");
		return 1;
	}
	for (int i=number;i!=0;--i ){
		// cout << numberOfDifferentDigits(i) << '\t' << digitsInNumber(i) << '\t' << i << '\n';
		if (numberOfDifferentDigits(i) == digitsInNumber(i)) {cout << i << ", ";}
	}
	cout << "\b\b.\n";
	system("pause");
	return 0;
}
int  digitsInNumber(int number){
	static int last=33;
	static int digits=2;
	// cout << number << '\t' << last << '\n';
	if (number==last){
		// cout << "here\n";
		return digits;
	} else {
		last = number;
		for (digits=1;number>=10;number/=10, ++digits);
		return digits;
	}
}
bool isDigitInNumber(int digit,int number) {
	int i = 0;
	int digits=digitsInNumber(number);
	for (int i=1,t=1;i<=digits;++i,t*=10){
		if (digit==(number/t)%10) return true;
	}
	return false;
}
int numberOfDifferentDigits(int number){
	bool digit[10];
	int digits=digitsInNumber(number);
	int result=0;
	int temp;
	for (int i=0;i<10;++i){digit[i]=false;}
	for (int i=1,t=1; i<=digits; ++i, t*=10){
		temp=(number/t)%10;
		if (!digit[temp]) {digit[temp]=true; ++result;}
	}
	return result;
}